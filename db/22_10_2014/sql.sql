-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table madinah.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.admin: ~1 rows (approximately)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`, `name`, `username`, `password`, `image`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'Administrator', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'DSCF8464.JPG', NULL, NULL, NULL, 'ar');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;


-- Dumping structure for table madinah.center_member
CREATE TABLE IF NOT EXISTS `center_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `emails` varchar(255) DEFAULT NULL,
  `mobiles` varchar(255) DEFAULT NULL,
  `phones` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `cv` varchar(255) DEFAULT NULL,
  `content` text,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.center_member: ~1 rows (approximately)
/*!40000 ALTER TABLE `center_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `center_member` ENABLE KEYS */;


-- Dumping structure for table madinah.content
CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_category_id` int(11) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.content: ~30 rows (approximately)
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` (`id`, `content_category_id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `content`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 4, 'eeeeeeeee', 'eeeeeeeeeeeeee', 'khaled', 'eeeeeeeeeeeeeee', '3azmmJYypUBl.jpg', '<p><img alt="" src="/ckfinder/userfiles/images/tw.png" style="height:48px; width:48px" /><img alt="" src="/ckfinder/userfiles/images/fb.png" style="height:48px; width:48px" /><img alt="" src="/ckfinder/userfiles/images/DSC01213.JPG" style="border-style:solid; border-width:1px; float:right; height:180px; width:240px" /></p>\r\n', '2014-10-21 00:00:00', 1, 0, 'ar'),
	(2, 2, 'sdsd', 'dsd', 'dsddsdsd', 'dsdsd', 'DmQex4bwW606.jpg', '', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(3, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(4, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(5, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(6, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(7, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(8, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(9, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(10, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(11, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(12, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(13, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(14, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(15, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(16, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(17, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(18, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(19, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(20, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(21, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(22, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(23, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(24, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(25, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(26, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(27, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(28, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(29, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar'),
	(30, 2, 'ssssssssadad', 'sadasd', 'sadasd', 'dasdasdas', '3azmmJYypUBl.jpg', 'sdasdasd', '2014-10-20 09:54:57', 1, 0, 'ar');
/*!40000 ALTER TABLE `content` ENABLE KEYS */;


-- Dumping structure for table madinah.content_category
CREATE TABLE IF NOT EXISTS `content_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.content_category: ~3 rows (approximately)
/*!40000 ALTER TABLE `content_category` DISABLE KEYS */;
INSERT INTO `content_category` (`id`, `code`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `content`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'about_madinah', 'عن المدينة المنورة', 'عن المدينة المنورة', 'عن المدينة المنورة', 'عن المدينة المنورة', NULL, 'عن المدينة المنورة', NULL, NULL, 0, 'ar'),
	(2, 'news', 'الأخبار', 'الأخبار', 'الأخبار', 'الأخبار', NULL, 'الأخبار', NULL, NULL, 0, 'ar'),
	(3, 'events', 'الفعاليات', 'الفعاليات', 'الفعاليات', 'الفعاليات', NULL, 'الفعاليات', NULL, NULL, 0, 'ar'),
	(4, 'new_events', 'جديد الفعاليات', 'جديد الفعاليات', 'جديد الفعاليات', 'جديد الفعاليات', NULL, 'جديد الفعاليات', NULL, NULL, 0, 'ar'),
	(5, 'content', 'محتوى', 'محتوى', 'محتوى', 'محتوى', NULL, 'محتوى', NULL, NULL, 0, 'ar');
/*!40000 ALTER TABLE `content_category` ENABLE KEYS */;


-- Dumping structure for table madinah.doc
CREATE TABLE IF NOT EXISTS `doc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_category_id` int(11) NOT NULL DEFAULT '0',
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text,
  `file` varchar(500) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.doc: ~18 rows (approximately)
/*!40000 ALTER TABLE `doc` DISABLE KEYS */;
INSERT INTO `doc` (`id`, `doc_category_id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `content`, `file`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 5, 'ثثث', 'ثثثث', 'ملف 1', 'ثثثثث', '42EB6ztYXroV.jpg', '', '0TDA7SHhDN40.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(2, 5, 'test', 'sasasa', 'test', '', 'JHgQSW1NAuYV.jpg', '', 'FBZZSH9YRLSD.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(3, 5, 'ثثث', 'ثثثث', 'ملف 1', 'ثثثثث', '42EB6ztYXroV.jpg', '', '0TDA7SHhDN40.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(4, 5, 'test', 'sasasa', 'test', '', 'JHgQSW1NAuYV.jpg', '', 'FBZZSH9YRLSD.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(5, 5, 'ثثث', 'ثثثث', 'ملف 1', 'ثثثثث', '42EB6ztYXroV.jpg', '', '0TDA7SHhDN40.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(6, 5, 'test', 'sasasa', 'test', '', 'JHgQSW1NAuYV.jpg', '', 'FBZZSH9YRLSD.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(7, 5, 'ثثث', 'ثثثث', 'ملف 1', 'ثثثثث', '42EB6ztYXroV.jpg', '', '0TDA7SHhDN40.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(8, 4, 'test', 'sasasa', 'test', '', 'JHgQSW1NAuYV.jpg', '', 'FBZZSH9YRLSD.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(9, 5, 'ثثث', 'ثثثث', 'ملف 1', 'ثثثثث', '42EB6ztYXroV.jpg', '', '0TDA7SHhDN40.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(10, 5, 'test', 'sasasa', 'test', '', 'JHgQSW1NAuYV.jpg', '', 'FBZZSH9YRLSD.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(11, 4, 'ثثث', 'ثثثث', 'ملف 1', 'ثثثثث', '42EB6ztYXroV.jpg', '', '0TDA7SHhDN40.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(12, 5, 'test', 'sasasa', 'test', '', 'JHgQSW1NAuYV.jpg', '', 'FBZZSH9YRLSD.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(13, 5, 'ثثث', 'ثثثث', 'ملف 1', 'ثثثثث', '42EB6ztYXroV.jpg', '', '0TDA7SHhDN40.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(14, 5, 'test', 'sasasa', 'test', '', 'JHgQSW1NAuYV.jpg', '', 'FBZZSH9YRLSD.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(15, 4, 'ثثث', 'ثثثث', 'ملف 1', 'ثثثثث', '42EB6ztYXroV.jpg', '', '0TDA7SHhDN40.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(16, 5, 'test', 'sasasa', 'test', '', 'JHgQSW1NAuYV.jpg', '', 'FBZZSH9YRLSD.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(17, 5, 'ثثث', 'ثثثث', 'ملف 1', 'ثثثثث', '42EB6ztYXroV.jpg', '', '0TDA7SHhDN40.jpg', '2014-10-14 00:00:00', 1, 0, 'ar'),
	(18, 4, 'test', 'sasasa', 'test', '', 'JHgQSW1NAuYV.jpg', '', 'FBZZSH9YRLSD.jpg', '2014-10-14 00:00:00', 1, 0, 'ar');
/*!40000 ALTER TABLE `doc` ENABLE KEYS */;


-- Dumping structure for table madinah.doc_category
CREATE TABLE IF NOT EXISTS `doc_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.doc_category: ~4 rows (approximately)
/*!40000 ALTER TABLE `doc_category` DISABLE KEYS */;
INSERT INTO `doc_category` (`id`, `code`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'books', 'الكتب', 'الكتب', 'الكتب', 'الكتب', NULL, NULL, NULL, 0, 'ar'),
	(2, 'manuscripts', 'المخطوطات', 'المخطوطات', 'المخطوطات', 'المخطوطات', NULL, NULL, NULL, 0, 'ar'),
	(3, 'documents', 'الوثائق', 'الوثائق', 'الوثائق', 'الوثائق', NULL, NULL, NULL, 0, 'ar'),
	(4, 'research', 'البحوث', 'البحوث', 'البحوث', 'البحوث', NULL, NULL, NULL, 0, 'ar'),
	(5, 'new_releases', 'جديد الاصدارات', 'جديد الاصدارات', 'جديد الاصدارات', 'جديد الاصدارات', NULL, NULL, NULL, 0, 'ar');
/*!40000 ALTER TABLE `doc_category` ENABLE KEYS */;


-- Dumping structure for table madinah.email
CREATE TABLE IF NOT EXISTS `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.email: ~0 rows (approximately)
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
/*!40000 ALTER TABLE `email` ENABLE KEYS */;


-- Dumping structure for table madinah.gallery
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.gallery: ~1 rows (approximately)
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
INSERT INTO `gallery` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `active`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, '', '', 'test', '', 'PW0V2hoSQEul.jpg', 1, '2014-10-20 00:00:00', 1, 0, 'ar');
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;


-- Dumping structure for table madinah.gallery_photo
CREATE TABLE IF NOT EXISTS `gallery_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.gallery_photo: ~0 rows (approximately)
/*!40000 ALTER TABLE `gallery_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_photo` ENABLE KEYS */;


-- Dumping structure for table madinah.header_slider
CREATE TABLE IF NOT EXISTS `header_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `content` text,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.header_slider: ~1 rows (approximately)
/*!40000 ALTER TABLE `header_slider` DISABLE KEYS */;
INSERT INTO `header_slider` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `content`, `image`, `active`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'بسي', 'بس', 'بسيبسيبسيب', 'سيبسي', NULL, 'aBQd5u7IB8QK.jpg', 1, NULL, 1, 0, 'ar'),
	(2, 'test', 'dasdasd', 'Reham test', 'Reham test Reham test Reham test Reham test Reham test Reham test', '<p style="text-align: right;">Reham test Reham test Reham test Reham test Reham test Reham test</p>\r\n\r\n<p style="text-align: right;">Reham test Reham test Reham test Reham test Reham test Reham test</p>\r\n\r\n<p style="text-align: right;">Reham test Reham test Reham test Reham test Reham test Reham test Reham test Reham test Reham test Reham test Reham test Reham test</p>\r\n', 'kQtzicEYW1OC.jpg', 1, NULL, 1, 0, 'ar');
/*!40000 ALTER TABLE `header_slider` ENABLE KEYS */;


-- Dumping structure for table madinah.madinah_place
CREATE TABLE IF NOT EXISTS `madinah_place` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `youtube_link` varchar(500) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `map_Latitude` varchar(50) DEFAULT NULL,
  `map_longitude` varchar(50) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.madinah_place: ~3 rows (approximately)
/*!40000 ALTER TABLE `madinah_place` DISABLE KEYS */;
INSERT INTO `madinah_place` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `video`, `youtube_link`, `gallery_id`, `map_Latitude`, `map_longitude`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, '', '', 'test', 'ay haga', 'DSC01213.JPG', 'what.mp4', '', 1, '24.4211245720623', '39.6171875', '2014-10-20 00:00:00', 1, 0, 'ar'),
	(2, '', '', 'test2', 'ay haga2', 'DSC01213.JPG', 'what.mp4', '', 1, '24.48503631731729', '39.61787414550781', '2014-10-20 00:00:00', 1, 0, 'ar'),
	(3, '', '', 'test3', 'ay haga3', 'DSC01213.JPG', 'what.mp4', '', 1, '24.442067442823692', '39.61787414550781', '2014-10-20 00:00:00', 1, 0, 'ar'),
	(4, '', '', 'test ttest test', 'Test For Style And Test Again', 'E2wsBEdZaSQ2.jpg', NULL, '', 1, '24.461131807318207', '40.0126953125', '2014-10-20 00:00:00', 1, 0, 'ar');
/*!40000 ALTER TABLE `madinah_place` ENABLE KEYS */;


-- Dumping structure for table madinah.member_of_honor
CREATE TABLE IF NOT EXISTS `member_of_honor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.member_of_honor: ~0 rows (approximately)
/*!40000 ALTER TABLE `member_of_honor` DISABLE KEYS */;
INSERT INTO `member_of_honor` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `content`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, '', '', 'test', 'test', '986nVXJVdrjH.jpeg', '', NULL, NULL, 0, 'ar');
/*!40000 ALTER TABLE `member_of_honor` ENABLE KEYS */;


-- Dumping structure for table madinah.nav
CREATE TABLE IF NOT EXISTS `nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.nav: ~2 rows (approximately)
/*!40000 ALTER TABLE `nav` DISABLE KEYS */;
INSERT INTO `nav` (`id`, `code`, `sort`, `title`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'main', NULL, 'الرئيسية', NULL, NULL, 0, 'ar'),
	(2, 'main', NULL, 'Main', NULL, NULL, 0, 'en');
/*!40000 ALTER TABLE `nav` ENABLE KEYS */;


-- Dumping structure for table madinah.nav_link
CREATE TABLE IF NOT EXISTS `nav_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `nav_id` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.nav_link: ~11 rows (approximately)
/*!40000 ALTER TABLE `nav_link` DISABLE KEYS */;
INSERT INTO `nav_link` (`id`, `title`, `link`, `nav_id`, `parent`, `sort`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'المركز الاعلامي', 'content_category/1', 1, NULL, 4, '2014-10-14 00:00:00', 1, 0, 'ar'),
	(2, 'المكتبه', 'content_category/2', 1, NULL, 2, '2014-10-14 00:00:00', 1, 0, 'ar'),
	(3, 'مواقع المدينه', 'madinah_place', 1, NULL, 3, '2014-10-15 00:00:00', 1, 0, 'ar'),
	(4, 'الملخصات', 'content_category/4', NULL, 1, NULL, '2014-10-15 00:00:00', 1, 0, 'ar'),
	(5, 'جديد الاصدارات', 'doc_category/5', NULL, 2, 1, '2014-10-15 00:00:00', 1, 0, 'ar'),
	(6, 'البحوث', 'doc_category/4', NULL, 2, 2, '2014-10-15 00:00:00', 1, 0, 'ar'),
	(7, 'موقع النخبه', 'madinah_place/1', NULL, 3, NULL, '2014-10-15 00:00:00', 1, 0, 'ar'),
	(8, 'الرئيسية', '', 1, NULL, 1, '2014-10-14 00:00:00', 1, 0, 'ar'),
	(9, 'مقترحات وشكاوي', 'proposal/create', 1, NULL, 5, NULL, NULL, 0, 'ar'),
	(10, 'الفيديوهات', NULL, 1, NULL, 6, '2014-10-15 00:00:00', 1, 0, 'ar'),
	(11, 'فيديوهات الصفحه الرئيسيه', 'video_category/1', NULL, 10, NULL, '2014-10-15 00:00:00', 1, 0, 'ar');
/*!40000 ALTER TABLE `nav_link` ENABLE KEYS */;


-- Dumping structure for table madinah.questionnaire
CREATE TABLE IF NOT EXISTS `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `question` varchar(500) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.questionnaire: ~2 rows (approximately)
/*!40000 ALTER TABLE `questionnaire` DISABLE KEYS */;
INSERT INTO `questionnaire` (`id`, `seo_meta_keywords`, `seo_meta_description`, `question`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, NULL, NULL, 'ماهو رأيك بالتطوير الأخير للموقع؟؟؟؟!!!!!', NULL, NULL, 0, 'ar'),
	(2, NULL, NULL, 'سؤال تاني ', NULL, NULL, 0, 'ar');
/*!40000 ALTER TABLE `questionnaire` ENABLE KEYS */;


-- Dumping structure for table madinah.questionnaire_choice
CREATE TABLE IF NOT EXISTS `questionnaire_choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `choice` varchar(500) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_questionnaire_choice_questionnaire` (`questionnaire_id`),
  CONSTRAINT `FK_questionnaire_choice_questionnaire` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaire` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.questionnaire_choice: ~6 rows (approximately)
/*!40000 ALTER TABLE `questionnaire_choice` DISABLE KEYS */;
INSERT INTO `questionnaire_choice` (`id`, `questionnaire_id`, `seo_meta_keywords`, `seo_meta_description`, `choice`, `last_update_date`, `last_update_admin_id`, `deleted`) VALUES
	(1, 1, NULL, NULL, 'مميز وإلى الأمام.', NULL, NULL, 0),
	(2, 1, NULL, NULL, 'sssss', NULL, NULL, 0),
	(3, 1, NULL, NULL, ' سئ يحتاج الكثير.', NULL, NULL, 0),
	(4, 2, NULL, NULL, 'ffff', NULL, NULL, 0),
	(5, 2, NULL, NULL, 'ggggggggggggg', NULL, NULL, 0),
	(6, 2, NULL, NULL, 'yyyyyyyyyy', NULL, NULL, 0);
/*!40000 ALTER TABLE `questionnaire_choice` ENABLE KEYS */;


-- Dumping structure for table madinah.questionnaire_choice_by_ip
CREATE TABLE IF NOT EXISTS `questionnaire_choice_by_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  `questionnaire_choice_id` int(11) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.questionnaire_choice_by_ip: ~7 rows (approximately)
/*!40000 ALTER TABLE `questionnaire_choice_by_ip` DISABLE KEYS */;
INSERT INTO `questionnaire_choice_by_ip` (`id`, `ip`, `questionnaire_id`, `questionnaire_choice_id`, `seo_meta_keywords`, `seo_meta_description`, `last_update_date`, `last_update_admin_id`, `deleted`) VALUES
	(8, '127.0.0.1', 1, 1, NULL, NULL, NULL, 2, 0),
	(9, '192.168.1.103', 1, 3, NULL, NULL, NULL, 2, 0),
	(26, '127.0.0.1', 1, 2, NULL, NULL, NULL, 2, 0),
	(27, '127.0.5.1', 2, 4, NULL, NULL, NULL, 2, 0),
	(28, '127.0.5.1', 2, 5, NULL, NULL, NULL, 2, 0),
	(29, '127.0.5.1', 2, 6, NULL, NULL, NULL, 2, 0),
	(30, '127.0.5.1', 2, 4, NULL, NULL, NULL, 2, 0);
/*!40000 ALTER TABLE `questionnaire_choice_by_ip` ENABLE KEYS */;


-- Dumping structure for table madinah.related_link
CREATE TABLE IF NOT EXISTS `related_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.related_link: ~2 rows (approximately)
/*!40000 ALTER TABLE `related_link` DISABLE KEYS */;
INSERT INTO `related_link` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `link`, `image`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'تتتتت', 'تتتت', 'دارة الملك عبدالعزيز', 'content/1', NULL, NULL, NULL, 0, 'ar'),
	(2, 'ooooo', 'oooooo', 'متحف الملك عبدالعزيز الوطني', 'center_member/1', NULL, NULL, NULL, 0, 'ar');
/*!40000 ALTER TABLE `related_link` ENABLE KEYS */;


-- Dumping structure for table madinah.screen
CREATE TABLE IF NOT EXISTS `screen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.screen: ~72 rows (approximately)
/*!40000 ALTER TABLE `screen` DISABLE KEYS */;
INSERT INTO `screen` (`id`, `title`, `code`, `table_name`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'الرئيسية', 'home', '', NULL, NULL, 0, 'ar'),
	(2, 'اعضاء المركز', 'center_member', 'center_member', NULL, NULL, 0, 'ar'),
	(3, 'تصنيفات المحتوى', 'content_category', 'content_category', NULL, NULL, 0, 'ar'),
	(4, 'المحتوى', 'content', 'content', NULL, NULL, 0, 'ar'),
	(5, 'تصنيفات الوثائق', 'doc_category', 'doc_category', NULL, NULL, 0, 'ar'),
	(6, 'الوثائق', 'doc', 'doc', NULL, NULL, 0, 'ar'),
	(7, 'البومات الصور', 'gallery', 'gallery', NULL, NULL, 0, 'ar'),
	(8, 'مواقع المدينة', 'madinah_place', 'madinah_place', NULL, NULL, 0, 'ar'),
	(9, 'اعضاء الشرف', 'member_of_honor', 'member_of_honor', NULL, NULL, 0, 'ar'),
	(10, 'الرعاه الرسميين', 'sponsor', 'sponsor', NULL, NULL, 0, 'ar'),
	(11, 'تصنيفات الفيديوهات', 'video_category', 'video_category', NULL, NULL, 0, 'ar'),
	(12, 'الفيديوهات', 'video', 'video', NULL, NULL, 0, 'ar'),
	(13, 'الرئيسية', 'home', '', NULL, NULL, 0, 'en'),
	(14, 'اعضاء المركز', 'center_member', 'center_member', NULL, NULL, 0, 'en'),
	(15, 'تصنيفات المحتوى', 'content_category', 'content_category', NULL, NULL, 0, 'en'),
	(16, 'المحتوى', 'content', 'content', NULL, NULL, 0, 'en'),
	(17, 'تصنيفات الوثائق', 'doc_category', 'doc_category', NULL, NULL, 0, 'en'),
	(18, 'الوثائق', 'doc', 'doc', NULL, NULL, 0, 'en'),
	(19, 'البومات الصور', 'gallery', 'gallery', NULL, NULL, 0, 'en'),
	(20, 'مواقع المدينة', 'madinah_place', 'madinah_place', NULL, NULL, 0, 'en'),
	(21, 'اعضاء الشرف', 'member_of_honor', 'member_of_honor', NULL, NULL, 0, 'en'),
	(22, 'الرعاه الرسميين', 'sponsor', 'sponsor', NULL, NULL, 0, 'en'),
	(23, 'تصنيفات الفيديوهات', 'video_category', 'video_category', NULL, NULL, 0, 'en'),
	(24, 'الفيديوهات', 'video', 'video', NULL, NULL, 0, 'en'),
	(25, 'الرئيسية', 'home', '', NULL, NULL, 0, 'fr'),
	(26, 'اعضاء المركز', 'center_member', 'center_member', NULL, NULL, 0, 'fr'),
	(27, 'تصنيفات المحتوى', 'content_category', 'content_category', NULL, NULL, 0, 'fr'),
	(28, 'المحتوى', 'content', 'content', NULL, NULL, 0, 'fr'),
	(29, 'تصنيفات الوثائق', 'doc_category', 'doc_category', NULL, NULL, 0, 'fr'),
	(30, 'الوثائق', 'doc', 'doc', NULL, NULL, 0, 'fr'),
	(31, 'البومات الصور', 'gallery', 'gallery', NULL, NULL, 0, 'fr'),
	(32, 'مواقع المدينة', 'madinah_place', 'madinah_place', NULL, NULL, 0, 'fr'),
	(33, 'اعضاء الشرف', 'member_of_honor', 'member_of_honor', NULL, NULL, 0, 'fr'),
	(34, 'الرعاه الرسميين', 'sponsor', 'sponsor', NULL, NULL, 0, 'fr'),
	(35, 'تصنيفات الفيديوهات', 'video_category', 'video_category', NULL, NULL, 0, 'fr'),
	(36, 'الفيديوهات', 'video', 'video', NULL, NULL, 0, 'fr'),
	(37, 'الرئيسية', 'home', '', NULL, NULL, 0, 'id'),
	(38, 'اعضاء المركز', 'center_member', 'center_member', NULL, NULL, 0, 'id'),
	(39, 'تصنيفات المحتوى', 'content_category', 'content_category', NULL, NULL, 0, 'id'),
	(40, 'المحتوى', 'content', 'content', NULL, NULL, 0, 'id'),
	(41, 'تصنيفات الوثائق', 'doc_category', 'doc_category', NULL, NULL, 0, 'id'),
	(42, 'الوثائق', 'doc', 'doc', NULL, NULL, 0, 'id'),
	(43, 'البومات الصور', 'gallery', 'gallery', NULL, NULL, 0, 'id'),
	(44, 'مواقع المدينة', 'madinah_place', 'madinah_place', NULL, NULL, 0, 'id'),
	(45, 'اعضاء الشرف', 'member_of_honor', 'member_of_honor', NULL, NULL, 0, 'id'),
	(46, 'الرعاه الرسميين', 'sponsor', 'sponsor', NULL, NULL, 0, 'id'),
	(47, 'تصنيفات الفيديوهات', 'video_category', 'video_category', NULL, NULL, 0, 'id'),
	(48, 'الفيديوهات', 'video', 'video', NULL, NULL, 0, 'id'),
	(49, 'الرئيسية', 'home', '', NULL, NULL, 0, 'fa'),
	(50, 'اعضاء المركز', 'center_member', 'center_member', NULL, NULL, 0, 'fa'),
	(51, 'تصنيفات المحتوى', 'content_category', 'content_category', NULL, NULL, 0, 'fa'),
	(52, 'المحتوى', 'content', 'content', NULL, NULL, 0, 'fa'),
	(53, 'تصنيفات الوثائق', 'doc_category', 'doc_category', NULL, NULL, 0, 'fa'),
	(54, 'الوثائق', 'doc', 'doc', NULL, NULL, 0, 'fa'),
	(55, 'البومات الصور', 'gallery', 'gallery', NULL, NULL, 0, 'fa'),
	(56, 'مواقع المدينة', 'madinah_place', 'madinah_place', NULL, NULL, 0, 'fa'),
	(57, 'اعضاء الشرف', 'member_of_honor', 'member_of_honor', NULL, NULL, 0, 'fa'),
	(58, 'الرعاه الرسميين', 'sponsor', 'sponsor', NULL, NULL, 0, 'fa'),
	(59, 'تصنيفات الفيديوهات', 'video_category', 'video_category', NULL, NULL, 0, 'fa'),
	(60, 'الفيديوهات', 'video', 'video', NULL, NULL, 0, 'fa'),
	(61, 'الرئيسية', 'home', '', NULL, NULL, 0, 'or'),
	(62, 'اعضاء المركز', 'center_member', 'center_member', NULL, NULL, 0, 'or'),
	(63, 'تصنيفات المحتوى', 'content_category', 'content_category', NULL, NULL, 0, 'or'),
	(64, 'المحتوى', 'content', 'content', NULL, NULL, 0, 'or'),
	(65, 'تصنيفات الوثائق', 'doc_category', 'doc_category', NULL, NULL, 0, 'or'),
	(66, 'الوثائق', 'doc', 'doc', NULL, NULL, 0, 'or'),
	(67, 'البومات الصور', 'gallery', 'gallery', NULL, NULL, 0, 'or'),
	(68, 'مواقع المدينة', 'madinah_place', 'madinah_place', NULL, NULL, 0, 'or'),
	(69, 'اعضاء الشرف', 'member_of_honor', 'member_of_honor', NULL, NULL, 0, 'or'),
	(70, 'الرعاه الرسميين', 'sponsor', 'sponsor', NULL, NULL, 0, 'or'),
	(71, 'تصنيفات الفيديوهات', 'video_category', 'video_category', NULL, NULL, 0, 'or'),
	(72, 'الفيديوهات', 'video', 'video', NULL, NULL, 0, 'or'),
	(73, 'صور الهيدر المتحركة', 'header_slider', 'header_slider', '2014-10-19 10:42:22', NULL, 0, 'ar'),
	(74, 'صور الهيدر المتحركة', 'header_slider', 'header_slider', NULL, NULL, 0, 'en'),
	(75, 'صور الهيدر المتحركة', 'header_slider', 'header_slider', NULL, NULL, 0, 'fr'),
	(76, 'صور الهيدر المتحركة', 'header_slider', 'header_slider', NULL, NULL, 0, 'id'),
	(77, 'صور الهيدر المتحركة', 'header_slider', 'header_slider', NULL, NULL, 0, 'fa'),
	(78, 'صور الهيدر المتحركة', 'header_slider', 'header_slider', NULL, NULL, 0, 'or'),
	(79, 'صور الهيدر المتحركة', 'header_slider', 'header_slider', NULL, NULL, 0, 'tr');
/*!40000 ALTER TABLE `screen` ENABLE KEYS */;


-- Dumping structure for table madinah.setting
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook_link` varchar(255) DEFAULT NULL,
  `twitter_link` varchar(255) DEFAULT NULL,
  `youtube_link` varchar(255) DEFAULT NULL,
  `google_plus_link` varchar(255) DEFAULT NULL,
  `instagram_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.setting: ~1 rows (approximately)
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` (`id`, `facebook_link`, `twitter_link`, `youtube_link`, `google_plus_link`, `instagram_link`) VALUES
	(1, 'dsdsdsdsdsddsds', 'aaaaaaaaaaaaaaaa', 'ssssssssssssssssss      rfew erwerwer wer      sssssssssssssssssssssssssssss', '122sdsd', 'eeeeeeeeeeeeeeeeea as dasd asda');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;


-- Dumping structure for table madinah.sponsor
CREATE TABLE IF NOT EXISTS `sponsor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.sponsor: ~0 rows (approximately)
/*!40000 ALTER TABLE `sponsor` DISABLE KEYS */;
INSERT INTO `sponsor` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `content`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, '', '', 'test', 'test', 'Vlyhud6aFyJL.jpg', '<p><img alt="" src="/ckfinder/userfiles/images/fb.png" style="height:48px; width:48px" />test test</p>\r\n', NULL, NULL, 0, 'ar');
/*!40000 ALTER TABLE `sponsor` ENABLE KEYS */;


-- Dumping structure for table madinah.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `authentication_code` varchar(50) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.user: ~2 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `name`, `email`, `password`, `mobile`, `phone`, `address`, `active`, `authentication_code`, `last_update_date`, `last_update_admin_id`, `deleted`) VALUES
	(2, 'nehad', 'nehad@yahoo.com', '123', '012456789', '21458752', 'cairo', 1, NULL, NULL, NULL, 0),
	(36, 'hoda', 'hoda@yahoo.com', '123', '0145678985', '52435468', 'cairo', 1, NULL, NULL, NULL, 0),
	(37, 'test', 'g.elalfy@dv.com.sa', '123456', '232324234', '344234234324', 'address', 1, NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Dumping structure for table madinah.user_proposal
CREATE TABLE IF NOT EXISTS `user_proposal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `content` text,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.user_proposal: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_proposal` DISABLE KEYS */;
INSERT INTO `user_proposal` (`id`, `user_id`, `title`, `summery`, `content`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 2, 'qqqqqqqqq', 'qqqqqqqqq', '<p>qqqqqqqqqqqqqqqq</p>\r\n', NULL, NULL, 0, 'ar'),
	(2, 2, 'ooooooooooooooo', 'oooooooooooooooooooo', '<p>dsfdsgr</p>\r\n', NULL, NULL, 0, 'ar'),
	(3, 2, 'suggest', 'test description', '<p>rtretger</p>\r\n', NULL, NULL, 0, 'ar'),
	(4, 36, 'شكوي هدي', 'وصف هدي', 'محتوي هدي', NULL, NULL, 0, 'ar');
/*!40000 ALTER TABLE `user_proposal` ENABLE KEYS */;


-- Dumping structure for table madinah.video
CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `video_category_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `youtube_link` varchar(500) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.video: ~0 rows (approximately)
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
INSERT INTO `video` (`id`, `seo_meta_keywords`, `seo_meta_description`, `video_category_id`, `title`, `summery`, `video`, `youtube_link`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, '', '', 1, 'test', '', 'what.mp4', 'http://www.youtube.com/v/pWH6tBWY1ag', NULL, NULL, 0, 'ar');
/*!40000 ALTER TABLE `video` ENABLE KEYS */;


-- Dumping structure for table madinah.video_category
CREATE TABLE IF NOT EXISTS `video_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table madinah.video_category: ~1 rows (approximately)
/*!40000 ALTER TABLE `video_category` DISABLE KEYS */;
INSERT INTO `video_category` (`id`, `code`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'home_videos', NULL, NULL, 'فيديو الصفحة الرئيسية', NULL, NULL, NULL, NULL, 0, 'ar');
/*!40000 ALTER TABLE `video_category` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
