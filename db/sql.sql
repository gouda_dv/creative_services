-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table creative_services.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.admin: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`, `name`, `username`, `password`, `image`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'Administrator', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'DSCF8464.JPG', NULL, NULL, NULL, 'ar');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;


-- Dumping structure for table creative_services.branch
CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` text,
  `content` text,
  `map_Latitude` varchar(50) DEFAULT NULL,
  `map_longitude` varchar(50) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.branch: ~4 rows (approximately)
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
INSERT INTO `branch` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `content`, `map_Latitude`, `map_longitude`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'Main Branch', 'Main Branch', 'Main Branch', '<p>Tel.No. : 123456789</p>\r\n\r\n<p>Mob.No. : 123456789</p>\r\n\r\n<p>Address : Kingdom Of Saudi Arabia, Medina, 24 Sultana Street</p>\r\n', NULL, '27.68802861038297', '41.736303418750026', '2014-12-02 00:00:00', 1, 0, 'en'),
	(2, 'Branch No 2', 'Branch No 2', 'Branch No 2', '<p>Tel.No. : 123456789</p>\r\n\r\n<p>Mob.No. : 123456789</p>\r\n\r\n<p>Address :Branch No 2 Summery , Branch No 2 Summery Branch No 2 Summery</p>\r\n', NULL, '24.42362539615964', '39.7325439453125', '2014-12-02 00:00:00', 1, 0, 'en'),
	(3, 'فففف', 'فففف', 'ففف', 'فففف', NULL, '24.4211245720623', '39.6171875', '2014-11-30 00:00:00', 1, 1, 'en'),
	(4, 'wd', 'sdfa', 'casda', 'weqdwqdqw', NULL, '24.55609804756381', '39.93235778808594', '2014-12-01 00:00:00', 1, 1, 'en');
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;


-- Dumping structure for table creative_services.contact_us
CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.contact_us: ~25 rows (approximately)
/*!40000 ALTER TABLE `contact_us` DISABLE KEYS */;
INSERT INTO `contact_us` (`id`, `name`, `email`, `mobile`, `title`, `content`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(36, 'gouda galal', 'g.elalfy@dv.com.sa', '', 'Hi Message', 'Welcome To New Website', NULL, NULL, 0, 'en'),
	(41, 'nehad ebrahim', 'nehad@yahoo.com', '965501234567', 'test', '<p>test order</p>\r\n', NULL, NULL, 0, 'en');
/*!40000 ALTER TABLE `contact_us` ENABLE KEYS */;


-- Dumping structure for table creative_services.content
CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_category_id` int(11) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` text,
  `date` date DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text,
  `read_count` int(11) NOT NULL DEFAULT '0',
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.content: ~7 rows (approximately)
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` (`id`, `content_category_id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `date`, `image`, `content`, `read_count`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(26, 5, '', '', 'Creative Service Group', '<p>Communications &amp; Information Technology Division</p>\r\n\r\n<p>CSG communication &amp; Information technology division (CSGIT) is a division within creative services group which provide system integrator with wide range of Communications and Information Technology solutions in the Hospitality, Real Estate, Telecom, Utilities, and Oil &amp; Gas and Manufacturing industries.</p>\r\n', '1970-01-01', '3yoXTB0bmoaU.jpg', '<p>CSGIT committed to add the value to any project by providing the optimum IP Smart Building Solutions to fit process requirements, energy saving objective and targeted budget.</p>\r\n\r\n<p>CSGIT is a part of the Telecommunication world with extensive knowledge and experienced staff mastering the IP Smart Building Technologies for more than 10 years throughout Kingdom Of Saudi Arabia from corporate Headquarter in Madinah and Branches at Jeddah &amp; Riyadh.</p>\r\n\r\n<p>CSGIT has the required financial and human resources capacity and strength to handle large span of projects&rsquo; sizes. The workforce includes sales, support team, superintendents and specialized calibers to perform different activities ranging from site preparation to commissioning and start up for different telecom, IT and Low Current systems.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 3, '2014-12-02 00:00:00', 1, 0, 'en'),
	(27, 5, 'Vision And Mission', 'Vision And Mission', 'Vision And Mission', '<p><strong>Our</strong><strong> Vision</strong></p>\r\n\r\n<p>To be recognized and valued as one of best systems integrator in the region with variety fields of choice.</p>\r\n', '1970-01-01', 'VRUclagRAAHI.jpg', '<p><strong>Our</strong><strong> Mission</strong></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>To contribute to local and regional development with technology solutions, commitment and dedicated customer service.</p>\r\n	</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Strive to foster an open, trustworthy and friendly environment conducive to innovation, mutual respect, career development and personal growth achieve staff full potential.</p>\r\n	</li>\r\n	<li>\r\n	<p>Committed to society and environment. Always endeavor to set a standard for others to follow.</p>\r\n	</li>\r\n</ul>\r\n', 0, '2014-12-02 00:00:00', 1, 0, 'en'),
	(28, 5, 'Contact Us', 'Contact Us', 'Contact Us', 'المركز الربٌسً : المملكة العربٌة السعودٌة - المدٌنة المنورة ص. ب : 2495 هاتف : 00966140222290 فرع جدة : فروع تحت الإنشاء : برٌد الكترونً : com.sa-creativeservices@info الموقع الالكترونً : com.sa-creativeservices.ww', NULL, 'W0lAS8LLDpK5.png', '', 0, '2014-12-01 00:00:00', 1, 0, 'en'),
	(29, 2, 'Creative Service Group Has A Web Site', 'Creative Service Group Has A Web Site', 'Creative Service Group Has A Web Site', 'Creative Service Group Has A Web Site Developed By Digital Visions Organization', NULL, 'rHX5mZlHMzg0.jpg', '<p>Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization</p>\r\n\r\n<p>Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization</p>\r\n\r\n<p>Creative Service Group Has A Web Site Developed By Digital Visions Organization</p>\r\n\r\n<p>Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization</p>\r\n\r\n<p>Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization</p>\r\n\r\n<p>Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization</p>\r\n\r\n<p>Creative Service Group Has A Web Site Developed By Digital Visions Organization</p>\r\n\r\n<p>Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization Creative Service Group Has A Web Site Developed By Digital Visions Organization</p>\r\n', 0, '2014-12-01 00:00:00', 1, 0, 'en'),
	(30, 2, 'Electric Service News', 'Electric Service News', 'Electric Service News', 'Electric Service News Electric Service News Electric Service News', NULL, 'WRH88DL8eqnS.jpg', '<p>Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News</p>\r\n\r\n<p>Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News</p>\r\n\r\n<p>Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News Electric Service News</p>\r\n', 0, '2014-12-01 00:00:00', 1, 0, 'en'),
	(31, 2, 'Creative Service Group Big Company', 'Creative Service Group Big Company', 'Creative Service Group Big Company', 'Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company', NULL, 'dBeeQZKOWKiS.jpg', '<p>Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company</p>\r\n\r\n<p>Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company</p>\r\n\r\n<p>Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company</p>\r\n\r\n<p>Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company Creative Service Group Big Company</p>\r\n', 0, '2014-12-01 00:00:00', 1, 0, 'en');
/*!40000 ALTER TABLE `content` ENABLE KEYS */;


-- Dumping structure for table creative_services.content_category
CREATE TABLE IF NOT EXISTS `content_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.content_category: ~5 rows (approximately)
/*!40000 ALTER TABLE `content_category` DISABLE KEYS */;
INSERT INTO `content_category` (`id`, `code`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `content`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'about_madinah', 'عن المدينة المنورة', 'عن المدينة المنورة', 'عن المدينة المنورة', 'عن المدينة المنورة', NULL, 'عن المدينة المنورة', NULL, NULL, 1, 'en'),
	(2, 'news', 'News', 'News', 'News', 'News', NULL, 'News', NULL, NULL, 0, 'en'),
	(3, 'events', 'الفعاليات', 'الفعاليات', 'الفعاليات', 'الفعاليات', NULL, 'الفعاليات', NULL, NULL, 1, 'en'),
	(4, 'new_events', 'جديد الفعاليات', 'جديد الفعاليات', 'جديد الفعاليات', 'جديد الفعاليات', NULL, 'جديد الفعاليات', NULL, NULL, 1, 'en'),
	(5, 'content', 'Creative Services', 'Creative Services', 'Content', 'Creative Services', NULL, 'Creative Services', NULL, NULL, 0, 'en');
/*!40000 ALTER TABLE `content_category` ENABLE KEYS */;


-- Dumping structure for table creative_services.email
CREATE TABLE IF NOT EXISTS `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.email: ~2 rows (approximately)
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
INSERT INTO `email` (`id`, `name`, `email`, `mobile`, `last_update_date`, `last_update_admin_id`, `deleted`) VALUES
	(27, NULL, 'g.elalfy@dv.com.sa', NULL, NULL, NULL, NULL),
	(28, NULL, 'g.elalfy@dv.com', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `email` ENABLE KEYS */;


-- Dumping structure for table creative_services.gallery
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.gallery: ~11 rows (approximately)
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
INSERT INTO `gallery` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `active`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(12, 'المدينة المنورة قديما و حديثا', 'المدينة المنورة قديما و حديثا', 'المدينة المنورة قديما و حديثا', 'بعض الصور للمدينة المنورة', 'drCu1I1Dk4CT.png', 1, '2014-10-21 02:00:00', 1, 0, 'ar'),
	(13, 'المدينة المنورة و الثراث', 'المدينة المنورة و الثراث', 'المدينة المنورة و الثراث', 'بعض الصور التى توضح تراث المدينة المنورة', 'Vr96W7RH0hef.jpg', 1, '2014-10-21 02:00:00', 1, 0, 'ar'),
	(14, 'اثار قديمة', 'اثار قديمة', 'اثار قديمة', 'اثار قديمة', 'Fg6E5ES2hoMC.png', 1, '2014-10-21 02:00:00', 1, 0, 'ar'),
	(15, 'محتوى متنوع', 'محتوى متنوع', 'محتوى متنوع', 'هذا السلايدر يحتوى على جميع الصور التى تستخدم فى الكونتنت', 'x0azJDBSYhob.png', 0, '2014-10-26 00:00:00', 1, 0, 'ar'),
	(16, 'test', 'test', 'test', 'test', 'SGXbhFz2uMvW.jpg', NULL, '2014-10-26 00:00:00', 1, 0, 'ar'),
	(17, 'yyy', 'yyyy', 'yy', 'yyyyyyyyy', 'rz2VKwtsc9aw.jpg', 1, '2014-10-26 00:00:00', 1, 0, 'ar'),
	(18, 'uu', 'uuu', 'uu', 'uuuuuuuuuuu', 'jXEoS6AxgHRk.jpg', 1, '2014-10-26 00:00:00', 1, 1, 'ar'),
	(19, 'mmmmm', 'mmmmmm', 'mm', 'mmmmm', '16CO6hx74bpN.jpg', 1, '2014-10-26 00:00:00', 1, 1, 'ar'),
	(20, 'qqqqqqqq', 'qqqqqqqqq', 'qqq', 'qqqqqqqqqq', '2xjjqTsgtTaB.jpg', 1, '2014-10-26 00:00:00', 1, 1, 'ar'),
	(21, 'aaaaaa', 'aaaaaa', 'a', 'aaaaaaaa', 'VQIifEcRqY30.jpg', 1, '2014-10-26 00:00:00', 1, 0, 'ar'),
	(22, '', '', 'test 1 1', 'test ', 'k17r8cKY4fO0.png', 0, '2014-10-26 00:00:00', 1, 0, 'ar');
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;


-- Dumping structure for table creative_services.gallery_photo
CREATE TABLE IF NOT EXISTS `gallery_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.gallery_photo: ~0 rows (approximately)
/*!40000 ALTER TABLE `gallery_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_photo` ENABLE KEYS */;


-- Dumping structure for table creative_services.header_slider
CREATE TABLE IF NOT EXISTS `header_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `read_count` int(11) NOT NULL DEFAULT '0',
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.header_slider: ~2 rows (approximately)
/*!40000 ALTER TABLE `header_slider` DISABLE KEYS */;
INSERT INTO `header_slider` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `content`, `image`, `active`, `read_count`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(37, 'Network', 'Network', 'Network', 'Creative Service Network', '<p>Creative Service Network</p>\r\n', 'UATjjMPx2FsQ.jpg', 1, 0, NULL, 1, 0, 'en'),
	(38, 'Face Recognition', 'Face Recognition', 'Face Recognition', 'Face Recognition', '', 'tG5tyrgS97qm.jpg', 1, 0, NULL, 1, 0, 'en'),
	(40, 'Electric', 'Electric', 'Electric', 'Electric Power', '<p>Electric Power</p>\r\n', 'glhzzOJX7FqA.jpg', 1, 0, NULL, 1, 0, 'en'),
	(41, 'Raise Floor', 'Raise Floor', 'Raise Floor', 'Raise Floor For Creative', '<p>Raise Floor</p>\r\n', 'OoXiWaYboHyg.jpg', 1, 0, NULL, 1, 0, 'en'),
	(42, 'Security Systems', 'Security Systems', 'Security Systems', 'For Creative Service', '<p>Security Systems</p>\r\n', 'TvPGfmktm3Ft.jpg', 1, 0, NULL, 1, 0, 'en');
/*!40000 ALTER TABLE `header_slider` ENABLE KEYS */;


-- Dumping structure for table creative_services.nav
CREATE TABLE IF NOT EXISTS `nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.nav: ~2 rows (approximately)
/*!40000 ALTER TABLE `nav` DISABLE KEYS */;
INSERT INTO `nav` (`id`, `code`, `title`, `sort`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'main', 'Main', 0, NULL, NULL, 0, 'en'),
	(2, 'welcome', 'Welcome', 0, NULL, NULL, 0, 'en');
/*!40000 ALTER TABLE `nav` ENABLE KEYS */;


-- Dumping structure for table creative_services.nav_link
CREATE TABLE IF NOT EXISTS `nav_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `nav_id` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.nav_link: ~28 rows (approximately)
/*!40000 ALTER TABLE `nav_link` DISABLE KEYS */;
INSERT INTO `nav_link` (`id`, `title`, `summery`, `image`, `link`, `nav_id`, `parent`, `sort`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'الرئيسية', NULL, NULL, '', 1, NULL, 1, '2014-10-14 02:00:00', 1, 1, 'en'),
	(2, 'Home', NULL, NULL, '', 1, NULL, 1, '2014-10-14 02:00:00', 1, 0, 'en'),
	(3, 'About Us', NULL, NULL, '', 1, NULL, 2, '2014-10-14 02:00:00', 1, 0, 'en'),
	(4, 'Creative Service Group', NULL, NULL, 'content/26', NULL, 3, 0, '2014-11-27 00:00:00', 1, 0, 'en'),
	(5, 'Vision And Mission', '', NULL, 'content/27', NULL, 3, 0, '2014-12-02 00:00:00', 1, 0, 'en'),
	(6, 'Our Services', NULL, NULL, '', 1, NULL, 3, '2014-11-27 00:00:00', 1, 0, 'en'),
	(12, 'Our Projects', NULL, NULL, 'project', 1, NULL, 4, '2014-10-21 02:00:00', 1, 0, 'en'),
	(15, 'Our News', '', NULL, 'content_category/2', 1, NULL, 5, '2014-11-30 00:00:00', 1, 0, 'en'),
	(16, 'Contact Us', NULL, NULL, 'contact_us', 1, NULL, 6, '2014-11-27 00:00:00', 1, 0, 'en'),
	(17, 'تحميل ماده من البوابة ', NULL, NULL, NULL, NULL, 10, 0, '2014-10-21 02:00:00', 1, 1, 'ar'),
	(18, '	الهيكل التنظيمي للمركز ', NULL, NULL, '', NULL, 3, 0, '2014-10-21 02:00:00', 1, 1, 'ar'),
	(19, 'الأخبار', NULL, NULL, '', NULL, 3, 0, '2014-10-21 02:00:00', 1, 1, 'ar'),
	(20, 'الفعاليات', NULL, NULL, '', NULL, 3, 0, '2014-10-21 02:00:00', 1, 1, 'ar'),
	(21, 'ارشيف الصور', NULL, NULL, 'gallery', NULL, 3, 0, '2014-10-21 02:00:00', 1, 1, 'ar'),
	(22, 'أرشيف الفيديو', NULL, NULL, 'video', NULL, 3, 0, '2014-10-21 02:00:00', 1, 1, 'ar'),
	(23, 'اتصل بنا', NULL, NULL, '', 1, NULL, 7, '2014-10-27 00:00:00', 1, 1, 'ar'),
	(24, 'المقترحات و الشكاوى', NULL, NULL, 'proposal', NULL, 23, 6, '2014-10-27 00:00:00', 1, 1, 'ar'),
	(25, 'وسائل الاتصال بالمركز', NULL, NULL, '', NULL, 23, 0, '2014-10-27 00:00:00', 1, 1, 'ar'),
	(26, 'visit us', NULL, NULL, 'home', 1, NULL, 0, '2014-11-25 00:00:00', 1, 1, 'en'),
	(27, 'Creative Service Group', 'About Creative Service Group and its fields in Different Services, About Creative Service Group and its fields in Different Services Also What We DO For this company.', 'SCHwG3CnUxPU.jpg', 'content/26', 2, NULL, 0, '2014-12-01 00:00:00', 1, 0, 'en'),
	(28, 'Commercial Services', 'Commercial Services', NULL, 'service_category/13', NULL, 6, 3, '2014-12-01 00:00:00', 1, 0, 'en'),
	(29, 'It & Intelligent System', 'It & Intelligent System', NULL, 'service_category/12', NULL, 6, 2, '2014-12-01 00:00:00', 1, 0, 'en'),
	(30, 'Electrical Installation', 'Electrical Installation', NULL, 'service_category/11', NULL, 6, 1, '2014-12-01 00:00:00', 1, 0, 'en'),
	(31, 'Innovated and Integrated Style', 'Innovated and Integrated Style', NULL, 'service/15', NULL, 28, 0, '2014-12-02 00:00:00', 1, 0, 'en'),
	(34, 'Contact Us', NULL, NULL, 'contact_us', 2, 16, 0, NULL, NULL, 0, 'en'),
	(35, 'Our Branches', NULL, NULL, 'branch', 2, 16, 0, NULL, NULL, 0, 'en'),
	(36, 'Transformer', 'Transformer', NULL, 'service/14', NULL, 28, 0, '2014-12-02 00:00:00', 1, 0, 'en');
/*!40000 ALTER TABLE `nav_link` ENABLE KEYS */;


-- Dumping structure for table creative_services.partner
CREATE TABLE IF NOT EXISTS `partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `summery` varchar(255) DEFAULT NULL,
  `content` text,
  `image` varchar(100) DEFAULT NULL,
  `last_update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(10) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.partner: ~3 rows (approximately)
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `content`, `image`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'AVAYA', 'AVAYA', 'AVAYA', 'AVAYA', '<p>AVAYA</p>\r\n', 'ZYBYn7O7o5tL.jpg', '2014-11-30 16:56:49', NULL, 0, 'en'),
	(2, 'TMain', 'TMain', 'TMain', 'TMain', '<p>TMain</p>\r\n', 'XpY9lAV6qRFb.jpg', '2014-11-30 16:57:14', NULL, 0, 'en'),
	(3, 'Cisco', 'Cisco', 'Cisco', 'Cisco', '<p>Cisco</p>\r\n', '9wKRgygl46Cs.jpg', '2014-11-30 16:57:38', NULL, 0, 'en');
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;


-- Dumping structure for table creative_services.project
CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `summery` varchar(500) DEFAULT NULL,
  `content` text,
  `image` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `last_update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(10) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.project: ~2 rows (approximately)
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` (`id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `content`, `image`, `date`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(27, 'Install Electric With Saudi Electric', 'Install Electric With Saudi Electric', 'Install Electric With Saudi Electric', '<p>Install Electric With Saudi Electric, Install Electric With Saudi Electric Install Electric With Saudi Electric, Install Electric With Saudi ElectricInstall Electric With Saudi Electric, Install Electric With Saudi ElectricInstall Electric With Saudi Electric</p>\r\n', '<p>Install Electric With Saudi Electric, Install Electric With Saudi Electric Install Electric With Saudi Electric, Install Electric With Saudi Electric&nbsp; Install Electric With Saudi Electric, Install Electric With Saudi Electric &nbsp;Install Electric With Saudi Electric, Install Electric With Saudi Electric&nbsp;Install Electric With Saudi Electric, Install Electric With Saudi Electric Install Electric With Saudi Electric, Install Electric With Saudi Electric Install Electric With Saudi Electric, Install Electric With Saudi Electric&nbsp; Install Electric With Saudi Electric, Install Electric With Saudi Electric &nbsp;Install Electric With Saudi Electric, Install Electric With Saudi Electric&nbsp;Install Electric With Saudi Electric, Install Electric With Saudi Electric Install Electric With Saudi Electric, Install Electric With Saudi Electric Install Electric With Saudi Electric, Install Electric With Saudi Electric&nbsp; Install Electric With Saudi Electric, Install Electric With Saudi Electric &nbsp;Install Electric With Saudi Electric, Install Electric With Saudi Electric&nbsp;Install Electric With Saudi Electric, Install Electric With Saudi Electric Install Electric With Saudi Electric, Install Electric With Saudi Electric Install Electric With Saudi Electric, Install Electric With Saudi Electric&nbsp; Install Electric With Saudi Electric, Install Electric With Saudi Electric &nbsp;Install Electric With Saudi Electric, Install Electric With Saudi Electric&nbsp;Install Electric With Saudi Electric, Install Electric With Saudi Electric Install Electric With Saudi Electric, Install Electric With Saudi Electric Install Electric With Saudi Electric, Install Electric With Saudi Electric&nbsp; Install Electric With Saudi Electric, Install Electric With Saudi Electric &nbsp;Install Electric With Saudi Electric, Install Electric With Saudi Electric&nbsp;Install Electric With Saudi Electric, Install Electric With Saudi Electric</p>\r\n', 'aOLluvL3W6JP.jpg', '2014-11-21', '2014-12-02 17:10:10', NULL, 0, 'en'),
	(28, 'Electric With Transform', 'Electric With Transform', 'Electric With Transform', '<p>Electric With Transform Electric With Transform Electric With Transform</p>\r\n\r\n<p>Electric With Transform</p>\r\n\r\n<p>Electric With Transform Electric With Transform</p>\r\n', '<p>Electric With Transform Electric With Transform Electric With Transform</p>\r\n\r\n<p>Electric With Transform</p>\r\n\r\n<p>Electric With Transform Electric With Transform</p>\r\n\r\n<p>Electric With Transform Electric With Transform Electric With Transform</p>\r\n\r\n<p>Electric With Transform</p>\r\n\r\n<p>Electric With Transform Electric With Transform</p>\r\n\r\n<p>Electric With Transform Electric With Transform Electric With Transform</p>\r\n\r\n<p>Electric With Transform</p>\r\n\r\n<p>Electric With Transform Electric With Transform</p>\r\n\r\n<p>Electric With Transform Electric With Transform Electric With Transform</p>\r\n\r\n<p>Electric With Transform</p>\r\n\r\n<p>Electric With Transform Electric With Transform</p>\r\n', 'QPe2QeqlPDAo.jpg', '2014-11-22', '2014-12-02 17:10:34', NULL, 0, 'en');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;


-- Dumping structure for table creative_services.screen
CREATE TABLE IF NOT EXISTS `screen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.screen: ~26 rows (approximately)
/*!40000 ALTER TABLE `screen` DISABLE KEYS */;
INSERT INTO `screen` (`id`, `title`, `code`, `table_name`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(1, 'الرئيسية', '', '', NULL, NULL, 0, 'ar'),
	(2, 'المشاريع', 'project', 'project', NULL, NULL, 1, 'ar'),
	(3, 'تصنيفات المحتوى', 'content_category', 'content_category', NULL, NULL, 0, 'ar'),
	(4, 'المحتوى', 'content', 'content', NULL, NULL, 0, 'ar'),
	(5, 'تصنيفات الخدمات', 'service_category', 'service_category', NULL, NULL, 0, 'ar'),
	(6, 'الخدمات', 'service', 'service', NULL, NULL, 0, 'ar'),
	(7, 'البومات الصور', 'gallery', 'gallery', NULL, NULL, 1, 'ar'),
	(8, 'مواقع المدينة', 'madinah_place', 'madinah_place', NULL, NULL, 1, 'ar'),
	(9, 'اعضاء الشرف', 'member_of_honor', 'member_of_honor', NULL, NULL, 1, 'ar'),
	(10, 'الرعاه الرسميين', 'sponsor', 'sponsor', NULL, NULL, 1, 'ar'),
	(11, 'تصنيفات الفيديوهات', 'video_category', 'video_category', NULL, NULL, 1, 'ar'),
	(12, 'الفيديوهات', 'video', 'video', NULL, NULL, 1, 'ar'),
	(13, 'Home', 'home', '', NULL, NULL, 0, 'en'),
	(14, 'Project', 'project', 'project', NULL, NULL, 0, 'en'),
	(15, 'Content Category', 'content_category', 'content_category', NULL, NULL, 0, 'en'),
	(16, 'Content', 'content', 'content', NULL, NULL, 0, 'en'),
	(17, 'Service Category', 'service_category', 'service_category', NULL, NULL, 0, 'en'),
	(18, 'Service', 'service', 'service', NULL, NULL, 0, 'en'),
	(19, 'البومات الصور', 'gallery', 'gallery', NULL, NULL, 1, 'en'),
	(20, 'مواقع المدينة', 'madinah_place', 'madinah_place', NULL, NULL, 1, 'en'),
	(21, 'اعضاء الشرف', 'member_of_honor', 'member_of_honor', NULL, NULL, 1, 'en'),
	(22, 'الرعاه الرسميين', 'sponsor', 'sponsor', NULL, NULL, 1, 'en'),
	(23, 'تصنيفات الفيديوهات', 'video_category', 'video_category', NULL, NULL, 1, 'en'),
	(24, 'الفيديوهات', 'video', 'video', NULL, NULL, 1, 'en'),
	(73, 'صور الهيدر المتحركة', 'header_slider', 'header_slider', '2014-10-19 12:42:22', NULL, 0, 'ar'),
	(74, 'Header Slider', 'header_slider', 'header_slider', NULL, NULL, 0, 'en');
/*!40000 ALTER TABLE `screen` ENABLE KEYS */;


-- Dumping structure for table creative_services.search_engine
CREATE TABLE IF NOT EXISTS `search_engine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_url` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_url` (`page_url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.search_engine: 0 rows
/*!40000 ALTER TABLE `search_engine` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_engine` ENABLE KEYS */;


-- Dumping structure for table creative_services.service
CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_category_id` int(11) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `summery` text,
  `content` text,
  `image` varchar(100) DEFAULT NULL,
  `last_update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(10) DEFAULT 'ar',
  PRIMARY KEY (`id`),
  KEY `FK_service_service_category` (`service_category_id`),
  CONSTRAINT `FK_service_service_category` FOREIGN KEY (`service_category_id`) REFERENCES `service_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.service: ~6 rows (approximately)
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` (`id`, `service_category_id`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `content`, `image`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(10, 11, 'Electrical Installation In Bdildings', 'Electrical Installation In Bdildings', 'Electrical Installation In Bdildings', 'Electrical Installation In Bdildings', '<p>Electrical Installation In Bdildings</p>\r\n', 'dbFhQiNR37DC.jpg', '2014-12-01 09:46:14', NULL, 0, 'en'),
	(11, 14, 'ATS', 'ATS', 'ATS', '<p>ATS</p>\r\n', '<p>&nbsp;</p>\r\n\r\n<p><span style="color:rgb(0,0,0); font-family:calibri; font-size:15pt">ATS</span><br />\r\n&nbsp;</p>\r\n', 'gyEEyhK0qmVr.jpg', '2014-12-02 12:28:38', NULL, 0, 'en'),
	(12, 11, 'VPS', 'VPS', 'VPS', 'VPS Summery', '<p><span style="color:rgb(0,0,0); font-family:calibri; font-size:15pt">VPS</span></p>\r\n', '3VqRMLHQDx2y.jpg', '2014-12-01 09:48:29', NULL, 0, 'en'),
	(13, 11, 'Electric Power Supply', 'Electric Power Supply', 'Electric Power Supply', 'Electric Power Supply', '<p>Electric Power Supply</p>\r\n', '0FA0s1vWXWmj.jpg', '2014-12-02 09:31:35', NULL, 0, 'en'),
	(14, 13, 'Transformer', 'Transformer', 'Transformer', '<p>Transformer</p>\r\n', '<p>Transformer</p>\r\n', 'aZjXlrjqOPcy.jpg', '2014-12-02 12:28:14', NULL, 0, 'en'),
	(15, 13, 'Innovated and Integrated Style', 'Innovated and Integrated Style', 'Innovated and Integrated Style', '<p>Using the IP integration layer creates a Smart Building Solutions&rsquo; innovation to a higher level. Massive range of products and services can be easily implemented and deployed with capability to get expanded and upgraded whenever required.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p>Currently, we offer full End to End IP Smart Building Solutions; can be monitored and centralized, regardless of the systems numbers, sizes or future expectations.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Using our IP Smart Building Solutions can offer the most cost effective and optimized designs, with following benefits: Lower infrastructure costs.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Y2CroKBM5Sk7.jpg', '2014-12-02 12:26:46', NULL, 0, 'en');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;


-- Dumping structure for table creative_services.service_category
CREATE TABLE IF NOT EXISTS `service_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `seo_meta_keywords` varchar(255) DEFAULT NULL,
  `seo_meta_description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summery` text,
  `image` varchar(255) DEFAULT NULL,
  `content` text,
  `last_update_date` timestamp NULL DEFAULT NULL,
  `last_update_admin_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `lang` varchar(5) DEFAULT 'ar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.service_category: ~6 rows (approximately)
/*!40000 ALTER TABLE `service_category` DISABLE KEYS */;
INSERT INTO `service_category` (`id`, `parent`, `seo_meta_keywords`, `seo_meta_description`, `title`, `summery`, `image`, `content`, `last_update_date`, `last_update_admin_id`, `deleted`, `lang`) VALUES
	(11, 0, 'CONTRACTING Service', 'CONTRACTING Service', 'Electrical Installation', 'CONTRACTING Service Summery Here.', 'SRUfNZWk0OPS.jpg', '<p>CONTRACTING</p>\r\n', '2014-12-01 00:00:00', 1, 0, 'en'),
	(12, 0, 'It & Intelligent System', 'It & Intelligent System', 'It & Intelligent System', 'It & Intelligent System For You.', 'yZqEmyd1Ei3w.jpg', '<p>It &amp; Intelligent System For You. It &amp; Intelligent System For You.</p>\r\n', '2014-12-01 00:00:00', 1, 0, 'en'),
	(13, 0, 'Commercial Services', 'Commercial Services', 'Commercial Services', 'Commercial Services And Sum Of Funds', 'hgJFoO25BtuS.jpg', '<p>Commercial Services And Sum Of Funds, Commercial Services And Sum Of Funds.</p>\r\n', '2014-12-01 00:00:00', 1, 0, 'en'),
	(14, 11, 'Grounding', 'Grounding', 'Grounding', 'Grounding Summery Suspendisse laoreet eu tortor vel dapibus. Etiam auctor nisl mattis, ornare nibh eu, lobortis leo. Sed congue mi eget velit dictum, id dictum massa tempus. Cras lobortis lectus neque. Fusce aliquet mauris ac bibendum pharetra.Grounding Summery Suspendisse laoreet eu tortor vel dapibus. Etiam auctor nisl mattis, ornare nibh eu, lobortis leo. Sed congue mi eget velit dictum, id dictum massa tempus. Cras lobortis lectus neque. Fusce aliquet mauris ac bibendum pharetraGrounding Summery Suspendisse laoreet eu tortor vel dapibus. Etiam auctor nisl mattis, ornare nibh eu, lobortis leo. Sed congue mi eget velit dictum, id dictum massa tempus. Cras lobortis lectus neque. Fusce aliquet mauris ac bibendum pharetra', 'cXe2NtaJp8wx.png', '<p>Grounding Summery Grounding Summery Grounding Summery Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.</p>\r\n', '2014-12-01 00:00:00', 1, 0, 'en'),
	(15, 11, 'SCECO', 'SCECO', 'SCECO', 'SCECO Summery Suspendisse laoreet eu tortor vel dapibus. Etiam auctor nisl mattis, ornare nibh eu, lobortis leo. Sed congue mi eget velit dictum, id dictum massa tempus. Cras lobortis lectus neque. Fusce aliquet mauris ac bibendum pharetra.', 'Ib60GIDRFvIw.jpg', '<p>Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Laravel Recipes by Chuck Heintzelman is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.</p>\r\n', '2014-12-01 00:00:00', 1, 0, 'en'),
	(17, 11, 'sadcsa', 'asda', 'dsfa', '', 'it7IWYkddkY1.jpg', '<p>dsafsq</p>\r\n', '2014-12-01 00:00:00', 1, 1, 'en');
/*!40000 ALTER TABLE `service_category` ENABLE KEYS */;


-- Dumping structure for table creative_services.setting
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook_link` varchar(255) DEFAULT NULL,
  `twitter_link` varchar(255) DEFAULT NULL,
  `youtube_link` varchar(255) DEFAULT NULL,
  `google_plus_link` varchar(255) DEFAULT NULL,
  `instagram_link` varchar(255) DEFAULT NULL,
  `linkedin_link` varchar(255) DEFAULT NULL,
  `address` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table creative_services.setting: ~1 rows (approximately)
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` (`id`, `facebook_link`, `twitter_link`, `youtube_link`, `google_plus_link`, `instagram_link`, `linkedin_link`, `address`) VALUES
	(1, 'https://www.facebook.com/pages', 'https://twitter.com', 'http://www.youtube.com', 'www.googleplus.com', '', 'https://www.linkedin.com', 'Kingdom Of Saudi Arabia, Medina, Sultana Street,24');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
