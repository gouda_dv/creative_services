/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

//By Gouda.
function getURL() { 
    var arr = window.location.href.split("/"); 
    delete arr[arr.length - 1]; 
    delete arr[arr.length - 2]; 
    delete arr[arr.length - 3]; 
    delete arr[arr.length - 4]; 
    delete arr[arr.length - 5];
    return arr.join("/"); 
}
var source_code_url = getURL();
source_code_url = source_code_url.substring(0, source_code_url.length - 4)

$(function () {
    'use strict';
        $('#fileupload').fileupload({
            url: source_code_url+'public/administrator/gallery/uploads/'+ $("#gallery_id").val()
        });

        //alert(source_code_url);
        
    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {

            url: source_code_url+'public/administrator/gallery/uploads/'+ $("#gallery_id").val()+'/' ,
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: source_code_url+'public/administrator/gallery/uploads/'+ $("#gallery_id").val()+'/' ,
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });
    }

});
