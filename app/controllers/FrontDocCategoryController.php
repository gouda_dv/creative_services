<?php

class FrontDocCategoryController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $lang = Session ::get('lang');
        $all_doc = Doc::whereRaw("deleted=0 AND lang= '$lang' AND doc_category_id='$id'")->get(); 

        $doc_category_row = DocCategory::whereRaw("deleted=0 AND id='$id' AND lang= '$lang' ")->get();
		
        return View::make("website.doc_category.show")->with('all_doc', $all_doc)->with('doc_category_row', $doc_category_row);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
    }

}
