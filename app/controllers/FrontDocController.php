<?php

class FrontDocController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {

		$lang = Session ::get('lang');
		if ($lang == null) {
			Session::put('lang', 'ar');
			$lang = Session ::get('lang');
		}
		$latest_content = Content::getByLangAndLimit($lang, 11);

		$doc = Doc::find($id);
		$doc->read_count++;
		$doc->save();

		$doc = Doc::find($id);
		$doc_cat = $doc->doc_category_id;
		$cats = DocCategory::find($doc_cat);
		$doc->doc_category_id = $cats->title;
$seo_meta_keywords= $doc->seo_meta_keywords;
$seo_meta_description=$doc->seo_meta_description;
		return View::make("website.doc.show")->with('doc', $doc)->with('latest_content', $latest_content)
               ->with('seo_meta_keywords', $seo_meta_keywords)
                        ->with('seo_meta_description', $seo_meta_description);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {

	}

	public function open_uploads_popup($id) {
		$lang = Session ::get('lang');
		if ($lang == null) {
			Session::put('lang', 'ar');
			$lang = Session ::get('lang');
		}

		$doc = Doc::find($id);
		return View::make("website.doc.print")->with('doc', $doc);
	}

	public function email() {

		return View::make("website.doc.email");
	}

	public function send_email()
	{

		$doc_id=$_POST['doc_id'];

		$params['message_title']=Lang::get('global.message_title');
		$params['message_body']=Lang::get('global.sending_email_message');
		$params['link_url']=Lang::get('global.link_url');
		$params['url']=url('doc', $parameters = array('doc_id' => $doc_id), $secure = null);
			
		$params['subject']=Lang::get('global.sending_email');
		
		$params_email['to']=$_POST['email'];
		$params_email['subject']=Lang::get('global.sending_email');

		$user_controller=new UserController();
		$params_email['message_body']=$user_controller->send_email_with_view($params);
		//$params_email['headers']=' ';

		$email_controller_object=new EmailController();
		return $email_controller_object->send_email($params_email);
	}

}
