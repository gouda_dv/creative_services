<?php

class FrontQuestionnaireController extends BaseController {

    public function index() {

        $questionnaire = Questionnaire::find(1);
        return View::make('website.master')->with('quetionnaire', $questionnaire);
    }

    public function create() {
        if (Session::has('id')) {
            //  return "ok";exit;
            $user_id = Session::get('id');
            // return $user_id;exit;
            return Redirect::to('website.master')->with('user_id', $user_id);
        } else {
//return"failed";exit;
            $flag = 4;
            return View::make('website.master')->with('flag', $flag);
        }
    }

    public function store() {

        if (Session::has('id')) {

            $ip = $_SERVER['REMOTE_ADDR'];
            //print $ip;exit;
            $vote = new ChoiceIp();
            // print $choice->answer;exit;
            //  $question= new questionnaire();


            $choice = QuestionnaireChoice::whereRaw("deleted=0 AND choice= '" . Input::get('optionsRadios') . "'")->get();
            foreach ($choice as $value) {
                $vote->questionnaire_choice_id = $value['id'];
            }

            //  print $vote->questionnaire_choice_id;exit;
            $vote->last_update_admin_id = Session::get('id');
            $vote->deleted = 0;
            $vote->ip = $ip;
            $vote->questionnaire_id = 1;
            $vote->save();
            return View::make('website.master');
        } else {

            $flag = 4;
            return View::make('website.master')->with('flag', $flag);
        }

//  		$news=User::create(array('title'=>Input::get('title'),'summery'=>Input::get('summery'),'content'=>Input::get('content'),'picture'=>Input::get('picture')));
        // another  way to intiate the user object $news= new USer();   $news->name="";.......etc
    }

    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function vote() {


        $questionnaire_id = $_POST['questionnaire_id'];

        if (isset($_POST['choice_id'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $choice_id = $_POST['choice_id'];
            $vote = new ChoiceIp();
            $vote->last_update_admin_id = Session::get('id');
            $vote->questionnaire_choice_id = $choice_id;
            $vote->questionnaire_id = $questionnaire_id;
            $vote->deleted = 0;
            $vote->ip = $ip;
            $vote->save();
        }

        $query_question_total = DB::table('questionnaire_choice_by_ip')//get total count of all answers to one question
                ->select('*')
                ->where('questionnaire_id', '=', $questionnaire_id)
                ->get();
        $count_question = count($query_question_total);
        //$total=$count_selected_choice / $count_question;


        $query_question_dis = DB::table('questionnaire_choice_by_ip')//get id's of all choices to one question
                ->select('questionnaire_choice_id')
                ->distinct('questionnaire_choice_id')
                ->where('questionnaire_id', '=', $questionnaire_id)
                ->get();


        $iteration = $query_question_dis;       // contains array of all choices of this question.

        $result = array();
        $result[0]=$count_question;
        foreach ($iteration as $iter) {

            $choice_id_ = $iter->questionnaire_choice_id;

            $choice_count = DB::table('questionnaire_choice_by_ip')
                    ->select('*')
                    ->where('questionnaire_id', '=', $questionnaire_id)
                    ->where('questionnaire_choice_id', '=', $choice_id_)
                    ->get();
            $choice_name = QuestionnaireChoice::find($choice_id_);
            $count = count($choice_count);
            $result[$choice_name->choice] = $count ;
        }
        return $result;
    }

    public function already_answerd() {
        $ip = $_SERVER['SERVER_ADDR'];

        $questionnaire_id = $_POST['questionnaire_id'];
        $result = DB::table('questionnaire_choice_by_ip')//get total count of all answers to one question
                ->select('*')
                ->where('questionnaire_id', '=', $questionnaire_id)
                ->where('ip', '=', $ip)
                ->get();

        if (count($result) != 0)
            return "true";
        else
            return "false";
    }

}
