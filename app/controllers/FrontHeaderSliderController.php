<?php

class FrontHeaderSliderController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$lang=Session ::get('lang');
		if($lang==null) {
			Session::put('lang', 'ar');
			$lang=Session ::get('lang');
		}

		//$latest_content = Content::getByLangAndLimit($lang, 11);
		 
		$row = HeaderSlider::find($id);
                 $seo_meta_keywords= $row->seo_meta_keywords;
$seo_meta_description=$row->seo_meta_description;
		return View::make("website.header_slider.show")
		//->with('latest_content', $latest_content)
		->with('row', $row)
                        ->with('seo_meta_keywords', $seo_meta_keywords)
                        ->with('seo_meta_description', $seo_meta_description);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {

	}

    

   
   
}
