<?php

class FrontContactUsController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     public function index() {
         return View::make('website.contact_us.contact_us');
        
    }
    
    public function create() {
   
      return Redirect::to('contact_us');
       
    }

    public function store() {
        $lang = Session ::get('lang');
		if ($lang == null) {
			Session::put('lang', 'en');
			$lang = Session ::get('lang');
		}
            $contact_us = new ContactUs();
            $contact_us->name = Input::get('name');
            $contact_us->email = Input::get('email');
            $contact_us->mobile = Input::get('mobile');
            $contact_us->title = Input::get('title');
            $contact_us->content = Input::get('content');
            $contact_us->lang=Session::get('lang');
            $contact_us->deleted = 0;
            $contact_us->save();
            $params['title']=Lang::get('global.contact_us');
            $params['message']=Lang::get('global.contact_us_message');
           // $flag = 0;
           // Session::put('flag', $flag);
            return View::make('website.success_view')->with('params',$params);
       

    }
     public function show($id) {
    
    }

    public function edit($id) {
 
    }

    public function update($id) {
       
    }
 
     public function destroy($id) {
       
    
    }

    
    
    
}