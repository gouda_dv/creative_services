<?php

class HomeController extends BaseController
{

	public function index()
	{
		
		$lang=Session ::get('lang');
		if($lang==null) {
			Session::put('lang', 'en');
			$lang=Session ::get('lang');
		}

		$sliders = HeaderSlider::whereRaw("deleted=0 AND active=1 AND lang= '$lang' ")->get();
               // $services = Service::whereRaw("deleted=0 AND lang= '$lang' ")->orderBy('id', 'DESC')->take(3)->get();
                $content = Content::whereRaw("deleted=0 AND id=24 AND lang= '$lang' ")->get();
                $project = Project::whereRaw("deleted=0 AND lang= '$lang' ")->orderBy('date', 'ASEC')->get();
                $partner = Partner::whereRaw("deleted=0 AND lang= '$lang' ")->get();
                $services_category = ServiceCategory::whereRaw("deleted=0 AND parent=0 AND lang= '$lang' ")->orderBy('id', 'DESC')->take(3)->get();
                $welcome = NavLink::whereRaw("deleted=0 AND nav_id=2 AND lang= '$lang' ")->take(1)->get();
//                $welcome_text = DB::table('nav_link')
//	->join('content', 'schedules.course_id', '=', 'courses.id')
//        ->get(array('name', 'term', 'units'));
////                ->select('*')
////                ->where('questionnaire_choice_id', '=', $choice_id)
////                ->where('questionnaire_id', '=', $questionnaire_id)
////                ->get();
                
                
                
              //$content = Content::whereRaw("deleted=0 AND lang= '$lang' ")->get();
               // $thumbs=HeaderSlider::whereRaw("deleted=0 AND active=1 AND lang= '$lang' ")->get();
              //  print $lang;exit;
              
		
//		$new_events_category_row = ContentCategory::whereRaw("deleted=0 AND code='events' AND lang= '$lang' ")->get();
//		$new_events_content = Content::getByLangAndCategoryCode($lang, 'events', 3);
//		
//		$news_category_row = ContentCategory::whereRaw("deleted=0 AND code='news' AND lang= '$lang' ")->get();
//		$news_content = Content::getByLangAndCategoryCode($lang, 'news', 3);
//		
//		$last_video = Videos::getLastOneByLangAndCategoryCode($lang, 'home_videos');
//		
//		$new_releases_doc_category_row = DocCategory::whereRaw("deleted=0 AND code='new_releases' AND lang= '$lang' ")->get();
//		$new_releases_doc = Doc::getByLangAndCategoryCode($lang, 'new_releases');
//		
//		$honor_members = HonorMembers::whereRaw("deleted=0 AND lang= '$lang'")->get();
//		$sponsors = Sponsors::whereRaw("deleted=0 AND lang= '$lang'")->get();
		//$related_links = RelatedLinks::whereRaw("deleted=0 AND lang= '$lang'")->get();
		//$last_question = Questionnaire::getLastOneByLang($lang);
		$settings = Settings::find(1);
		
		return View::make('website.home.index')       
		->with('sliders', $sliders)
               // ->with('services', $services)
                ->with('settings',$settings)
                ->with('content',$content)
                ->with('project',$project)
                ->with('services_category',$services_category)
                ->with('welcome',$welcome)   
                ->with('partner',$partner);
                        
//		->with('new_events_category_row', $new_events_category_row)
//		->with('new_events_content', $new_events_content)
//		->with('news_category_row', $news_category_row)
//		->with('news_content', $news_content)
//	
//		->with('last_video', $last_video)
//		
//		->with('new_releases_doc_category_row', $new_releases_doc_category_row)	
//		->with('new_releases_doc', $new_releases_doc)
//		->with('honor_members', $honor_members)
//		->with('sponsors', $sponsors)
		//->with('flag',Session::get('flag'));
                
                
	}



}
