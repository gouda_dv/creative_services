<?php

class FrontVideoController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $lang = Session ::get('lang');
        $all_video = Videos::whereRaw("deleted=0 AND lang= '$lang' ")->get();   
        return View::make("website.video_category.index")->with('all_video', $all_video);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $video =Videos::find($id);
        $video_cat = $video->video_category_id;
        $cats = VideoCategory::find($video_cat);
            $video->video_category_id = $cats->title;

        return View::make("website.video.show")->with('video', $video);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
    }

}
