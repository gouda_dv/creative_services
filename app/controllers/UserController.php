<?php

class UserController extends BaseController {

    public function index() {
        return View::make('website.master');
    }

    public function create() {

        return View::make('website.master');
    }

    public function store() {
      
        $input = Input::all();
        $rules = array(
            'name' => 'required',
            'email' => 'required',
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return Redirect::to('website/user/master')->withErrors($validator);
        }


        $check = User::whereRaw("email='" . Input::get('email') . "'  ")->get();
        $arr = (array) $check;
        $count = count($arr);

        if ($count == 1) {
            $user = new User();
            $user->name = Input::get('name');
            $user->email = Input::get('email');
            $user->password = Input::get('password');
           // $user->mobile = Input::get('mobile');
          // $user->phone = Input::get('phone');
          //  $user->address = Input::get('address');
            $user->deleted = 0;
            $user->active = 0;
            $user->authentication_code = sha1(mt_rand(10000, 99999) . time() . $user->email);
            $user->save();

            $to = $user->email;
            $params=array();
            $params['to']=$to;
            $params['subject']=Lang::get('global.email_subject');
            $params['message_title']=Lang::get('global.message_title');
            $params['message_body']=Lang::get('global.email_activation');
           // $params['headers']=' ';
            $params['link_url']=Lang::get('global.link_url');
            $params['url']=url('user/activate', $parameters = array('auth' => $user->authentication_code), $secure = null);
        
   $headers= "From: Madinah History Center \r\n";
   $headers.= "Content-Type:text/html ; charset=utf-8 " . "\r\n";
    

//            print $params['to'];
//            print $params['subject'];
//           print  $params['message_title'];
//            print   $params['message_body'];
//            print   $params['headers']=' ';
//            print    $params['url']; 
//                exit;
//               
            
           // $subject = 'تاكيد التسجيل';
            
            $message = $this->send_email_with_view($params);
//                    "لتفعيل الحساب يجيب عليك الضغط علي اللينك التالي"
//                    . '"' . url('user/activate', $parameters = array('auth' => $user->authentication_code), $secure = null) . '"';
//            
           // $headers = ' ';
            mail($to, $params['subject'], $message, $headers);
$params['title']=Lang::get('global.continue_reg');
$params['message']=Lang::get('global.act_account');
           // $flag = 0;
           // Session::put('flag', $flag);
            return View::make('website.success_view')->with('params',$params);
        } elseif ($count > 1) {

            $flag = 1;
            Session::put('flag', $flag);
            return Redirect::back();
        }
    }

    public function show($id) {
        $user = User::find($id);

        return View::make('website.user.profile')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $user = User::find($id);
        $user->name = Input::get('name');
      //  $user->email = Input::get('email');
        $user->password = Input::get('password');
        if ($user->address != null && Input::get('address') == null) {
            $user->address = "";
        }

        if ($user->phone != null && Input::get('phone') == null) {
            $user->phone = "";
        }

        if ($user->mobile != null && Input::get('mobile') == null) {
            $user->mobile = "";
        }
        if (Input::get('address')) {
            $user->address = Input::get('address');
        }
        if (Input::get('phone'))
            $user->phone = Input::get('phone');

        if (Input::get('mobile'))
            $user->mobile = Input::get('mobile');

        $user->save();
         $flag = 8;
            Session::put('flag', $flag);
        return View::make('website.user.profile')->with('user', $user)->with('flag',$flag);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function login() {


        return View::make('website.user.master');
    }

    public function validate() {
        $password = Input::get('password');
        // $name = Input::get('name');
        $email = Input::get('email');

        $user = User::whereRaw("email='" . Input::get('email') . "' AND password='" . Input::get('password') . "' ")->get();

        if (!$user->isEmpty()) {

            if ($user[0]->active == 1) {

                foreach ($user as $u) {

                    Session::put('id', $u->id);
                    Session::put('password', $u->password);
                    Session::put('email', $u->email);
                    Session::put('name', $u->name);
                }
         $flag = 6;
         Session::put('flag', $flag);
        return Redirect::back();
//                return View::make('website.sign_in');
            } elseif ($user[0]->active == 0) {
               $params['title']=Lang::get('global.act_firist');
$params['message']=Lang::get('global.act_account');
           // $flag = 0;
           // Session::put('flag', $flag);
            return View::make('website.success_view')->with('params',$params);
            }
        } else {
            //check if user registered if email exist , then show re enter login info.
            $check_register = User::whereRaw("email='" . Input::get('email') . "' ")->get();

            if (!$check_register->isEmpty()) {
                $flag = 2;
                Session::put('flag', $flag);
                return Redirect::back();
            }
            // if count ==0 then user not registered and redirect to registeration form.
            else {
                $flag = 3;
                Session::put('flag', $flag);
                return Redirect::back();
            }
        }
    }

    public function logout() {
        Session::forget('id');
        Session::forget('password');
        Session::forget('email');
        Session::forget('name');

        // $errors[] = "please Login first";
        return Redirect::to('/');
    }

    public function activate($auth) {
        $result = DB::table('user')
                ->select('*')
                ->where('authentication_code', '=', $auth)
                ->get();
        $user = User::find($result[0]->id);
        $user->active = 1;
        $user->save();
        Session::put('id', $user->id);
        Session::put('password', $user->password);
        Session::put('email', $user->email);
        Session::put('name', $user->name);
         //$flag = 7;
         //Session::put('flag', $flag);
        $params = array('title'=>Lang::get('global.activation'),'message'=>Lang::get('global.success_activation') );
        return View::make('website.success_view')->with('params',$params);
    }
    public function check_email_register() {
   //$val=$_POST['token'];
  // print $val;
        
        $email=$_POST['email'];
         $result = DB::table('user')
                ->select('*')
                ->where('email', '=', $email)
                ->get();
         $count=count($result);
    
        return $count;
       //  return View::make('website.test')->with('email', $email);
        
        
    }
    public function send_email_with_view($params){
        $params['copy_right']=Lang::get('global.copy_right');
        $params['powered_by']=Lang::get('powered_by') ;
        $params['com_name']=Lang::get('global.com_name') ;
        $image_link=URL::asset('website_design/images/header.png');
        
   // echo "hello";
//    $params['subject']=Lang::get('global.email_subject');
//     $params['body']=Lang::get('global.email_activation');
     
  //  print_r($params);
   // $context = stream_context_create($params);
    //return Redirect::to('email_template');
  return "
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>النشرة البريدية</title>
<style>
@import url(http://fonts.googleapis.com/earlyaccess/droidarabickufi.css);
 @import url(http://fonts.googleapis.com/earlyaccess/droidarabicnaskh.css);
* {
	margin: 0;
	padding: 0;
	border: 0;
}
html {
	padding: 0;
	margin: 0;
	height: 100%;
	width: 100%;
}
body {
	margin: 0 0 0 0;
	padding: 0 0 0 0;
	direction: rtl;
	font-size: 15px;
	font-family: 'Droid Arabic Naskh', serif;
	text-align: justify;
	font-weight: 700px;
}
.container {
	margin: 0 auto 0 auto;
	width: 970px;
}
.header {
	background: url(".$image_link.") no-repeat;
	width: 970px;
	height: 146px;
	float: right;
	top: 0;
}
.box {
	line-height: 190%;
	width: 950px;
	height: auto;
	float: right;
	color: #232323;
	padding: 10px;
	background: #ffffff;
	background-image: -webkit-gradient( linear, right top, right bottom, color-stop(0.4, #FFFFFF), color-stop(1, #ddd) );
	background-image: -o-linear-gradient(bottom, #FFFFFF 10%, #eee 100%);
	background-image: -moz-linear-gradient(bottom, #FFFFFF 10%, #eee 100%);
	background-image: -webkit-linear-gradient(bottom, #FFFFFF 10%, #eee 100%);
	background-image: -ms-linear-gradient(bottom, #FFFFFF 10%, #eee 100%);
	background-image: linear-gradient(to bottom, #FFFFFF 10%, #eee 100%);
 filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#ffffff', EndColorStr='#eee');
}
.box a {
01573c float: right;
	color: #609686;
	text-decoration: none;
}
.box a:hover {
	float: right;
	color: #01573c;
	text-decoration: none;
}
h1 {
	font-size: 15px;
	font-family: 'Droid Arabic Kufi', serif;
	color: #01573c;
	height: 20px;
	float: right;
	width: 950px;
	padding: 10px 10px 10px 5px;
	font-weight: bold;
	background: #eeeeee;
}
.site-footer {
	background: #01573c;
	width: 970px;
	height: 45px;
	padding: 10px 0 10px 0;
	text-align: center;
	font-family: 'Droid Arabic Kufi', serif;
	font-size: 12px;
	color: #fff;
	float: right;
	-webkit-border-bottom-right-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-bottomright: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-bottom-right-radius: 10px;
	border-bottom-left-radius: 10px;
}
.site-footer a {
	text-decoration: none;
	font-family: 'Droid Arabic Kufi', serif;
	font-size: 12px;
	color: #609686;
}
.site-footer a:hover {
	font-family: 'Droid Arabic Kufi', serif;
	font-size: 12px;
	color: #ddd;
	text-decoration: none;
}
.title {
	font-family: 'Droid Arabic Kufi', serif;
	font-size: 16px;
	color: #222;
	margin-top: 100px;
	margin-left: 120px;
	font-weight: bold;
	float: left;
}
</style>
</head>

<body>
<div class='container'>
  <div class='header'>
    <div class='title'> " . $params["subject"]  ." </div>
  </div>
  <h1>". $params['message_title']." </h1>
  <div class='box'>
    <p>". $params['message_body']." <br/>
      <a href=' ".$params['url']." '> ". $params['link_url'] . "</a> </p>
  </div>
  <div class='site-footer'> © ". $params['copy_right']. "<br/> ";
  $params['powered_by'] . " <a href='' target='_blank'> ". $params['com_name'] ."</a></div>
</div>
</body>
</html>";
   // $homepage = file_get_contents('C:/xampp/htdocs/email_template/email_template.html?params='.$params);
   // echo $homepage;
    
    
}


}
