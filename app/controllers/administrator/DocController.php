<?php

class DocController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
            $lang = Session ::get('lang');
            $all_doc = Doc::whereRaw("deleted=0 AND lang= '$lang' ")->get();
             foreach ($all_doc as $doc) {
                $category = DocCategory::find($doc['doc_category_id']);
                $doc['doc_category_id'] = $category->title;
            }
            return View::make('admin.doc.index')->with('all_doc', $all_doc);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            $lang=Session ::get('lang');
            $categories = DocCategory::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedCategories = array();
            foreach ($categories as $category) {
                $selectedCategories[$category->id] = $category->title;
            }
             $coins = Currency::whereRaw('deleted=0')->get();
            $selectedCoins = array();
             foreach ($coins as $coin) {
                $selectedCoins[0]=Lang::get('global.choose');
                $selectedCoins[$coin->id] = $coin->title;
            }
            return View::make('admin.doc.create')->with("cats", $selectedCategories)
                 ->with("coin", $selectedCoins);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000|required',
                'title' => 'required',
                'file' => 'required',
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/doc/create')->withErrors($validator);
            }
            $doc = new Doc();
            $image = Input::file('image');
            $file = Input::file('file');
            $video=Input::file('video');
            if ($image) {
                $destinationPath = 'uploads/doc/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    $doc->image = $filename_image;
                } else {
                    return Redirect::to('administrator/doc/create')->withErrors("can't save");
                }
            }
            if ($file) {
                $destinationPath = 'uploads/doc/';
                $extension_file = $file->getClientOriginalExtension();
                $filename_file = str_random(12) . "." . $extension_file;
                $upload_success_file = Input::file('file')->move($destinationPath, $filename_file);
                if ($upload_success_file) {
                    $doc->file = $filename_file;
                } else {
                    return Redirect::to('administrator/doc/create')->withErrors("can't save");
                }
            }
            if ($video) {
                          $destinationPath = 'uploads/doc/';
                $extension_video = $video->getClientOriginalExtension();
                $videoname = str_random(12) . "." . $extension_video;
                $upload_success_video = Input::file('video')->move($destinationPath, $videoname);
                if ($upload_success_video) {
                    $doc->video = $videoname;
                } else {
                    return Redirect::to('administrator/doc/create')->withErrors("can't save");
                }
            }

            $doc->title = Input::get('title');
            $doc->doc_category_id = Input::get('doc_category_id');
            $doc->seo_meta_keywords = Input::get('seo_meta_keywords');
            $doc->seo_meta_description = Input::get('seo_meta_description');
            $doc->summery = Input::get('summery');
            $doc->content = Input::get('content');
            $doc->file_paid = Input::get('file_paid');
            $doc->file_price = Input::get('file_price');
            $doc->file_currency_id = Input::get('file_currency_id');
            $doc->youtube_link = Input::get('youtube_link');
            $doc->video_paid = Input::get('video_paid');
            $doc->video_price = Input::get('video_price');
            $doc->video_currency_id = Input::get('video_currency_id');
             $paid_file = Input::get('file_paid');
             
                if ($paid_file == 1) {
                    $doc->file_paid = 1;
                } else {
                    $doc->file_paid = 0;
                }
                 $paid_video = Input::get('video_paid');
             
                if ($paid_video == 1) {
                    $doc->video_paid = 1;
                } else {
                    $doc->video_paid = 0;
                }
            $doc->last_update_date = date("Y-m-d");
            $doc->last_update_admin_id = Session::get('admin_id');
            $doc->deleted = 0;
            $doc->lang = Session::get('lang');
            $doc->save();
            return Redirect::to('administrator/doc');
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (Session::has('admin_name')) {
             $lang=Session ::get('lang');
            $categories = DocCategory::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedCategories = array();
            foreach ($categories as $category) {
                $selectedCategories[$category->id] = $category->title;
            }
             $coins = Currency::whereRaw('deleted=0')->get();
            $selectedCoins = array();
            foreach ($coins as $coin) {
                $selectedCoins[0]=Lang::get('global.choose');
                $selectedCoins[$coin->id] = $coin->title;
            }
            
            $doc = Doc::find($id);
            return View::make("admin.doc.create")->with('doc', $doc)->with("cats", $selectedCategories)
                    ->with("coin", $selectedCoins);
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000',
                'title' => 'required',
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/doc/' . Input::get('id') . '/edit')->withErrors($validator);
            }
            $doc = Doc::find($id);

            $image = Input::file('image');
            $file = Input::file('file');
             $video = Input::file('video');
            if ($image) {
                $destinationPath = 'uploads/doc/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    File::delete('uploads/doc/' . $doc->image);
                    $doc->image = $filename_image;
                } else {
                    return Redirect::to('administrator/doc/create')->withErrors("can't save");
                }
            }
            if ($file) {
                $destinationPath = 'uploads/doc/';
                $extension_file = $file->getClientOriginalExtension();
                $filename_file = str_random(12) . "." . $extension_file;
                $upload_success_file = Input::file('file')->move($destinationPath, $filename_file);
                if ($upload_success_file) {
                    File::delete('uploads/doc/' . $doc->file);
                    $doc->file = $filename_file;
                } else {
                    return Redirect::to('administrator/doc/create')->withErrors("can't save");
                }
            }
             if ($video) {
                $destinationPath = 'uploads/doc/';
                $extension_video = $video->getClientOriginalExtension();
                $filename_video = str_random(12) . "." . $extension_video;
                $upload_success_video = Input::file('file')->move($destinationPath, $filename_video);
                if ($upload_success_video) {
                    File::delete('uploads/doc/' . $doc->video);
                    $doc->video = $filename_video;
                } else {
                    return Redirect::to('administrator/doc/create')->withErrors("can't save");
                }
            }

            $doc->title = Input::get('title');
            $doc->doc_category_id = Input::get('doc_category_id');
            $doc->seo_meta_keywords = Input::get('seo_meta_keywords');
            $doc->seo_meta_description = Input::get('seo_meta_description');
            $doc->summery = Input::get('summery');
            $doc->content = Input::get('content');
            $doc->last_update_date = date("Y-m-d");
             $doc->youtube_link = Input::get('youtube_link');
             $doc->video_price=Input::get('video_price');
              $doc->video_currency_id=Input::get('video_currency_id');
            $doc->file_price = Input::get('file_price');
            $doc->file_currency_id=Input::get('file_currency_id');
                     $doc->video_paid=Input::get('video_paid');
                      if (isset($doc->video_paid)) {
                        $doc->video_paid = 1;
                    } else {
                        $doc->video_paid = 0;
                    }
            $doc->file_paid = Input::get('file_paid');
                    if (isset($doc->file_paid)) {
                        $doc->file_paid = 1;
                    } else {
                        $doc->file_paid = 0;
                    }
            
            $doc->last_update_admin_id = Session::get('admin_id');
            $doc->deleted = 0;
            $doc->lang = Session::get('lang');
            $doc->save();
            return Redirect::to('administrator/doc');
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $doc = Doc::find($id);
        $doc->deleted = 1;
        $doc->save();
        return Redirect::to('administrator/doc');
    }

}
