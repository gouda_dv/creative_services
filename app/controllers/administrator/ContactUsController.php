<?php

class ContactUsController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     public function index() {
         if(Session::has('admin_name')){
             $lang=Session ::get('lang');
           //  $condition[]="deleted=0 AND lang= '$lang' ";
             //print $condition;exit;
           //  $cond=implode(',',$condition);
           //  print $cond;exit;
        $contact_us = ContactUs::whereRaw("deleted=0 AND lang= '$lang' ")->orderBy('id','ASEC')->get();
        return View::make('admin.contact_us.index')->with('contact_us', $contact_us);
         }
         else
             {
             return Redirect::to('administrator/admin/login');
             }
    }
    
    public function create() {
         if(Session::has('admin_name')){
        return View::make('admin.contact_us.create');
         }
         else
         {
             return Redirect::to('administrator/admin/login');
         }
    }

    public function store() {
         if(Session::has('admin_name')){
    $input = Input::all();

        $rules = array(

            'name' => 'required',

            'email' => 'required',

            'title' => 'required',

            'content' => 'required',

            

           

        );



        $validator = Validator::make($input, $rules);



        if ($validator->fails()) {

            $messages = $validator->messages();



            // redirect our user back to the form with the errors from the validator

            return Redirect::to('administrator/contact_us/create')->withErrors($validator);

        }


            $contact_us = new ContactUs();           
            $contact_us->name = Input::get('name');
            $contact_us->email = Input::get('email');
            $contact_us->mobile=Input::get('mobile');
            $contact_us->title=Input::get('title');
            $contact_us->content=Input::get('content');
             $contact_us->lang=Session::get('lang');
           // $proposal->user_id=1;
            $contact_us->deleted = 0;
            $contact_us->save();
            return Redirect::to('administrator/contact_us');
       
       
        }
         else
             {
             return Redirect::to('administrator/admin/login');
             }

//  		$news=User::create(array('title'=>Input::get('title'),'summery'=>Input::get('summery'),'content'=>Input::get('content'),'picture'=>Input::get('picture')));
        // another  way to intiate the user object $news= new USer();   $news->name="";.......etc
    }
     public function show($id) {
        //
    }

    public function edit($id) {
 if(Session::has('admin_name')){
        $contact_us = ContactUs::find($id);
        return View::make("admin.contact_us.create")->with('contact_us', $contact_us);
 }else{
      return Redirect::to('administrator/admin/login');
 }
    }

    public function update($id) {
        if(Session::has('admin_name')){
       $input = Input::all();

       $rules = array(

           'name' => 'required',

            'email' => 'required',

            'title' => 'required',

            'content' => 'required',   

        );

        $validator = Validator::make($input, $rules);



        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::to('administrator/contact_us/' . $id . '/edit')->withErrors($validator);

        }


            $contact_us = ContactUs::find($id);
             $contact_us->name = Input::get('name');
             $contact_us->email = Input::get('email');
            $contact_us->mobile = Input::get('mobile');
            $contact_us->title=Input::get('title');
            $contact_us->content=Input::get('content');
            //$proposal->user_id=1;
            $contact_us->deleted = 0;
            $contact_us->save();
                return Redirect::to('administrator/contact_us');
            }
       
        else
            {
            return Redirect::to('administrator/admin/login');
            }
    }
 
     public function destroy($id) {
        $contact_us = ContactUs::find($id);
        $contact_us->deleted = 1;
         
         $contact_us->save();
        // print "<script type=\"text/javascript\">alert('تم الحذف بنجاح');</script>";
        // redirect
       // Session::flash('message', 'Successfully deleted !');
      return Redirect::to('administrator/contact_us');
    
    }

    
    
    
}