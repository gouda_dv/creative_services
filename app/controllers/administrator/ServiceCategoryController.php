<?php

class ServiceCategoryController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
            $lang = Session ::get('lang');
            $all_content = ServiceCategory::whereRaw("deleted=0 AND lang= '$lang' ")->get();
foreach ($all_content as $content) {
   if($content['parent'] ==0){
       $content['parent']=Lang::get('global.not_found');
   }else{
       $all = DB::table('service_category')->where('deleted', '=', 0)->Where('lang', $lang)->Where('parent', '=', 0)->Where('id',$content['parent'] )->get();
   foreach($all as $contents){
    // print $content->title;
    $content->parent=$contents->title;
    
   }
          
      } 
       
       
   }
        


            return View::make('admin.service_category.index')->with('all_content', $all_content);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            $lang=Session ::get('lang');
            $categories = ServiceCategory::whereRaw("deleted=0 AND lang= '$lang' AND parent=0 ")->get();
            $selectedCategories = array();
            $selectedCategories['']=Lang::get('global.choose');
            foreach ($categories as $category) {
                $selectedCategories[$category->id] = $category->title;
            }
            return View::make('admin.service_category.create')->with("cats", $selectedCategories);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000',
                'title' => 'required',
               
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/service_category/create')->withErrors($validator);
            }
            $image = Input::file('image');
            if ($image) {
                $destinationPath = 'uploads/service_category/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    $content = new ServiceCategory();
                    $content->title = Input::get('title');
                    $content->parent = Input::get('parent');
                    $content->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $content->seo_meta_description = Input::get('seo_meta_description');
                    $content->summery = Input::get('summery');
                    $content->content = Input::get('content');
                    $content->image = $filename_image;
                    $content->last_update_date = date("Y-m-d");
                    $content->last_update_admin_id = Session::get('admin_id');
                    $content->deleted = 0;
                     $content->lang=Session::get('lang');
                    $content->save();
                    return Redirect::to('administrator/service_category');
                } else {
                    return Redirect::to('administrator/service_category/create')->withErrors("can't save");
                }
            } else {
                $content = new ServiceCategory();
                $content->title = Input::get('title');
                $content->parent = Input::get('parent');
                $content->seo_meta_keywords = Input::get('seo_meta_keywords');
                $content->seo_meta_description = Input::get('seo_meta_description');
                $content->summery = Input::get('summery');
                $content->content = Input::get('content');
                $content->last_update_date = date("Y-m-d");
                $content->last_update_admin_id = Session::get('admin_id');
                $content->deleted = 0;
                 $content->lang=Session::get('lang');
                $content->save();
                return Redirect::to('administrator/service_category');
            }
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (Session::has('admin_name')) {
             $lang=Session ::get('lang');
            $categories = ServiceCategory::whereRaw("deleted=0 AND lang= '$lang' AND parent=0 ")->get();
            $selectedCategories = array();
            $selectedCategories['']=Lang::get('global.choose');
            foreach ($categories as $category) {
                $selectedCategories[$category->id] = $category->title;
            }
            $content = ServiceCategory::find($id);
            return View::make("admin.service_category.create")->with('content', $content)->with("cats", $selectedCategories);
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000',
                'title' => 'required',
               
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/service_category/' . Input::get('id') . '/edit')->withErrors($validator);
            }
            $image = Input::file('image');
            if ($image) {
                $destinationPath = 'uploads/service_category/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    $content = ServiceCategory::find($id);
                    File::delete('uploads/service_category/'.$content->image);
                    $content->title = Input::get('title');
                    $content->parent = Input::get('parent');
                    $content->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $content->seo_meta_description = Input::get('seo_meta_description');
                    $content->summery = Input::get('summery');
                    $content->content = Input::get('content');
                    $content->image = $filename_image;
                    $content->last_update_date = date("Y-m-d");
                    $content->last_update_admin_id = Session::get('admin_id');
                    $content->deleted = 0;
                    $content->save();
                    return Redirect::to('administrator/service_category');
                } else {
                    return Redirect::to('administrator/service_category/' . Input::get('id') . '/edit')->withErrors("can't save");
                }
            } else {
                $content = ServiceCategory::find($id);
                $content->title = Input::get('title');
                $content->parent = Input::get('parent');
                $content->seo_meta_keywords = Input::get('seo_meta_keywords');
                $content->seo_meta_description = Input::get('seo_meta_description');
                $content->summery = Input::get('summery');
                $content->content = Input::get('content');
                $content->last_update_date = date("Y-m-d");
                $content->last_update_admin_id = Session::get('admin_id');
                $content->deleted = 0;
                $content->save();
                return Redirect::to('administrator/service_category');
            }
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $content = ServiceCategory::find($id);
        $content->deleted = 1;
        $content->save();
        return Redirect::to('administrator/service_category');
    }

}
