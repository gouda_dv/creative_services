<?php

class QuestionnaireChoiceController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
              $lang=Session ::get('lang');
            $all_questions = Questionnaire::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            return View::make('admin.question_choice.index')->with('all_questions', $all_questions);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            $questions = Questionnaire::whereRaw('deleted=0')->get();
            $selectedQuestions = array();
            foreach ($questions as $question) {
                $selectedQuestions[$question->id] = $question->question;
            }
            return View::make('admin.question_choice.create')->with("questions", $selectedQuestions);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        if (Session::has('admin_name')) {
            $input = Input::all();
            //    print_r($input);exit;
            $rules = array(
                'newchoice[]' => 'required'
            );
            $messages = array(
                'newchoice[].required' => 'choice field is required'
            );

           $validator = Validator::make($input, $rules, $messages);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/question_choice/create')->withErrors($validator);
            }

            foreach ($input['newchoice'] as $choice) {
                $question_choice = new QuestionnaireChoice();
                $question_choice->questionnaire_id = Input::get('questions');
                $question_choice->choice = $choice;
                $question_choice->seo_meta_keywords = Input::get('seo_meta_keywords');
                $question_choice->seo_meta_description = Input::get('seo_meta_description');
                $question_choice->last_update_date = date("Y-m-d");
                $question_choice->last_update_admin_id = Session::get('admin_id');
                $question_choice->deleted = 0;
                 $question_choice->lang=Session::get('lang');
                $question_choice->save();
            }

            return Redirect::to('administrator/question_choice');
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (Session::has('admin_name')) {

            $question_details = DB::table('questionnaire_choice')->where('questionnaire_id', $id)->where('deleted', "0")->select('questionnaire_id', 'seo_meta_keywords', 'seo_meta_description')->first();
            $choice_details = DB::table('questionnaire_choice')->where('questionnaire_id', $id)->select('id', 'choice')->get();
            //print_r($question_details); print_r($choice_details);exit;

            $questions = Questionnaire::whereRaw('deleted=0')->get();
            $selectedQuestions = array();
            foreach ($questions as $question) {
                $selectedQuestions[$question->id] = $question->question;
            }
            return View::make('admin.question_choice.create')->with("questions", $selectedQuestions)->with("question_details", $question_details)->with("choice_details", $choice_details);
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();
//            $rules = array(
//                'image' => 'image|max:8000',
//                'title' => 'required',
//                'content_category_id' => 'required'
//            );
//
//            $validator = Validator::make($input, $rules);
//
//            if ($validator->fails()) {
//                $messages = $validator->messages();
//                return Redirect::to('administrator/question/' . Input::get('id') . '/edit')->withErrors($validator);
//            }
//            print_r($input['oldchoice']);
//            print_r($input['newchoice']);
//            exit;
            foreach ($input['oldchoice']as $key => $choice) {
                $question_choice = QuestionnaireChoice::find($key);
                $question_choice->questionnaire_id = $id;
                $question_choice->choice = $choice;
                $question_choice->seo_meta_keywords = Input::get('seo_meta_keywords');
                $question_choice->seo_meta_description = Input::get('seo_meta_description');
                $question_choice->last_update_date = date("Y-m-d");
                $question_choice->last_update_admin_id = Session::get('admin_id');
                $question_choice->deleted = 0;
                $question_choice->save();
            }
            if (isset($input['newchoice'])) {
                foreach ($input['newchoice'] as $key => $choice) {
                    $question_choice = new QuestionnaireChoice();
                    $question_choice->questionnaire_id = $id;
                    $question_choice->choice = $choice;
                    $question_choice->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $question_choice->seo_meta_description = Input::get('seo_meta_description');
                    $question_choice->last_update_date = date("Y-m-d");
                    $question_choice->last_update_admin_id = Session::get('admin_id');
                    $question_choice->deleted = 0;
                    $question_choice->save();
                }
            }
            return Redirect::to('administrator/question_choice');
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        echo 'hiiiiiii'.$id;exit;
            $choice = QuestionnaireChoice::find($id);
            $choice->deleted = 1;
            $choice->save();
        
    }

}
