<?php

class BranchController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
            $lang = Session ::get('lang');
            $all_branches = Branch::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            return View::make('admin.branch.index')->with('all_branches', $all_branches);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            $lang = Session ::get('lang');
             return View::make('admin.branch.create');
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(              
                'title' => 'required',
                'map_Latitude' => 'required',
                'map_longitude' => 'required',
                'summery'=>'required',               
            );
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/branch/create')->withErrors($validator);
            }
            $branch = new Branch();
            $branch->title = Input::get('title');
            $branch->seo_meta_keywords = Input::get('seo_meta_keywords');
            $branch->seo_meta_description = Input::get('seo_meta_description');
            $branch->summery = Input::get('summery');
            $branch->content = Input::get('content');
            $branch->map_Latitude = Input::get('map_Latitude');
            $branch->map_longitude = Input::get('map_longitude');
            $branch->last_update_date = date("Y-m-d");
            $branch->last_update_admin_id = Session::get('admin_id');
            $branch->deleted = 0;
            $branch->lang = Session::get('lang');
            $branch->save();
            return Redirect::to('administrator/branch');
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (Session::has('admin_name')) {
            $lang = Session ::get('lang'); 
             $branch = Branch::find($id);
             return View::make('admin.branch.create')->with('branch',$branch);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
               
                'title' => 'required',
                'map_Latitude' => 'required',
                'map_longitude' => 'required',
               'summery'=>'required',
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/branch/create')->withErrors($validator);
            }
            $branch = Branch::find($id);                     
            $branch->title = Input::get('title');
            $branch->seo_meta_keywords = Input::get('seo_meta_keywords');
            $branch->seo_meta_description = Input::get('seo_meta_description');
            $branch->summery = Input::get('summery');
            $branch->content = Input::get('content');
            $branch->map_Latitude = Input::get('map_Latitude');
            $branch->map_longitude = Input::get('map_longitude');         
            $branch->last_update_date = date("Y-m-d");
            $branch->last_update_admin_id = Session::get('admin_id');
            $branch->deleted = 0;
            $branch->save();
            return Redirect::to('administrator/branch');
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $branch = Branch::find($id);
        $branch->deleted = 1;
        $branch->save();
        return Redirect::to('administrator/branch');
    }

}
