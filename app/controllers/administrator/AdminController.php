<?php

class AdminController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
            Session::put('lang', 'ar');
            return Redirect::to('administrator/header_slider');
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    /*            khaled defined function  */

    public function login() {
        return View::make('admin.login');
    }

    public function validate() {
        $password = md5(Input::get('password'));
        $admin_name = Input::get('admin_name');
        $admin = Admin::find(1);
      //  print_r($password);exit;

        if ($admin->username == $admin_name && $admin->password == $password) {
            
            Session::put('admin_name', $admin->name);
            Session::put('admin_id', $admin->id);
            Session::put('admin_image', $admin->image);
            
            $lang=Session ::get('lang');
            if($lang==null) {
            	Session::put('lang', 'en');  
            }
            return Redirect::to('administrator/header_slider');

        }

        $errors[] = "please insert valid name and password";
        return Redirect::to('administrator/admin/login')->withErrors($errors);
    }

    public function logout() {
        Session::forget('admin_id');
        Session::forget('admin_name');
        Session::forget('admin_image');
        $errors[] = "please Login first";
        return Redirect::to('administrator/admin/login')->withErrors($errors);
    }

}
