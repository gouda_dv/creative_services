<?php

class GalleryController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
            $lang = Session ::get('lang');
            $all_galleries = Gallery::whereRaw("deleted=0 AND lang= '$lang' ")->get();

            return View::make('admin.gallery.index')->with('all_galleries', $all_galleries);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            return View::make('admin.gallery.create');
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000|required',
                'title' => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/gallery/create')->withErrors($validator);
            }
            $image = Input::file('image');

            $destinationPath = 'uploads/gallery/';
            $extension_image = $image->getClientOriginalExtension();
            $filename_image = str_random(12) . "." . $extension_image;
            $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);

            if ($upload_success_image) {
                $gallery = new Gallery();
                $gallery->title = Input::get('title');
                $gallery->seo_meta_keywords = Input::get('seo_meta_keywords');
                $gallery->seo_meta_description = Input::get('seo_meta_description');
                $gallery->summery = Input::get('summery');
                $active = Input::get('active');
                if ($active == 1) {
                    $gallery->active = 1;
                } else {
                    $gallery->active = 0;
                }

                $gallery->image = $filename_image;
                $gallery->last_update_date = date("Y-m-d");
                $gallery->last_update_admin_id = Session::get('admin_id');
                $gallery->deleted = 0;
                $gallery->lang = Session::get('lang');
                $gallery->save();
                return Redirect::to('administrator/gallery');
            } else {
                return Redirect::to('administrator/gallery/create')->withErrors("can't save");
            }
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (Session::has('admin_name')) {
            $gallery = Gallery::find($id);
            return View::make("admin.gallery.create")->with('gallery', $gallery);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000',
                'title' => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/gallery/' . Input::get('id') . '/edit')->withErrors($validator);
            }
            $image = Input::file('image');
            if ($image) {
                $destinationPath = 'uploads/gallery/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    $gallery = Gallery::find($id);
                    File::delete('uploads/gallery/' . $gallery->image);
                    $gallery->title = Input::get('title');
                    $gallery->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $gallery->seo_meta_description = Input::get('seo_meta_description');
                    $gallery->summery = Input::get('summery');
                    $gallery->active = Input::get('active');
                    if (isset($gallery->active)) {
                        $gallery->active = 1;
                    } else {
                        $gallery->active = 0;
                    }

                    $gallery->image = $filename_image;
                    $gallery->last_update_date = date("Y-m-d");
                    $gallery->last_update_admin_id = Session::get('admin_id');
                    $gallery->deleted = 0;
                    $gallery->save();
                    return Redirect::to('administrator/gallery');
                } else {
                    return Redirect::to('administrator/gallery/' . Input::get('id') . '/edit')->withErrors("can't save");
                }
            } else {
                $gallery = Gallery::find($id);
                $gallery->title = Input::get('title');
                $gallery->seo_meta_keywords = Input::get('seo_meta_keywords');
                $gallery->seo_meta_description = Input::get('seo_meta_description');
                $gallery->summery = Input::get('summery');
                $gallery->active = Input::get('active');
                if (isset($gallery->active)) {
                    $gallery->active = 1;
                } else {
                    $gallery->active = 0;
                }
                $gallery->last_update_date = date("Y-m-d");
                $gallery->last_update_admin_id = Session::get('admin_id');
                $gallery->deleted = 0;
                $gallery->save();
                return Redirect::to('administrator/gallery');
            }
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $gallery = Gallery::find($id);
        $gallery->deleted = 1;
        $gallery->save();
        return Redirect::to('administrator/gallery');
    }

    public function open_uploads_popup($id) {
        if (Session::has('admin_name')) {
            if (isset($_REQUEST['file'])) {
                $filename = public_path() . '/uploads/gallery/' . $id . '/' . $_REQUEST['file'];
                $filename_thumbnail = public_path() . '/uploads/gallery/' . $id . '/thumbnail/' . $_REQUEST['file'];

                if (File::exists($filename)) {
                    File::delete($filename);
                }
                if (File::exists($filename_thumbnail)) {
                    File::delete($filename_thumbnail);
                }
                return Redirect::to('administrator/gallery/open_uploads_popup/' . $id, 303)->withInput();
            }
            $gallery = Gallery::find($id);
            return View::make("admin.gallery.uploads")->with('gallery', $gallery);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    public function uploads($id) {
        if (Session::has('admin_name')) {
            include(public_path() . '/jQuery_File_Upload_9_8_0/server/php/UploadHandler.php');
            $options = array('gallery_id' => $id, 'upload_dir' => 'uploads/gallery/' . $id . '/');
            $upload_handler = new UploadHandler($options);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

}
