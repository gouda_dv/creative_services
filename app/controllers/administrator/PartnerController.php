<?php

class PartnerController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
         if (Session::has('admin_name')) {
               $lang=Session ::get('lang');
        $partner = Partner::whereRaw("deleted=0 AND lang= '$lang' ")->get();
        return View::make('admin.partner.index')->with('partner', $partner);
         }
         else
         {
             
            return Redirect::to('administrator/admin/login');
         }
    }

    public function create() {
         if (Session::has('admin_name')) {
        return View::make('admin.partner.create');
         }
         else
         {
               return Redirect::to('administrator/admin/login');
         }
    }

    public function store() {
           if (Session::has('admin_name')) {
        $input = Input::all();
        $rules = array(
            'image' => 'image|max:8000 | required',
            'title' => 'required',
            
           
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return Redirect::to('administrator/partner/create')->withErrors($validator);
        }

        $file = Input::file('image');
        if($file){
        $destinationPath = 'uploads/partner';
// If the uploads fail due to file system, you can try doing public_path().'/uploads' 
//$filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $filename = str_random(12) . "." . $extension;
        $upload_success = Input::file('image')->move($destinationPath, $filename);

        if ($upload_success) {
            $partner = new Partner();
            $partner->seo_meta_keywords = Input::get('seo_meta_keywords');
            $partner->seo_meta_description = Input::get('seo_meta_description');
            $partner->title = Input::get('title');
            $partner->summery = Input::get('summery');
             $partner->content = Input::get('content');
            
            $partner->image = $filename;
             $partner->deleted = 0;
             $partner->lang=Session::get('lang');
          
            $partner->save();
            return Redirect::to('administrator/partner');
        } else {
            return Redirect::to('administrator/partner/create')->withErrors("can't save");
        }
           }else
               {
                 $partner = new Partner();
            $partner->seo_meta_keywords = Input::get('seo_meta_keywords');
            $partner->seo_meta_description = Input::get('seo_meta_description');
            $partner->title = Input::get('title');
            $partner->summery = Input::get('summery');
             $partner->content = Input::get('content');
               $partner->deleted = 0;
               $partner->lang=Session::get('lang');
            $partner->save();
            return Redirect::to('administrator/partner');
               
               
               }
           }
           else
           {
               return Redirect::to('administrator/admin/login');
           }

//  		$news=User::create(array('title'=>Input::get('title'),'summery'=>Input::get('summery'),'content'=>Input::get('content'),'picture'=>Input::get('picture')));
        // another  way to intiate the user object $news= new USer();   $news->name="";.......etc
    }
     public function show($id) {
        //
    }
    public function edit($id) {
  if (Session::has('admin_name')) {
       
        $partner = Partner::find($id);
        return View::make("admin.partner.create")->with('partner', $partner);
  }
  else
  {
      return Redirect::to('administrator/admin/login');
  }
    }

    public function update($id) {
         if (Session::has('admin_name')) {
        $input = Input::all();
        $rules = array(
            'image' => 'image|max:8000',
            'title' => 'required',
            
           
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('administrator/partner/' . $id . '/edit')->withErrors($validator);
        }

        $file = Input::file('image');
       
        if ($file ) {
            $destinationPath = 'uploads/partner';
            $extension = $file->getClientOriginalExtension();
            $filename = str_random(12) . "." . $extension;
            $upload_success = Input::file('image')->move($destinationPath, $filename);
            if ($upload_success) {
                 $partner = Partner::find($id);
                $partner->seo_meta_keywords = Input::get('seo_meta_keywords');
                $partner->seo_meta_description = Input::get('seo_meta_description');
                $partner->title = Input::get('title');
                $partner->summery = Input::get('summery');
                $partner->content = Input::get('content');
                $old_image=$partner->image;
                if($old_image != $filename ){
                     $partner->image = $filename;
                     if($old_image !="")
                     {
                         unlink('uploads/partner/'.$old_image);
                     }
                }else{
                $partner->image = $filename;
                }
                  $partner->deleted = 0;
                
                $partner->save();
                return Redirect::to('administrator/partner');
            }
        }
       $partner = Partner::find($id);
        $partner->seo_meta_keywords = Input::get('seo_meta_keywords');
        $partner->seo_meta_description = Input::get('seo_meta_description');
        $partner->title = Input::get('title');
        $partner->summery = Input::get('summery');
          $partner->content = Input::get('content');
            $partner->deleted = 0;
        $partner->save();
        return Redirect::to('administrator/partner');
    }
    else 
    {
        return Redirect::to('administrator/admin/login'); 
    }
    }
    
 
     public function destroy($id) {
        $partner = Partner::find($id);
        $partner->deleted = 1;
        $partner->save();

        // redirect
        //Session::flash('message', 'Successfully deleted !');
        return Redirect::to('administrator/partner');
    }

}
