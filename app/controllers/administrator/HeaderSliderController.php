<?php

class HeaderSliderController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
             $lang=Session ::get('lang');
            $slider = HeaderSlider::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            return View::make('admin.header_slider.index')->with('slider_images', $slider);
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            return View::make('admin.header_slider.create');
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    public function store() {
        if (Session::has('admin_name')) {
            $input = Input::all();

            $rules = array(
                'image' => 'image|max:8000 |required',
                'title' => 'required',
                'summery' => 'required',
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();

                // redirect our user back to the form with the errors from the validator
                return Redirect::to('administrator/header_slider/create')->withErrors($validator);
            }

            $file = Input::file('image');
            if ($file) {
                $destinationPath = 'uploads/header_slider';
// If the uploads fail due to file system, you can try doing public_path().'/uploads' 
//$filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $filename = str_random(12) . "." . $extension;
                $upload_success = Input::file('image')->move($destinationPath, $filename);

                if ($upload_success) {
                    $slider_images = new HeaderSlider();
                    $slider_images->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $slider_images->seo_meta_description = Input::get('seo_meta_description');
                    $slider_images->title = Input::get('title');
                    $slider_images->summery = Input::get('summery');
                    $slider_images->content = Input::get('content');
                    $slider_images->image = $filename;
                     $slider_images->last_update_admin_id = Session::get('admin_id');
                    $slider_images->active = 1;
                    $slider_images->deleted = 0;
                     $slider_images->lang=Session::get('lang');
                    $slider_images->save();
                    return Redirect::to('administrator/header_slider');
                } else {
                    return Redirect::to('administrator/header_slider/create')->withErrors("can't save");
                }
            } else {

                $slider_images = new HeaderSlider();
                $slider_images->seo_meta_keywords = Input::get('seo_meta_keywords');
                $slider_images->seo_meta_description = Input::get('seo_meta_description');
                $slider_images->title = Input::get('title');
                $slider_images->summery = Input::get('summery');
                $slider_images->content = Input::get('content');
                 $slider_images->last_update_admin_id = Session::get('admin_id');

                $slider_images->active = 1;
                  $slider_images->deleted = 0;
                   $slider_images->lang=Session::get('lang');
                $slider_images->save();
                return Redirect::to('administrator/header_slider');
            }
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

//  		$news=User::create(array('title'=>Input::get('title'),'summery'=>Input::get('summery'),'content'=>Input::get('content'),'picture'=>Input::get('picture')));
    // another  way to intiate the user object $news= new USer();   $news->name="";.......etc


    public function show($id) {
        //
    }

    public function edit($id) {
        if (Session::has('admin_name')) {
            $slider_images = HeaderSlider::find($id);
            return View::make("admin.header_slider.create")->with('slider_images', $slider_images);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    public function update($id) {
         if (Session::has('admin_name')) {
        $input = Input::all();
        $rules = array(
            'image' => 'image|max:8000',
            'title' => 'required',
            'summery' => 'required',
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('administrator/header_slider/' . $id . '/edit')->withErrors($validator);
        }

        $file = Input::file('image');

        if ($file) {
            $destinationPath = 'uploads/header_slider';
            $extension = $file->getClientOriginalExtension();
            $filename = str_random(12) . "." . $extension;
            $upload_success = Input::file('image')->move($destinationPath, $filename);
            if ($upload_success) {
                $slider_images = HeaderSlider::find($id);
                $slider_images->seo_meta_keywords = Input::get('seo_meta_keywords');
                $slider_images->seo_meta_description = Input::get('seo_meta_description');
                $slider_images->title = Input::get('title');
                $slider_images->summery = Input::get('summery');
                $slider_images->content = Input::get('content');
                $old_image=$slider_images->image;
                if($old_image != $filename){
                     $slider_images->image = $filename;
                     if($old_image !="")
                     { 
                         unlink('uploads/header_slider/'.$old_image);
                     }
                }else{
                $slider_images->image = $filename;
                }
                 $slider_images->last_update_admin_id = Session::get('admin_id');
                $slider_images->active = 1;
                  $slider_images->deleted = 0;
                $slider_images->save();
                return Redirect::to('administrator/header_slider');
            }
        }
        $slider_images = HeaderSlider::find($id);
        $slider_images->seo_meta_keywords = Input::get('seo_meta_keywords');
        $slider_images->seo_meta_description = Input::get('seo_meta_description');
        $slider_images->title = Input::get('title');
        $slider_images->summery = Input::get('summery');
        $slider_images->content = Input::get('content');
         $slider_images->last_update_admin_id = Session::get('admin_id');
        $slider_images->active = 1;
          $slider_images->deleted = 0;
        $slider_images->save();
        return Redirect::to('administrator/header_slider');
    }else 
        {
        return Redirect::to('administrator/admin/login');
        }
    }

    public function destroy($id) {
        $slider_images = HeaderSlider::find($id);
        $slider_images->deleted = 1;
         $slider_images->save();

        // redirect
       // Session::flash('message', 'Successfully deleted !');
        return Redirect::to('administrator/header_slider');
    }

    public function ActiveAjax($id) {
        $active = HeaderSlider::find($id);
   
        if($active->active==0)
        {
            $active->active=1;
            
        }
        elseif($active->active==1)
        {
            $active->active=0;
             
        }
        $active->save();
        // return Redirect::to('administrator/header_slider');
          
        
    }

}
