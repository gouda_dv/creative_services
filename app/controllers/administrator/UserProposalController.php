<?php

class UserProposalController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     public function index() {
         if(Session::has('admin_name')){
             $lang=Session ::get('lang');
           //  $condition[]="deleted=0 AND lang= '$lang' ";
             //print $condition;exit;
           //  $cond=implode(',',$condition);
           //  print $cond;exit;
        $proposal = UserProposal::whereRaw("deleted=0 AND lang= '$lang' ")->get();
        return View::make('admin.user_proposal.index')->with('proposal', $proposal);
         }
         else
             {
             return Redirect::to('administrator/admin/login');
             }
    }
    
    public function create() {
         if(Session::has('admin_name')){
        return View::make('admin.user_proposal.create');
         }
         else
         {
             return Redirect::to('administrator/admin/login');
         }
    }

    public function store() {
         if(Session::has('admin_name')){
        $input = Input::all();
        $rules = array(
            'name' => 'required',
            'email' => 'required',
            'title' => 'required',
            'content' => 'required',
            
            
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return Redirect::to('administrator/user_proposal/create')->withErrors($validator);
        }
            $proposal = new UserProposal();
            $proposal->seo_meta_keywords = Input::get('seo_meta_keywords');
            $proposal->seo_meta_description = Input::get('seo_meta_description');
            $proposal->name = Input::get('name');
            $proposal->email = Input::get('email');
            $proposal->title=Input::get('title');
            $proposal->content=Input::get('content');
             $proposal->lang=Session::get('lang');
           // $proposal->user_id=1;
            $proposal->deleted = 0;
            $proposal->save();
            return Redirect::to('administrator/user_proposal');
       
       
        }
         else
             {
             return Redirect::to('administrator/admin/login');
             }

//  		$news=User::create(array('title'=>Input::get('title'),'summery'=>Input::get('summery'),'content'=>Input::get('content'),'picture'=>Input::get('picture')));
        // another  way to intiate the user object $news= new USer();   $news->name="";.......etc
    }
     public function show($id) {
        //
    }

    public function edit($id) {
 if(Session::has('admin_name')){
        $proposal = UserProposal::find($id);
        return View::make("admin.user_proposal.create")->with('proposal', $proposal);
 }else{
      return Redirect::to('administrator/admin/login');
 }
    }

    public function update($id) {
        if(Session::has('admin_name')){
        $input = Input::all();
       $rules = array(
           'name' => 'required',
            'email' => 'required',
            'title' => 'required',
            'content' => 'required',
            
            
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('administrator/user_proposal/' . $id . '/edit')->withErrors($validator);
        }
            $proposal = UserProposal::find($id);
            $proposal->seo_meta_keywords = Input::get('seo_meta_keywords');
            $proposal->seo_meta_description = Input::get('seo_meta_description');
             $proposal->name = Input::get('name');
            $proposal->email = Input::get('email');
            $proposal->title=Input::get('title');
            $proposal->content=Input::get('content');
            //$proposal->user_id=1;
            $proposal->deleted = 0;
            $proposal->save();
                return Redirect::to('administrator/user_proposal');
            }
       
        else
            {
            return Redirect::to('administrator/admin/login');
            }
    }
 
     public function destroy($id) {
        $proposal = UserProposal::find($id);
        $proposal->deleted = 1;
         
         $proposal->save();
        // print "<script type=\"text/javascript\">alert('تم الحذف بنجاح');</script>";
        // redirect
       // Session::flash('message', 'Successfully deleted !');
      return Redirect::to('administrator/user_proposal');
    
    }

    
    
    
}