<?php

class SettingsController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
            $settings = Settings::find(1);
            return View::make('admin.settings.create')->with('settings', $settings);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            return Redirect::to('administrator/settings');
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (Session::has('admin_name')) {
            $settings = Settings::find(1);
            return View::make('admin.settings.create')->with('settings', $settings);
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();

            $settings = Settings::find(1);
            $settings->facebook_link = Input::get('facebook_link');
            $settings->twitter_link = Input::get('twitter_link');
            $settings->youtube_link = Input::get('youtube_link');
            $settings->google_plus_link = Input::get('google_plus_link');
            $settings->instagram_link = Input::get('instagram_link');
           // $settings->address = Input::get('address');
            $settings->linkedin_link = Input::get('linkedin_link');
            $settings->save();
            return View::make('admin.settings.create')->with('settings', $settings)->with('success','success');

        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
    }

}