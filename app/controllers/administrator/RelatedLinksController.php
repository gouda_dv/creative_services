<?php

class RelatedLinksController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     public function index() {
         if(Session::has('admin_name')){
             $lang=Session ::get('lang');
           //  $condition[]="deleted=0 AND lang= '$lang' ";
             //print $condition;exit;
           //  $cond=implode(',',$condition);
           //  print $cond;exit;
        $links = RelatedLinks::whereRaw("deleted=0 AND lang= '$lang' ")->get();
        return View::make('admin.related_links.index')->with('links', $links);
         }
         else
             {
             return Redirect::to('administrator/admin/login');
             }
    }
    
    public function create() {
         if(Session::has('admin_name')){
        return View::make('admin.related_links.create');
         }
         else
         {
             return Redirect::to('administrator/admin/login');
         }
    }

    public function store() {
         if(Session::has('admin_name')){
        $input = Input::all();
        $rules = array(
            'image' => 'image|max:8000',
            'title' => 'required',
            'link' => 'required',
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return Redirect::to('administrator/related_links/create')->withErrors($validator);
        }

        $file = Input::file('image');
        if($file){
        $destinationPath = 'uploads/related_links';
// If the uploads fail due to file system, you can try doing public_path().'/uploads' 
//$filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $filename = str_random(12) . "." . $extension;
        $upload_success = Input::file('image')->move($destinationPath, $filename);

        if ($upload_success) {
            $links = new RelatedLinks();
            $links->seo_meta_keywords = Input::get('seo_meta_keywords');
            $links->seo_meta_description = Input::get('seo_meta_description');
            $links->title = Input::get('title');
            $links->link = Input::get('link');
            $links->lang=Session::get('lang');
            $links->image = $filename;
            $links->deleted = 0;
          
            $links->save();
            return Redirect::to('administrator/related_links');
        } else {
            return Redirect::to('administrator/related_links/create')->withErrors("can't save");
        }
        } else {
            
             $links = new RelatedLinks();
            $links->seo_meta_keywords = Input::get('seo_meta_keywords');
            $links->seo_meta_description = Input::get('seo_meta_description');
            $links->title = Input::get('title');
            $links->link = Input::get('link');
             $links->lang=Session::get('lang');
             $links->deleted = 0;
            $links->save();
            return Redirect::to('administrator/related_links');
            
        }
        }
         else
             {
             return Redirect::to('administrator/admin/login');
             }

//  		$news=User::create(array('title'=>Input::get('title'),'summery'=>Input::get('summery'),'content'=>Input::get('content'),'picture'=>Input::get('picture')));
        // another  way to intiate the user object $news= new USer();   $news->name="";.......etc
    }
     public function show($id) {
        //
    }

    public function edit($id) {
 if(Session::has('admin_name')){
        $links = RelatedLinks::find($id);
        return View::make("admin.related_links.create")->with('links', $links);
 }else{
      return Redirect::to('administrator/admin/login');
 }
    }

    public function update($id) {
        if(Session::has('admin_name')){
        $input = Input::all();
        $rules = array(
            'image' => 'image|max:8000',
            'title' => 'required',
            'link' => 'required',
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('administrator/related_links/' . $id . '/edit')->withErrors($validator);
        }

        $file = Input::file('image');
       
        if ($file ) {
            $destinationPath = 'uploads/related_links';
            $extension = $file->getClientOriginalExtension();
            $filename = str_random(12) . "." . $extension;
            $upload_success = Input::file('image')->move($destinationPath, $filename);
            if ($upload_success) {
                 $links = RelatedLinks::find($id);
                $links->seo_meta_keywords = Input::get('seo_meta_keywords');
                $links->seo_meta_description = Input::get('seo_meta_description');
                $links->title = Input::get('title');
                $old_img=$links->image;
                $links->link = Input::get('link');
                if($old_img != $filename )
                    {
                    $links->image = $filename;
                    if($old_img !="")
                    {
                        unlink('uploads/related_links/'.$old_img);
                    }
                    }
                    else{
                $links->image = $filename;
                    }
               // print_r($_FILES['image']['name']);exit;
              //  unlink($_FILES['image']['name']);
             $links->deleted = 0;
                $links->save();
                return Redirect::to('administrator/related_links');
            }
        }
        $links = RelatedLinks::find($id);
        $links->seo_meta_keywords = Input::get('seo_meta_keywords');
        $links->seo_meta_description = Input::get('seo_meta_description');
        $links->title = Input::get('title');
        $links->link = Input::get('link');
        $links->deleted = 0;
        $links->save();
        return Redirect::to('administrator/related_links');
        }
        else
            {
            return Redirect::to('administrator/admin/login');
            }
    }
 
     public function destroy($id) {
        $links = RelatedLinks::find($id);
        $links->deleted = 1;
         
         $links->save();
        // print "<script type=\"text/javascript\">alert('تم الحذف بنجاح');</script>";
        // redirect
       // Session::flash('message', 'Successfully deleted !');
      return Redirect::to('administrator/related_links');
    
    }

    
    
    
}