<?php

class ProjectController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
         if (Session::has('admin_name')) {
               $lang=Session ::get('lang');
        $project = Project::whereRaw("deleted=0 AND lang= '$lang' ")->get();
        return View::make('admin.project.index')->with('project', $project);
         }
         else
         {
             
            return Redirect::to('administrator/admin/login');
         }
    }

    public function create() {
         if (Session::has('admin_name')) {
        return View::make('admin.project.create');
         }
         else
         {
               return Redirect::to('administrator/admin/login');
         }
    }

    public function store() {
           if (Session::has('admin_name')) {
       $input = Input::all();

            $rules = array(

                'image' => 'required| image|max:8000',

                'title' => 'required',
              

     

            );



            $validator = Validator::make($input, $rules);



            if ($validator->fails()) {

                $messages = $validator->messages();

                return Redirect::to('administrator/project/create')->withErrors($validator);

           }


        $file = Input::file('image');
        if($file){
        $destinationPath = 'uploads/project';
// If the uploads fail due to file system, you can try doing public_path().'/uploads' 
//$filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $filename = str_random(12) . "." . $extension;
        $upload_success = Input::file('image')->move($destinationPath, $filename);

        if ($upload_success) {
            $project = new Project();
            $project->seo_meta_keywords = Input::get('seo_meta_keywords');
            $project->seo_meta_description = Input::get('seo_meta_description');
            $project->title = Input::get('title');
            $project->summery = Input::get('summery');
             $project->content = Input::get('content');
            
            $project->date= date("y-m-d", strtotime(Input::get('date')));
            $project->image = $filename;
             $project->deleted = 0;
             $project->lang=Session::get('lang');
      // print($project->date);exit;
            $project->save();
            return Redirect::to('administrator/project');
        } else {
            return Redirect::to('administrator/project/create')->withErrors("can't save");
        }
           }else
               {
                 $project = new Project();
            $project->seo_meta_keywords = Input::get('seo_meta_keywords');
            $project->seo_meta_description = Input::get('seo_meta_description');
            $project->title = Input::get('title');
            $project->summery = Input::get('summery');
             $project->content = Input::get('content');
              $project->date= date("y-m-d", strtotime(Input::get('date')));
               $project->deleted = 0;
               $project->lang=Session::get('lang');
             // print($project->date);exit;
            $project->save();
            return Redirect::to('administrator/project');
               
               
               }
           }
           else
           {
               return Redirect::to('administrator/admin/login');
           }

//  		$news=User::create(array('title'=>Input::get('title'),'summery'=>Input::get('summery'),'content'=>Input::get('content'),'picture'=>Input::get('picture')));
        // another  way to intiate the user object $news= new USer();   $news->name="";.......etc
    }
     public function show($id) {
        //
    }
    public function edit($id) {
  if (Session::has('admin_name')) {
       
        $project = Project::find($id);
        return View::make("admin.project.create")->with('project', $project);
  }
  else
  {
      return Redirect::to('administrator/admin/login');
  }
    }

    public function update($id) {
         if (Session::has('admin_name')) {
       
              $input = Input::all();

            $rules = array(

                'image' => 'image|max:8000',

                'title' => 'required',
               

     

            );



            $validator = Validator::make($input, $rules);



            if ($validator->fails()) {

                $messages = $validator->messages();

                return Redirect::to('administrator/project/create')->withErrors($validator);

           }

             
        $file = Input::file('image');
       
        if ($file ) {
            $destinationPath = 'uploads/project';
            $extension = $file->getClientOriginalExtension();
            $filename = str_random(12) . "." . $extension;
            $upload_success = Input::file('image')->move($destinationPath, $filename);
            if ($upload_success) {
                 $project = Project::find($id);
                $project->seo_meta_keywords = Input::get('seo_meta_keywords');
                $project->seo_meta_description = Input::get('seo_meta_description');
                $project->title = Input::get('title');
                $project->summery = Input::get('summery');
                $project->content = Input::get('content');
                $date=date("y-m-d", strtotime(Input::get('date')));
              
                 $project->date=$date;
                  //print $project->date;exit;
                $old_image=$project->image;
                if($old_image != $filename ){
                     $project->image = $filename;
                     if($old_image !="")
                     {
                          if(file_exists('uploads/project/'.$old_image)){
                         unlink('uploads/project/'.$old_image);
                          }
                     }
                }else{
                $project->image = $filename;
                }
                  $project->deleted = 0;
                
                $project->save();
                return Redirect::to('administrator/project');
            }
        }
       $project = Project::find($id);
        $project->seo_meta_keywords = Input::get('seo_meta_keywords');
        $project->seo_meta_description = Input::get('seo_meta_description');
        $project->title = Input::get('title');
        $project->summery = Input::get('summery');
          $project->content = Input::get('content');
          $date=date("y-m-d", strtotime(Input::get('date')));
               
           $project->date=$date;
          // print $project->date;exit;
            $project->deleted = 0;
        $project->save();
        return Redirect::to('administrator/project');
    }
    else 
    {
        return Redirect::to('administrator/admin/login'); 
    }
    }
    
 
     public function destroy($id) {
        $project = Project::find($id);
        $project->deleted = 1;
        $project->save();

        // redirect
        //Session::flash('message', 'Successfully deleted !');
        return Redirect::to('administrator/project');
    }

}
