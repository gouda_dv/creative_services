<?php

class VideosController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
          if (Session::has('admin_name')) {
                $lang=Session ::get('lang');
        $videos = Videos::whereRaw("deleted=0 AND lang= '$lang' ")->get();
        return View::make('admin.videos.index')->with('videos', $videos);
          }
 else {
     return Redirect::to('administrator/admin/login');
 }
    }

    public function create() {
         if (Session::has('admin_name')) {
         	
         	$categories = VideoCategory::whereRaw('deleted=0')->get();
            $selectedCategories = array();
            foreach ($categories as $category) {
                $selectedCategories[0] =Lang::get('global.choose');
                $selectedCategories[$category->id] = $category->title;
            }
            $coins = Currency::whereRaw('deleted=0')->get();
            $selectedCoins = array();
            foreach ($coins as $coin) {
                $selectedCoins[0]=Lang::get('global.choose');
                $selectedCoins[$coin->id] = $coin->title;
            }
            return View::make('admin.videos.create')->with("cats", $selectedCategories)
                    ->with("coin", $selectedCoins);
            
         }
         else 
         {
               return Redirect::to('administrator/admin/login');
         }
    }

    public function store() {
          if (Session::has('admin_name')) {
       $input = Input::all();
//print_r($input);exit;
        
        $rules = array(
          
             'title' => 'required',
        
            );
          
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return Redirect::to('administrator/videos/create')->withErrors($validator);
        }

        $file = Input::file('video');
        if($file){
        $destinationPath = 'uploads/videos';
     
// If the uploads fail due to file system, you can try doing public_path().'/uploads' 
//$filename = $file->getClientOriginalName();
       $extension = $file->getClientOriginalExtension();
       // $extension = $file->getMimeType();
      //  print $extension;die;
        $filename = str_random(12) . "." . $extension;
       // print_r($filename);exit;
       // print_r( $destinationPath) ; echo "okkkk"; print_r( $filename);exit;
        $upload_success = Input::file('video')->move($destinationPath, $filename);
       // print_r($upload_success);exit;
        if ($upload_success) {
            $videos = new Videos();
            $videos->seo_meta_keywords = Input::get('seo_meta_keywords');
            $videos->seo_meta_description = Input::get('seo_meta_description');
            $videos->title = Input::get('title');
            $videos->summery = Input::get('summery');
            $videos->video_category_id = Input::get('video_category_id');
            $videos->youtube_link = Input::get('youtube_link');
            $videos->video = $filename;
            $videos->video_price=Input::get('price');
                     $videos->video_currency_id=Input::get('coin');
             $paid = Input::get('video_paid');
                if ($paid == 1) {
                    $videos->video_paid = 1;
                    
                } else {
                    $videos->video_paid = 0;
                    
                }
            
              $videos->deleted = 0;
               $videos->lang=Session::get('lang');
            $videos->save();
           
            return Redirect::to('administrator/videos');
        } else {
            return Redirect::to('administrator/videos/create')->withErrors("can't save");
        }
          }else 
        {
              
            $videos = new Videos();
            $videos->seo_meta_keywords = Input::get('seo_meta_keywords');
            $videos->seo_meta_description = Input::get('seo_meta_description');
            $videos->title = Input::get('title');
            $videos->summery = Input::get('summery');
            $videos->youtube_link = Input::get('youtube_link');
            $videos->video_category_id = Input::get('video_category_id');
            $videos->video_price=Input::get('price');
                     $videos->video_currency_id=Input::get('coin');
            $paid = Input::get('video_paid');
                if ($paid == 1) {
                    $videos->video_paid = 1;
                } else {
                    $videos->video_paid = 0;
                }
             $videos->deleted = 0;
              $videos->lang=Session::get('lang');
            $videos->save();
           
            return Redirect::to('administrator/videos');    
              
        }
          }
          
          else
          {
               return Redirect::to('administrator/admin/login');
          }
//  		$news=User::create(array('title'=>Input::get('title'),'summery'=>Input::get('summery'),'content'=>Input::get('content'),'picture'=>Input::get('picture')));
        // another  way to intiate the user object $news= new USer();   $news->name="";.......etc
 
        
    }
     public function show($id) {
        //
    }
    public function edit($id) {
  if (Session::has('admin_name')) {
  		$categories = VideoCategory::whereRaw('deleted=0')->get();
            $selectedCategories = array();
            foreach ($categories as $category) {
                $selectedCategories[$category->id] = $category->title;
            }
             $coins = Currency::whereRaw('deleted=0')->get();
            $selectedCoins = array();
            foreach ($coins as $coin) {
                $selectedCoins[0]=Lang::get('global.choose');
                $selectedCoins[$coin->id] = $coin->title;
            }
            
        $videos = Videos::find($id);
        return View::make("admin.videos.create")->with('videos', $videos)->with("cats", $selectedCategories)
          ->with("coin", $selectedCoins);
  }
 else {
        return Redirect::to('administrator/admin/login');
  }
    }

    public function update($id) {
        
         if (Session::has('admin_name')) {
        $input = Input::all();
        $rules = array(
            
            'title' => 'required',
           
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('administrator/videos/' . $id . '/edit')->withErrors($validator);
        }

        $file = Input::file('video');
       
        if ($file ) {
            $destinationPath = 'uploads/videos';
            $extension = $file->getClientOriginalExtension();
            $filename = str_random(12) . "." . $extension;
            $upload_success = Input::file('video')->move($destinationPath, $filename);
            if ($upload_success) {
                 $videos = Videos::find($id);
                $videos->seo_meta_keywords = Input::get('seo_meta_keywords');
                $videos->seo_meta_description = Input::get('seo_meta_description');
                $videos->title = Input::get('title');
                $videos->summery = Input::get('summery');
                $videos->video_category_id = Input::get('video_category_id');
                $videos->youtube_link = Input::get('youtube_link');
                $videos->video_price=Input::get('price');
                     $videos->video_currency_id=Input::get('coin');
                $videos->video_paid = Input::get('video_paid');
                 if (isset($videos->video_paid)) {
                        $videos->video_paid = 1;
                    } else {
                        $videos->video_paid = 0;
                    }

                
                $old_video=$videos->video;
                if($old_video != $filename ){
                     $videos->video = $filename;
                     if($old_video !="")
                     {
                         unlink('uploads/videos/'.$old_video);
                     }
                }else{
                $videos->video = $filename;
                }
                 $videos->deleted = 0;
                
                $videos->save();
                return Redirect::to('administrator/videos');
            }
        }else{
        $videos = Videos::find($id);
        $videos->seo_meta_keywords = Input::get('seo_meta_keywords');
        $videos->seo_meta_description = Input::get('seo_meta_description');
        $videos->title = Input::get('title');
        $videos->summery = Input::get('summery');
        $videos->video_category_id = Input::get('video_category_id');
          $videos->youtube_link = Input::get('youtube_link');
          $videos->video_price=Input::get('price');
                     $videos->video_currency_id=Input::get('coin');
           $videos->video_paid = Input::get('video_paid');
         if (isset($videos->video_paid)) {
                        $videos->video_paid = 1;
                    } else {
                        $videos->video_paid = 0;
                    }


           $videos->deleted = 0;
        $videos->save();
        return Redirect::to('administrator/videos');
        }
    }
    else 
    {
         return Redirect::to('administrator/admin/login');
    }
    }
 
     public function destroy($id) {
        $videos = Videos::find($id);
          $videos->deleted = 1;
        $videos->save();

        // redirect
      //  Session::flash('message', 'Successfully deleted !');
        return Redirect::to('administrator/videos');
    }

}
