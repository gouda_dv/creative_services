<?php

class ContentController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
            $lang = Session ::get('lang');
            $all_content = Content::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            foreach ($all_content as $content) {
                $category = ContentCategory::find($content['content_category_id']);
                $content['content_category_id'] = $category->title;
            }

            return View::make('admin.content.index')->with('all_content', $all_content);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            $lang=Session ::get('lang');
            $categories = ContentCategory::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedCategories = array();
            $selectedCategories['']=Lang::get('global.choose');
            foreach ($categories as $category) {
                $selectedCategories[$category->id] = $category->title;
            }
            return View::make('admin.content.create')->with("cats", $selectedCategories);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000',
                'title' => 'required',
                'content_category_id' => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/content/create')->withErrors($validator);
            }
            $image = Input::file('image');
            if ($image) {
                $destinationPath = 'uploads/content/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    $content = new Content();
                    $content->title = Input::get('title');
                    $content->content_category_id = Input::get('content_category_id');
                    $content->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $content->seo_meta_description = Input::get('seo_meta_description');
                    $content->summery = Input::get('summery');
                    $content->date = date("y-m-d", strtotime(Input::get('date')));
                    $content->content = Input::get('content');
                    $content->image = $filename_image;
                    $content->last_update_date = date("Y-m-d");
                    $content->last_update_admin_id = Session::get('admin_id');
                    $content->deleted = 0;
                     $content->lang=Session::get('lang');
                    $content->save();
                    return Redirect::to('administrator/content');
                } else {
                    return Redirect::to('administrator/content/create')->withErrors("can't save");
                }
            } else {
                $content = new Content();
                $content->title = Input::get('title');
                $content->content_category_id = Input::get('content_category_id');
                $content->seo_meta_keywords = Input::get('seo_meta_keywords');
                $content->seo_meta_description = Input::get('seo_meta_description');
                $content->summery = Input::get('summery');
                $content->date = date("y-m-d", strtotime(Input::get('date')));
                $content->content = Input::get('content');
                $content->last_update_date = date("Y-m-d");
                $content->last_update_admin_id = Session::get('admin_id');
                $content->deleted = 0;
                 $content->lang=Session::get('lang');
                $content->save();
                return Redirect::to('administrator/content');
            }
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (Session::has('admin_name')) {
            $categories = ContentCategory::whereRaw('deleted=0')->get();
            $selectedCategories = array();
            $selectedCategories['']=Lang::get('global.choose');
            foreach ($categories as $category) {
                $selectedCategories[$category->id] = $category->title;
            }
            $content = Content::find($id);
            return View::make("admin.content.create")->with('content', $content)->with("cats", $selectedCategories);
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000',
                'title' => 'required',
                'content_category_id' => 'required'
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/content/' . Input::get('id') . '/edit')->withErrors($validator);
            }
            $image = Input::file('image');
            if ($image) {
                $destinationPath = 'uploads/content/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    $content = Content::find($id);
                    File::delete('uploads/content/'.$content->image);
                    $content->title = Input::get('title');
                    $content->content_category_id = Input::get('content_category_id');
                    $content->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $content->seo_meta_description = Input::get('seo_meta_description');
                    $content->summery = Input::get('summery');
                    $content->date = date("y-m-d", strtotime(Input::get('date')));
                    $content->content = Input::get('content');
                    $content->image = $filename_image;
                    $content->last_update_date = date("Y-m-d");
                    $content->last_update_admin_id = Session::get('admin_id');
                    $content->deleted = 0;
                    $content->save();
                    return Redirect::to('administrator/content');
                } else {
                    return Redirect::to('administrator/content/' . Input::get('id') . '/edit')->withErrors("can't save");
                }
            } else {
                $content = Content::find($id);
                $content->title = Input::get('title');
                $content->content_category_id = Input::get('content_category_id');
                $content->seo_meta_keywords = Input::get('seo_meta_keywords');
                $content->seo_meta_description = Input::get('seo_meta_description');
                $content->summery = Input::get('summery');
                $content->date = date("y-m-d", strtotime(Input::get('date')));
                $content->content = Input::get('content');
                $content->last_update_date = date("Y-m-d");
                $content->last_update_admin_id = Session::get('admin_id');
                $content->deleted = 0;
                $content->save();
                return Redirect::to('administrator/content');
            }
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $content = Content::find($id);
        $content->deleted = 1;
        $content->save();
        return Redirect::to('administrator/content');
    }

}
