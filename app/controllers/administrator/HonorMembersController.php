<?php

class HonorMembersController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
              $lang=Session ::get('lang');
            $members = HonorMembers::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            return View::make('admin.honor_members.index')->with('members', $members);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    public function create() {
        if (Session::has('admin_name')) {
            return View::make('admin.honor_members.create');
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    public function store() {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000 |required',
                'title' => 'required',
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();

                // redirect our user back to the form with the errors from the validator
                return Redirect::to('administrator/honor_members/create')->withErrors($validator);
            }

            $file = Input::file('image');
            if ($file) {
                $destinationPath = 'uploads/honor_members';
// If the uploads fail due to file system, you can try doing public_path().'/uploads' 
//$filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $filename = str_random(12) . "." . $extension;
                $upload_success = Input::file('image')->move($destinationPath, $filename);

                if ($upload_success) {
                    $members = new HonorMembers();
                    $members->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $members->seo_meta_description = Input::get('seo_meta_description');
                    $members->title = Input::get('title');
                    $members->summery = Input::get('summery');
                    $members->content = Input::get('content');



                    $members->image = $filename;


                    $members->deleted = 0;
                     $members->lang=Session::get('lang');

                    $members->save();
                    return Redirect::to('administrator/honor_members');
                } else {
                    return Redirect::to('administrator/honor_members/create')->withErrors("can't save");
                }
            } else {
                $members = new HonorMembers();
                $members->seo_meta_keywords = Input::get('seo_meta_keywords');
                $members->seo_meta_description = Input::get('seo_meta_description');
                $members->title = Input::get('title');
                $members->summery = Input::get('summery');
                $members->content = Input::get('content');
                $members->deleted = 0;
                 $members->lang=Session::get('lang');
                $members->save();
                return Redirect::to('administrator/honor_members');
            }
        } else {
            return Redirect::to('administrator/admin/login');
        }

//  		$news=User::create(array('title'=>Input::get('title'),'summery'=>Input::get('summery'),'content'=>Input::get('content'),'picture'=>Input::get('picture')));
        // another  way to intiate the user object $news= new USer();   $news->name="";.......etc
    }

    public function show($id) {
        //
    }

    public function edit($id) {
        if (Session::has('admin_name')) {
            $members = HonorMembers::find($id);
            return View::make("admin.honor_members.create")->with('members', $members);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000',
                'title' => 'required',
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/honor_members/' . $id . '/edit')->withErrors($validator);
            }

            $file = Input::file('image');

            if ($file) {
                $destinationPath = 'uploads/honor_members';
                $extension = $file->getClientOriginalExtension();
                $filename = str_random(12) . "." . $extension;
                $upload_success = Input::file('image')->move($destinationPath, $filename);
                if ($upload_success) {
                    $members = HonorMembers::find($id);
                    $members->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $members->seo_meta_description = Input::get('seo_meta_description');
                    $members->title = Input::get('title');
                    $members->summery = Input::get('summery');
                    $members->content = Input::get('content');
$old_image=$members->image;
if($old_image != $filename ){
     $members->image = $filename;
     if($old_image !="")
     {
         unlink('uploads/honor_members/'.$old_image);
     }     
    
}
else{
      $members->image = $filename;
}
                  
                    $members->deleted = 0;

                    $members->save();
                    return Redirect::to('administrator/honor_members');
                }
            }
            $members = HonorMembers::find($id);
            $members->seo_meta_keywords = Input::get('seo_meta_keywords');
            $members->seo_meta_description = Input::get('seo_meta_description');
            $members->title = Input::get('title');
            $members->summery = Input::get('summery');
            $members->content = Input::get('content');
            $members->deleted = 0;
            $members->save();
            return Redirect::to('administrator/honor_members');
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    public function destroy($id) {
        $members = HonorMembers::find($id);
        $members->deleted = 1;
        $members->save();

        // redirect
        //  Session::flash('message', 'Successfully deleted !');
        return Redirect::to('administrator/honor_members');
    }

}
