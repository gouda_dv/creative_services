<?php

class ServiceController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
         if (Session::has('admin_name')) {
                $lang = Session ::get('lang');
                 $service = Service::whereRaw("deleted=0 AND lang= '$lang' ")->get();
           // $all_content = ServiceCategory::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            foreach ($service as $content) {
                $category = ServiceCategory::find($content['service_category_id']);
                $content['service_category_id'] = $category->title;
            }
       
        return View::make('admin.service.index')->with('service', $service);
         }
         else
         {
             
            return Redirect::to('administrator/admin/login');
         }
    }

    public function create() {
         if (Session::has('admin_name')) {
              $lang=Session ::get('lang');
              $categories = ServiceCategory::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedCategories = array();
            $selectedCategories['']=Lang::get('global.choose');
            foreach ($categories as $category) {
                $selectedCategories[$category->id] = $category->title;
            }
        return View::make('admin.service.create')->with("cats", $selectedCategories);
         }
         else
         {
               return Redirect::to('administrator/admin/login');
         }
    }

    public function store() {
           if (Session::has('admin_name')) {
        $input = Input::all();
        $rules = array(
            'image' => 'image|max:8000 | required',
            'title' => 'required',
           
           
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return Redirect::to('administrator/service/create')->withErrors($validator);
        }

        $file = Input::file('image');
        if($file){
        $destinationPath = 'uploads/service';
// If the uploads fail due to file system, you can try doing public_path().'/uploads' 
//$filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $filename = str_random(12) . "." . $extension;
        $upload_success = Input::file('image')->move($destinationPath, $filename);

        if ($upload_success) {
            $service = new Service();
            $service->seo_meta_keywords = Input::get('seo_meta_keywords');
            $service->seo_meta_description = Input::get('seo_meta_description');
            $service->title = Input::get('title');
            $service->summery = Input::get('summery');
             $service->content = Input::get('content');
              $service->service_category_id = Input::get('service_category_id');
            
            $service->image = $filename;
             $service->deleted = 0;
             $service->lang=Session::get('lang');
          
            $service->save();
            return Redirect::to('administrator/service');
        } else {
            return Redirect::to('administrator/service/create')->withErrors("can't save");
        }
           }else
               {
                 $service = new Service();
            $service->seo_meta_keywords = Input::get('seo_meta_keywords');
            $service->seo_meta_description = Input::get('seo_meta_description');
            $service->title = Input::get('title');
            $service->summery = Input::get('summery');
             $service->content = Input::get('content');
              $service->service_category_id = Input::get('service_category_id');
               $service->deleted = 0;
               $service->lang=Session::get('lang');
            $service->save();
            return Redirect::to('administrator/service');
               
               
               }
           }
           else
           {
               return Redirect::to('administrator/admin/login');
           }

//  		$news=User::create(array('title'=>Input::get('title'),'summery'=>Input::get('summery'),'content'=>Input::get('content'),'picture'=>Input::get('picture')));
        // another  way to intiate the user object $news= new USer();   $news->name="";.......etc
    }
     public function show($id) {
        //
    }
    public function edit($id) {
  if (Session::has('admin_name')) {
        $lang=Session ::get('lang');
            $categories = ServiceCategory::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedCategories = array();
            $selectedCategories['']=Lang::get('global.choose');
            foreach ($categories as $category) {
                $selectedCategories[$category->id] = $category->title;
            }
            $content = ServiceCategory::find($id);
        $service = Service::find($id);
        return View::make("admin.service.create")->with('service', $service)->with("cats", $selectedCategories);
  }
  else
  {
      return Redirect::to('administrator/admin/login');
  }
    }

    public function update($id) {
         if (Session::has('admin_name')) {
        $input = Input::all();
        $rules = array(
            'image' => 'image|max:8000',
            'title' => 'required',
           
           
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('administrator/service/' . $id . '/edit')->withErrors($validator);
        }

        $file = Input::file('image');
       
        if ($file ) {
            $destinationPath = 'uploads/service';
            $extension = $file->getClientOriginalExtension();
            $filename = str_random(12) . "." . $extension;
            $upload_success = Input::file('image')->move($destinationPath, $filename);
            if ($upload_success) {
                 $service = Service::find($id);
                $service->seo_meta_keywords = Input::get('seo_meta_keywords');
                $service->seo_meta_description = Input::get('seo_meta_description');
                $service->title = Input::get('title');
                $service->summery = Input::get('summery');
                $service->content = Input::get('content');
                 $service->service_category_id = Input::get('service_category_id');
                $old_image=$service->image;
                if($old_image != $filename ){
                     $service->image = $filename;
                     if($old_image !="")
                     {
                        if(file_exists('uploads/service/'.$old_image)) {
                             unlink('uploads/service/'.$old_image);
                        }
                        
                     }
                }else{
                $service->image = $filename;
                }
                  $service->deleted = 0;
                
                $service->save();
                return Redirect::to('administrator/service');
            }
        }
       $service = Service::find($id);
        $service->seo_meta_keywords = Input::get('seo_meta_keywords');
        $service->seo_meta_description = Input::get('seo_meta_description');
        $service->title = Input::get('title');
        $service->summery = Input::get('summery');
          $service->content = Input::get('content');
           $service->service_category_id = Input::get('service_category_id');
            $service->deleted = 0;
        $service->save();
        return Redirect::to('administrator/service');
    }
    else 
    {
        return Redirect::to('administrator/admin/login'); 
    }
    }
    
 
     public function destroy($id) {
        $service = Service::find($id);
        $service->deleted = 1;
        $service->save();

        // redirect
        //Session::flash('message', 'Successfully deleted !');
        return Redirect::to('administrator/service');
    }

}
