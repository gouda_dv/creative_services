<?php

class CenterMemberController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
            $lang = Session ::get('lang');
            $all_members = CenterMember::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            return View::make('admin.center_member.index')->with('all_members', $all_members);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            return View::make('admin.center_member.create');
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000|required',
                'title' => 'required',
                'cv' => 'mimes:pdf,doc,docx',
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();

// redirect our user back to the form with the errors from the validator
                return Redirect::to('administrator/center_members/create')->withErrors($validator);
            }
            $image = Input::file('image');
            $cv = Input::file('cv');
            if ($cv) {
                $destinationPath = 'uploads/center_member/';
// If the uploads fail due to file system, you can try doing public_path().'/uploads' 
                $extension_image = $image->getClientOriginalExtension();
                $extension_cv = $cv->getClientOriginalExtension();

                $filename_image = str_random(12) . "." . $extension_image;
                $filename_cv = str_random(12) . "." . $extension_cv;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                $upload_success_cv = Input::file('cv')->move($destinationPath, $filename_cv);
                if ($upload_success_image && $upload_success_cv) {
                    $member = new CenterMember();
                    $member->title = Input::get('title');
                    $member->emails = Input::get('emails');
                    $member->mobiles = Input::get('mobiles');
                    $member->phones = Input::get('phones');
                    $member->summery = Input::get('summery');
                    $member->content = Input::get('content');
                    $member->image = $filename_image;
                    $member->cv = $filename_cv;
                    $member->last_update_date = date("Y-m-d");
                    $member->last_update_admin_id = Session::get('admin_id');
                    $member->deleted = 0;
                    $member->lang = Session::get('lang');
                    $member->save();
                    return Redirect::to('administrator/center_members');
                } else {
                    return Redirect::to('administrator/center_members/create')->withErrors("can't save");
                }
            } else {
                $destinationPath = 'uploads/center_member/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    $member = new CenterMember();
                    $member->title = Input::get('title');
                    $member->emails = Input::get('emails');
                    $member->mobiles = Input::get('mobiles');
                    $member->phones = Input::get('phones');
                    $member->summery = Input::get('summery');
                    $member->content = Input::get('content');
                    $member->image = $filename_image;
                    $member->last_update_date = date("Y-m-d");
                    $member->last_update_admin_id = Session::get('admin_id');
                    $member->deleted = 0;
                    $member->lang = Session::get('lang');
                    $member->save();
                    return Redirect::to('administrator/center_members');
                } else {
                    return Redirect::to('administrator/center_members/create')->withErrors("can't save");
                }
            }
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (Session::has('admin_name')) {
            $member = CenterMember::find($id);
            return View::make("admin.center_member.create")->with('member', $member);
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'image' => 'image|max:8000',
                'title' => 'required',
                'cv' => 'mimes:pdf,doc,docx',
            );

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/center_members/' . Input::get('id') . '/edit')->withErrors($validator);
            }

            $image = Input::file('image');
            $cv = Input::file('cv');
            if ($image) {
                $destinationPath = 'uploads/center_member/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    $member = CenterMember::find($id);
                    $member->title = Input::get('title');
                    $member->emails = Input::get('emails');
                    $member->mobiles = Input::get('mobiles');
                    $member->phones = Input::get('phones');
                    $member->summery = Input::get('summery');
                    $member->content = Input::get('content');
                    File::delete('uploads/center_member/' . $member->image);
                    $member->image = $filename_image;

                    if ($cv) {
                        $extension_cv = $cv->getClientOriginalExtension();
                        $filename_cv = str_random(12) . "." . $extension_cv;
                        $upload_success_cv = Input::file('cv')->move($destinationPath, $filename_cv);
                        if ($upload_success_cv) {
                            
                            File::delete('uploads/center_member/' . $member->cv);
                            $member->cv = $filename_cv;
                        } else {
                            return Redirect::to('administrator/center_members/' . $member->id . '/edit')->withErrors("can't save");
                        }
                    }

                    $member->last_update_date = date("Y-m-d");
                    $member->last_update_admin_id = Session::get('admin_id');
                    $member->deleted = 0;
                    $member->save();
                    return Redirect::to('administrator/center_members');
                } else {
                    return Redirect::to('administrator/center_members/' . $member->id . '/edit')->withErrors("can't save");
                }
            } elseif (Input::file('cv')) {
                $destinationPath = 'uploads/center_member/';
                $cv = Input::file('cv');
                $extension_cv = $cv->getClientOriginalExtension();
                $filename_cv = str_random(12) . "." . $extension_cv;
                $upload_success_cv = Input::file('cv')->move($destinationPath, $filename_cv);
                if ($upload_success_cv) {
                    $member = CenterMember::find($id);
                    File::delete('uploads/center_member/' . $member->cv);
                    $member->title = Input::get('title');
                    $member->emails = Input::get('emails');
                    $member->mobiles = Input::get('mobiles');
                    $member->phones = Input::get('phones');
                    $member->summery = Input::get('summery');
                    $member->content = Input::get('content');
                    $member->cv = $filename_cv;
                    $member->last_update_date = date("Y-m-d");
                    $member->last_update_admin_id = Session::get('admin_id');
                    $member->deleted = 0;
                    $member->save();
                    return Redirect::to('administrator/center_members');
                } else {
                    return Redirect::to('administrator/center_members/' . $member->id . '/edit')->withErrors("can't save");
                }
            } else {
                $member = CenterMember::find($id);
                $member->title = Input::get('title');
                $member->emails = Input::get('emails');
                $member->mobiles = Input::get('mobiles');
                $member->phones = Input::get('phones');
                $member->summery = Input::get('summery');
                $member->content = Input::get('content');
                $member->last_update_date = date("Y-m-d");
                $member->last_update_admin_id = Session::get('admin_id');
                $member->deleted = 0;
                $member->save();
                return Redirect::to('administrator/center_members');
            }
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $member = CenterMember::find($id);
        $member->deleted = 1;
        $member->save();
        return Redirect::to('administrator/center_members');
    }

}
