<?php

class QuestionnaireController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
            $lang=Session ::get('lang');
            $all_questions = Questionnaire::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            return View::make('admin.question.index')->with('all_questions', $all_questions);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            return View::make('admin.question.create');
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        if (Session::has('admin_name')) {
            $input = Input::all();
            // print_r($input);exit;
            $rules = array(
                'question' => 'required',
                'newchoice' => 'required'
            );
            $messages = array(
                'newchoice.required' => 'choice field is required',
                'question.required' => 'question field is required'
            );
            $validator = Validator::make($input, $rules, $messages);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/question/create')->withErrors($validator);
            }
            $question = new Questionnaire();
            $question->question = Input::get('question');
            $question->seo_meta_keywords = Input::get('seo_meta_keywords');
            $question->seo_meta_description = Input::get('seo_meta_description');
            $question->last_update_date = date("Y-m-d");
            $question->last_update_admin_id = Session::get('admin_id');
            $question->deleted = 0;
               $question->lang=Session::get('lang');
            $question->save();
            $question_id = $question->id;

            foreach ($input['newchoice'] as $choice) {
                if ($choice != null) {
                    $question_choice = new QuestionnaireChoice();
                    $question_choice->questionnaire_id = $question_id;
                    $question_choice->choice = $choice;
                    $question_choice->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $question_choice->seo_meta_description = Input::get('seo_meta_description');
                    $question_choice->last_update_date = date("Y-m-d");
                    $question_choice->last_update_admin_id = Session::get('admin_id');
                    $question_choice->deleted = 0;
                       $question->lang=Session::get('lang');
                    $question_choice->save();
                }
            }

            return Redirect::to('administrator/question');
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (Session::has('admin_name')) {
            $question = Questionnaire::find($id);
            $choice_details = DB::table('questionnaire_choice')->where('deleted',0)->where('questionnaire_id', $id)->select('id', 'choice')->get();
            return View::make("admin.question.create")->with('question', $question)->with("choice_details", $choice_details);
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array(
                'question' => 'required',
                'oldchoice' => 'required'
            );
            $messages = array(
                'oldchoice.required' => 'choice field is required'
            );
            $validator = Validator::make($input, $rules, $messages);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to('administrator/question/' . Input::get('id') . '/edit')->withErrors($validator);
            }
            $question = Questionnaire::find($id);
            $question->question = Input::get('question');
            $question->seo_meta_keywords = Input::get('seo_meta_keywords');
            $question->seo_meta_description = Input::get('seo_meta_description');
            $question->last_update_date = date("Y-m-d");
            $question->last_update_admin_id = Session::get('admin_id');
            $question->deleted = 0;
            $question->save();

            foreach ($input['oldchoice']as $key => $choice) {
                if ($choice != null) {
                    $question_choice = QuestionnaireChoice::find($key);
                    $question_choice->questionnaire_id = $id;
                    $question_choice->choice = $choice;
                    $question_choice->seo_meta_keywords = Input::get('seo_meta_keywords');
                    $question_choice->seo_meta_description = Input::get('seo_meta_description');
                    $question_choice->last_update_date = date("Y-m-d");
                    $question_choice->last_update_admin_id = Session::get('admin_id');
                    $question_choice->deleted = 0;
                    $question_choice->save();
                }
            }
            if (isset($input['newchoice'])) {
                foreach ($input['newchoice'] as $key => $choice) {
                    if ($choice != null) {
                        $question_choice = new QuestionnaireChoice();
                        $question_choice->questionnaire_id = $id;
                        $question_choice->choice = $choice;
                        $question_choice->seo_meta_keywords = Input::get('seo_meta_keywords');
                        $question_choice->seo_meta_description = Input::get('seo_meta_description');
                        $question_choice->last_update_date = date("Y-m-d");
                        $question_choice->last_update_admin_id = Session::get('admin_id');
                        $question_choice->deleted = 0;
                        $question_choice->save();
                    }
                }
            }

            return Redirect::to('administrator/question');
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $question = Questionnaire::find($id);
        $question->deleted = 1;
        $question->save();
         return Redirect::to('administrator/question');
    }

    public function destroy_choice($id) {
        $question = QuestionnaireChoice::find($id);
        $question->deleted = 1;
        $question->save();
        return Redirect::to('administrator/question/'.$question->questionnaire_id.'/edit');
    }

}
