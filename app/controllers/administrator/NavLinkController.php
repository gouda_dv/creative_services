<?php

class NavLinkController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        if (Session::has('admin_name')) {
            $lang = Session ::get('lang');
            $all_links = NavLink::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            foreach ($all_links as $link) {
                if (isset($link['nav_id'])) {
                    $category = Nav::find($link['nav_id']);
                    $link['nav_id'] = $category->title;
                }
                if (isset($link['parent'])) {
                    $category = NavLink::find($link['parent']);
                    $link['parent'] = $category->title;
                }
            }
            return View::make('admin.nav_link.index')->with('all_links', $all_links);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        if (Session::has('admin_name')) {
            $lang = Session::get('lang');
            $navs = Nav::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedNavs = array();
            $selectedNavs[''] = Lang::get('global.choose');
            foreach ($navs as $nav) {
                $selectedNavs[$nav->id] = $nav->title;
            }
            $parent_links = NavLink::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedLinks = array();
            $selectedLinks[''] = Lang::get('global.choose');
            foreach ($parent_links as $parent) {
                $selectedLinks[$parent->id] = $parent->title;
            }

            $screens = Screen::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedScreens = array();
            $selectedScreens[''] = Lang::get('global.choose');
            foreach ($screens as $screen) {
                $selectedScreens[$screen->id] = $screen->title;
            }
            return View::make('admin.nav_link.create')->with("selectedNavs", $selectedNavs)->with("selectedLinks", $selectedLinks)->with("selectedScreens", $selectedScreens);
        } else {
            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array('title' => 'required');

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::back()->withErrors($validator);
            }
            $nav_link = new NavLink();

            if (Input::get('screen') && Input::get('record') && Input::get('record') != 0) {
                $screen = Screen::find(Input::get('screen'));
                $screen_name = $screen->code;
                $nav_link->link = $screen_name . '/' . Input::get('record');
            } elseif (Input::get('screen')) {
                $screen = Screen::find(Input::get('screen'));
                $screen_name = $screen->code;
                $nav_link->link = $screen_name;
            }
            if (Input::get('nav_id') == 0 && Input::get('parent') == 0) {

                return Redirect::back()->withErrors("you must insert nav or parent");
            }


            if (Input::get('nav_id') != 0 && Input::get('parent') == 0) {
                $nav_link->nav_id = Input::get('nav_id');
                $nav_link->parent = null;
            } elseif (Input::get('parent') != 0 && Input::get('nav_id') == 0) {
                $nav_link->nav_id = null;
                $nav_link->parent = Input::get('parent');
            }

            $nav_link->title = Input::get('title');
            $nav_link->summery = Input::get('summery');
            $image = Input::get('image');
             if ($image) {
                $destinationPath = 'uploads/nav_link/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    $nav_link->image = $filename_image;
                } else {
                    return Redirect::to('administrator/nav_link/create')->withErrors("can't save");
                }
            }
           // $nav_link->image = Input::get('title');
            $nav_link->last_update_date = date("Y-m-d");
            $nav_link->last_update_admin_id = Session::get('admin_id');
            $nav_link->deleted = 0;
            $nav_link->lang = Session::get('lang');
            $nav_link->save();
            return Redirect::to('administrator/nav_link');
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if (Session::has('admin_name')) {
            $lang = Session::get('lang');
            $navs = Nav::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedNavs = array();
            $selectedNavs[''] = Lang::get('global.choose');
            foreach ($navs as $nav) {
                $selectedNavs[$nav->id] = $nav->title;
            }
            $parent_links = NavLink::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedLinks = array();
            $selectedLinks[''] = Lang::get('global.choose');
            foreach ($parent_links as $parent) {
                $selectedLinks[$parent->id] = $parent->title;
            }

            $screens = Screen::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            $selectedScreens = array();
            $selectedScreens[''] = Lang::get('global.choose');
            foreach ($screens as $screen) {
                $selectedScreens[$screen->id] = $screen->title;
            }
            $nav_link = NavLink::find($id);
            $link = explode("/", $nav_link->link);
            // print_r($link);exit;
            $screen_id = Screen::whereRaw("deleted=0 AND lang= '$lang' AND code='$link[0]'")->get();
            foreach ($screen_id as $screen) {
                $nav_link->screen = $screen->id;
            }
            if (count($link) != 1) {
                $nav_link->record = $link[1];
            }
            return View::make('admin.nav_link.create')->with("selectedNavs", $selectedNavs)->with("selectedLinks", $selectedLinks)->with("selectedScreens", $selectedScreens)->with("nav_link", $nav_link);
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Session::has('admin_name')) {
            $input = Input::all();
            $rules = array('title' => 'required');

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::back()->withErrors($validator);
            }
            $nav_link = NavLink::find(Input::get('id'));

            if (Input::get('screen') && Input::get('record') && Input::get('record') != 0) {
                $screen = Screen::find(Input::get('screen'));
                $screen_name = $screen->code;
                $nav_link->link = $screen_name . '/' . Input::get('record');
            } elseif (Input::get('screen')) {
                $screen = Screen::find(Input::get('screen'));
                $screen_name = $screen->code;
                $nav_link->link = $screen_name;
            }
            if (Input::get('nav_id') == 0 && Input::get('parent') == 0) {

                return Redirect::back()->withErrors("you must insert nav or parent");
            }


            if (Input::get('nav_id') != 0 && Input::get('parent') == 0) {
                $nav_link->nav_id = Input::get('nav_id');
                $nav_link->parent = null;
            } elseif (Input::get('parent') != 0 && Input::get('nav_id') == 0) {
                $nav_link->nav_id = null;
                $nav_link->parent = Input::get('parent');
            }
            $image = Input::get('image');  
            if ($image) {
                $destinationPath = 'uploads/nav_link/';
                $extension_image = $image->getClientOriginalExtension();
                $filename_image = str_random(12) . "." . $extension_image;
                $upload_success_image = Input::file('image')->move($destinationPath, $filename_image);
                if ($upload_success_image) {
                    File::delete('uploads/nav_link/' . $nav_link->image);
                    $nav_link->image = $filename_image;
                } else {
                    return Redirect::to('administrator/nav_link/create')->withErrors("can't save");
                }
            }

            $nav_link->title = Input::get('title');
             $nav_link->summery = Input::get('summery');
            $nav_link->last_update_date = date("Y-m-d");
            $nav_link->last_update_admin_id = Session::get('admin_id');
            $nav_link->deleted = 0;
            $nav_link->lang = Session::get('lang');
            $nav_link->save();
            return Redirect::to('administrator/nav_link');
        } else {

            return Redirect::to('administrator/admin/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $nav_link = NavLink::find($id);
        $nav_link->deleted = 1;
        $nav_link->save();
        return Redirect::to('administrator/nav_link');
    }

    public function screen_record($id) {
        if (Request::ajax()) {
            $screen = Screen::find($id);
            $screen_name = $screen->code;
            $records = DB::table($screen_name)->select('id', 'title')->get();
            return Response::json($records);
        }
    }

    public function set_order_navs() {
        if (Request::ajax()) {
            $list_order = $_POST['list_order'];
// convert the string list to an array
            $list = explode(',', $list_order);
            $i = 1;
            foreach ($list as $id) {
                $nav = Nav::find($id);
                $nav->sort = $i;
                $nav->save();

                $i++;
            }
        }
    }

    public function set_order_links() {
        if (Request::ajax()) {
            $list_order = $_POST['list_order'];
// convert the string list to an array
            $list = explode(',', $list_order);
            $i = 1;
            foreach ($list as $id) {
                $nav = NavLink::find($id);
                $nav->sort = $i;
                $nav->save();
                $i++;
            }
        }
    }
    
        public function view_order_navs() {
            $lang = Session::get('lang');
        $all_links = Nav::whereRaw("deleted=0 AND lang= '$lang'")->orderBy('sort', 'ASC')->get();
        return View::make('admin.nav_link.nav_sort')->with('all_links', $all_links);
    }

    public function view_order_links($id) {
        $lang = Session::get('lang');
        $all_links = NavLink::whereRaw("deleted=0 AND lang= '$lang' AND nav_id='$id'")->orderBy('sort', 'ASC')->get();
        return View::make('admin.nav_link.link_sort')->with('all_links', $all_links);
    }
    
        public function view_order_internal_links($id) {
            $lang = Session::get('lang');
        $all_links = NavLink::whereRaw("deleted=0 AND lang= '$lang' AND parent='$id'")->orderBy('sort', 'ASC')->get();
        return View::make('admin.nav_link.internal_link_sort')->with('all_links', $all_links);
    }

}
