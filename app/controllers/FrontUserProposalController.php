<?php

class FrontUserProposalController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     public function index() {
        if(Session::has('id'))
        {
        
       $user_id=Session::get('id');
      $result = DB::table('user')
                ->select('*')
                ->where('id', '=', $user_id)
                ->get();
        $user = User::find($result[0]->id);
      
         return View::make('website.sign_in')->with('user',$user);
        }
       else
           {
          
           return View::make('website.sign_in');
            
           }
    }
    
    public function create() {
   
      return Redirect::to('proposal');
       
    }

    public function store() {
      
            $proposal = new UserProposal();
           // $proposal->seo_meta_keywords = Input::get('seo_meta_keywords');
           // $proposal->seo_meta_description = Input::get('seo_meta_description');
            
            $proposal->title = Input::get('proposal_type');
            $proposal->content=Input::get('message');
            if(Session::has('id')){
              $proposal->user_id=Session::get('id'); 
              $proposal->name = Session::get('name');
              
              $proposal->email = Session::get('email');
             
            }else{
                 $proposal->name = Input::get('name');
              $proposal->email = Input::get('email');
              $proposal->user_id=0;  
            }
           
            $proposal->deleted = 0;
            $proposal->save();
           $params['title']=Lang::get('global.user_proposal');
$params['message']=Lang::get('global.send_proposal');
           // $flag = 0;
           // Session::put('flag', $flag);
            return View::make('website.success_view')->with('params',$params);
       

    }
     public function show($id) {
    
    }

    public function edit($id) {
 
    }

    public function update($id) {
       
    }
 
     public function destroy($id) {
       
    
    }

    
    
    
}