<?php

class FrontContentCategoryController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
    	
    	$lang=Session ::get('lang');
		if($lang==null) {
			Session::put('lang', 'ar');
			$lang=Session ::get('lang');
		}
		
        $lang = Session ::get('lang');
        $all_content = Content::whereRaw("deleted=0 AND lang= '$lang' AND content_category_id='$id'")->orderBy('id', 'desc')->get();  

        $category_row = ContentCategory::whereRaw("deleted=0 AND id='$id' AND lang= '$lang' ")->get();
		
       // $latest_content = Content::getByLangAndLimit($lang, 11); 
        
        return View::make("website.content_category.show")
        ->with('all_content', $all_content)
        ->with('category_row', $category_row)
//        ->with('latest_content', $latest_content)
        ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
    }

}
