<?php

class FrontMapController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

		$lang = Session ::get('lang');
		$all_branches = Branch::whereRaw("deleted=0 AND lang= '$lang' ")->get();
		return View::make('website.branch.map')->with('all_branches', $all_branches);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('website.branch.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$branch = branch::find($id);

		$gallery_dir =  'uploads/gallery/'.$branch->gallery_id;
		$branch_galleries = array();
		if (is_dir($gallery_dir)) {
			foreach (scandir($gallery_dir) as $file) {
				if ('.' === $file) continue;
				if ('..' === $file) continue;
				$branch_galleries[] = $file;
			}
		}

		return View::make("website.branch.show")->with('branch', $branch)->with('branch_galleries', $branch_galleries);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		return View::make("website.branch.index")->with('branch', $branch);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
			
	}

}
