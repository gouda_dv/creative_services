<?php

class FrontContentController extends BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {

		$lang = Session ::get('lang');
		if ($lang == null) {
			Session::put('lang', 'ar');
			$lang = Session ::get('lang');
		}

		$content = Content::find($id);
		//$content->read_count++;
		//$content->save();
//		$content_cat = $content->content_category_id;
//		$cats = ContentCategory::find($content_cat);
//		$content->content_category_id = $cats->title;
//		$latest_content = Content::getByLangAndLimit($lang, 11);
//		$most_read = Content::getMostRead($lang);
                $seo_meta_keywords= $content->seo_meta_keywords;
$seo_meta_description=$content->seo_meta_description;
		return View::make("website.content.show")
//		->with('latest_content', $latest_content)
//		->with('most_read', $most_read)
		->with('content', $content)
                ->with('seo_meta_keywords', $seo_meta_keywords)
                        ->with('seo_meta_description', $seo_meta_description);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {

	}
	public function open_uploads_popup($id) {
		$lang = Session ::get('lang');
		if ($lang == null) {
			Session::put('lang', 'ar');
			$lang = Session ::get('lang');
		}

		$content = Content::find($id);
		return View::make("website.content.print")->with('content', $content);

	}
	public function email() {

		return View::make("website.content.email");

	}
	public function send_email()
	{
		$doc_id=$_POST['doc_id'];
//print $doc_id;exit;
		$params['message_title']=Lang::get('global.message_title');
		$params['message_body']=Lang::get('global.sending_email_message');
		$params['link_url']=Lang::get('global.link_url');
		$params['url']=url('content', $parameters = array('doc_id' => $doc_id), $secure = null);
			
		$params['subject']=Lang::get('global.sending_email');
		
		
		$params_email['to']=$_POST['email'];
		$params_email['subject']=Lang::get('global.sending_email');
//print_r($params);exit;
		$user_controller=new UserController();
		$params_email['message_body']=$user_controller->send_email_with_view($params);
		//$params_email['headers']=' ';

		$email_controller_object=new EmailController();
		return $email_controller_object->send_email($params_email);

	}



}
