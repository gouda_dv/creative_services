<?php

class EmailController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {


	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {

	}
	
	public function send_email($params)
	{
		$to = $params['to'];
		$subject = $params['subject'];
		$message_body = $params['message_body'];
		//$headers = $params['headers'];
		$headers= "From: Madinah History Center \r\n";
   $headers.= "Content-Type:text/html ; charset=utf-8 " . "\r\n";
		
		//$headers= "From: http://www.madinahhistorycenter.org\r\n";
              //  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		
		if($to !=null){
			$result= mail($to, $subject, $message_body,  $headers);
			if($result ==1){
				$params = array('title'=>Lang::get('global.sending_email'),'message'=>Lang::get('global.send_doc_email') );
				return View::make('website.popup_success_view')->with('params', $params);
			}else{
				$params = array('title'=>Lang::get('global.sending_email_failed'),'message'=>Lang::get('global.sending_email_message_failed') );
				return View::make('website.popup_failed_view')->with('params', $params);
			}
		}else{
			echo '<script>window.close();</script>';
		}
	}


}
