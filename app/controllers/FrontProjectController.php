<?php

class FrontProjectController extends BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
           $lang = Session ::get('lang');
		if ($lang == null) {
			Session::put('lang', 'en');
			$lang = Session ::get('lang');
		}
            
            $all_projects = Project::whereRaw("deleted=0 AND lang= '$lang' ")->get();
            
            return View::make('website.project.index')->with('all_projects', $all_projects);
         
            
            

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {

		$lang = Session ::get('lang');
		if ($lang == null) {
			Session::put('lang', 'en');
			$lang = Session ::get('lang');
		}

		$project = Project::find($id);
		//$content->read_count++;
		//$content->save();
//		$content_cat = $content->content_category_id;
//		$cats = ContentCategory::find($content_cat);
//		$content->content_category_id = $cats->title;
//		$latest_content = Content::getByLangAndLimit($lang, 11);
//		$most_read = Content::getMostRead($lang);
//                $seo_meta_keywords= $service->seo_meta_keywords;
//$seo_meta_description=$service->seo_meta_description;
		return View::make("website.project.show")
//		->with('latest_content', $latest_content)
//		->with('most_read', $most_read)
//                         ->with('seo_meta_keywords', $seo_meta_keywords)
//                        ->with('seo_meta_description', $seo_meta_description)
		->with('project', $project) ;
              
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {

	}
	
	
	



}
