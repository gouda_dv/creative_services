<?php

class FrontPlaceController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

		$lang = Session ::get('lang');
		if ($lang == null) {
			Session::put('lang', 'ar');
			$lang = Session ::get('lang');
		}

		$latest_content = Content::getByLangAndLimit($lang, 11);

		$all_places = Place::whereRaw("deleted=0 AND lang= '$lang' ")->get();
		return View::make('website.place.index')->with('all_places', $all_places)
		->with('latest_content', $latest_content)
		;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('website.place.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$lang = Session ::get('lang');
		if ($lang == null) {
			Session::put('lang', 'ar');
			$lang = Session ::get('lang');
		}

		$place = Place::find($id);

		$gallery_dir = 'uploads/gallery/' . $place->gallery_id;
		$place_galleries = array();
		if (is_dir($gallery_dir)) {
			foreach (scandir($gallery_dir) as $file) {
				if ('.' === $file)
				continue;
				if ('..' === $file)
				continue;
				if ('thumbnail' === $file)
				continue;
				
				$place_galleries[] = $file;
			}
		}


		$latest_content = Content::getByLangAndLimit($lang, 11);
 $seo_meta_keywords= $place->seo_meta_keywords;
$seo_meta_description=$place->seo_meta_description;
		return View::make("website.place.show")->with('place', $place)->with('place_galleries', $place_galleries)
		->with('latest_content', $latest_content)
                         ->with('seo_meta_keywords', $seo_meta_keywords)
                        ->with('seo_meta_description', $seo_meta_description);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		return View::make("website.place.index")->with('place', $place);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {

	}

	public function open_uploads_popup($id) {
		$lang = Session ::get('lang');
		if ($lang == null) {
			Session::put('lang', 'ar');
			$lang = Session ::get('lang');
		}

		$doc = Place::find($id);
		return View::make("website.place.print")->with('doc', $doc);
	}

	public function email() {

		return View::make("website.place.email");
	}

	public function send_email()
	{

		$doc_id=$_POST['doc_id'];

		$params['message_title']=Lang::get('global.message_title');
		$params['message_body']=Lang::get('global.sending_email_message');
		$params['link_url']=Lang::get('global.link_url');
		$params['url']=url('madinah_place', $parameters = array('doc_id' => $doc_id), $secure = null);

		$params['subject']=Lang::get('global.sending_email');
		
		$params_email['to']=$_POST['email'];
		$params_email['subject']=Lang::get('global.sending_email');

		$user_controller=new UserController();
		$params_email['message_body']=$user_controller->send_email_with_view($params);
		//$params_email['headers']=' ';

		$email_controller_object=new EmailController();
		return $email_controller_object->send_email($params_email);
	}

}
