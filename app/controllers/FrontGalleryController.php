<?php

class FrontGalleryController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$lang = Session ::get('lang');
		$galleries = Gallery::whereRaw("deleted=0 AND active=1 AND lang= '$lang' ")->get();

		return View::make("website.gallery.index")->with('galleries', $galleries);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		
		$gallery = Gallery::find($id);
		
		$gallery_dir = 'uploads/gallery/'.$id;
		$images = array();
		if (is_dir($gallery_dir)) {
			foreach (scandir($gallery_dir) as $file) {
				if ('.' === $file) continue;
				if ('..' === $file) continue;
				if ('thumbnail' === $file) continue;
				$images[] = $file;
			}
		}
		return View::make("website.gallery.show")->with('images', $images)->with('gallery', $gallery);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {

	}

}
