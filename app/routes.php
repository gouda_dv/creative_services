<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */
Route::resource('/', 'HomeController');

Route::post('admin/validate', 'AdminController@validate');
Route::get('admin/login', 'AdminController@login');
Route::get('admin/logout', 'AdminController@logout');
Route::resource('admin', 'AdminController');
Route::resource('administrator/partner','PartnerController');
Route::resource('administrator/project','ProjectController');
Route::resource('administrator/header_slider','HeaderSliderController');

Route::get('administrator', 'AdminController@index');

Route::post('administrator/admin/validate', 'AdminController@validate');
Route::get('administrator/admin/login', 'AdminController@login');
Route::get('administrator/admin/logout', 'AdminController@logout');
Route::resource('administrator/admin', 'AdminController');
Route::resource('administrator/settings', 'SettingsController');
Route::resource('administrator/content', 'ContentController');
Route::get('administrator/nav_link/screen_record/{id}', 'NavLinkController@screen_record');

Route::get('administrator/nav_link/view_order_navs', 'NavLinkController@view_order_navs');
Route::get('administrator/nav_link/view_order_links/{id}', 'NavLinkController@view_order_links');
Route::get('administrator/nav_link/view_order_internal_links/{id}', 'NavLinkController@view_order_internal_links');
Route::post('administrator/nav_link/set_order_navs', 'NavLinkController@set_order_navs');
Route::post('administrator/nav_link/set_order_links', 'NavLinkController@set_order_links');
Route::resource('administrator/nav_link', 'NavLinkController');
Route::resource('administrator/service', 'ServiceController');
Route::resource('administrator/service_category', 'ServiceCategoryController');
Route::resource('administrator/content', 'ContentController');
Route::resource('administrator/branch', 'BranchController');
Route::resource('administrator/contact_us', 'ContactUsController');

Route::resource('header_slider', 'FrontHeaderSliderController');
Route::resource('email','FrontEmailController');
Route::resource('content', 'FrontContentController');

Route::resource('service', 'FrontServiceController');
Route::resource('service_category', 'FrontServiceCategoryController');
Route::resource('content_category', 'FrontContentCategoryController');
Route::resource('project', 'FrontProjectController');
Route::resource('contact_us', 'FrontContactUsController');
Route::resource('branch', 'FrontBranchController');

Route::resource('map', 'FrontMapController');



//Route::get('header_slider/ActiveAjax/{id}','HeaderSliderController@ActiveAjax');
//Route::get('gallery/open_uploads_popup/{id}', 'GalleryController@open_uploads_popup');
//Route::post('gallery/uploads/{id}', 'GalleryController@uploads');
//
//
//Route::resource('related_links','RelatedLinksController');
//
//Route::resource('honor_members','HonorMembersController');
//Route::resource('sponsors','SponsorsController');
//
//Route::resource('video','FrontVideoController');
//Route::resource('video_category','FrontVideoCategoryController');
//Route::resource('center_members', 'CenterMemberController');
//Route::resource('gallery', 'FrontGalleryController');
//Route::get('content/open_uploads_popup/{id}', 'FrontContentController@open_uploads_popup');
//Route::get('content/email', 'FrontContentController@email');
//Route::post('content/send_email', 'FrontContentController@send_email');
//Route::resource('content', 'FrontContentController');
//Route::resource('content_category', 'FrontContentCategoryController');
////Route::get('question/destroy_choice/(:any)', array('as' => 'id', 'uses' => 'QuestionnaireController@destroy_choice'));
//Route::get('question/destroy_choice/{id}', 'QuestionnaireController@destroy_choice');
//
////Route::resource('question', 'QuestionnaireController');
//Route::resource('question_choice', 'QuestionnaireChoiceController');
//
//Route::resource('settings', 'SettingsController');
//Route::get('madinah_place/open_uploads_popup/{id}', 'FrontPlaceController@open_uploads_popup');
//Route::get('madinah_place/email', 'FrontPlaceController@email');
//Route::post('madinah_place/send_email', 'FrontPlaceController@send_email');
//Route::resource('madinah_place', 'FrontPlaceController');
//Route::get('doc/open_uploads_popup/{id}', 'FrontDocController@open_uploads_popup');
//Route::get('doc/email', 'FrontDocController@email');
//Route::post('doc/send_email', 'FrontDocController@send_email');
//Route::resource('doc', 'FrontDocController');
//Route::resource('doc_category', 'FrontDocCategoryController');
//Route::get('header_slider/open_uploads_popup/{id}', 'FrontHeaderSliderController@open_uploads_popup');
//Route::get('header_slider/email', 'FrontHeaderSliderController@email');
//Route::post('header_slider/send_email', 'FrontHeaderSliderController@send_email');
//Route::resource('header_slider', 'FrontHeaderSliderController');
//Route::get('member_of_honor/open_uploads_popup/{id}', 'FrontMemberOfHonorController@open_uploads_popup');
//Route::get('member_of_honor/email', 'FrontMemberOfHonorController@email');
//Route::post('member_of_honor/send_email', 'FrontMemberOfHonorController@send_email');
//Route::resource('member_of_honor', 'FrontMemberOfHonorController');
//Route::get('sponsor/open_uploads_popup/{id}', 'FrontSponsorController@open_uploads_popup');
//Route::get('sponsor/email', 'FrontSponsorController@email');
//Route::post('sponsor/send_email', 'FrontSponsorController@send_email');
//Route::resource('sponsor', 'FrontSponsorController');
//



  Route::get('/administrator/lang/en',function (){
      Session::put('lang', 'en');
     
   return Redirect::back();
    
});


  Route::get('/administrator/lang/ar',function (){
    Session::put('lang', 'ar');
   
   return Redirect::back();
    
});
 Route::get('/lang/ar',function (){
      Session::put('lang', 'ar');
     
   return Redirect::back();
    
});
 Route::get('/lang/en',function (){
      Session::put('lang', 'en');
     
   return Redirect::back();
    
});
//Route::get('/administrator/lang/fr',function (){
//      Session::put('lang', 'fr');
//     
//   return Redirect::back();
//    
//});
//Route::get('/administrator/lang/id',function (){
//      Session::put('lang', 'id');
//     
//   return Redirect::back();
//    
//});
//Route::get('/administrator/lang/or',function (){
//      Session::put('lang', 'or');
//     
//   return Redirect::back();
//    
//});
//Route::get('/administrator/lang/tr',function (){
//      Session::put('lang', 'tr');
//     
//   return Redirect::back();
//    
//});
//Route::get('/administrator/lang/fa',function (){
//      Session::put('lang', 'fa');
//     
//   return Redirect::back();
//    
//});



//Route::get('administrator', 'AdminController@index');
//
//Route::post('administrator/admin/validate', 'AdminController@validate');
//Route::get('administrator/admin/login', 'AdminController@login');
//Route::get('administrator/admin/logout', 'AdminController@logout');
//Route::resource('administrator/admin', 'AdminController');
//Route::get('administrator/header_slider/ActiveAjax/{id}','HeaderSliderController@ActiveAjax');
//
//Route::any('administrator/gallery/open_uploads_popup/{id}', 'GalleryController@open_uploads_popup');
//Route::any('administrator/gallery/uploads/{id}', 'GalleryController@uploads');
//
//Route::resource('administrator/header_slider','HeaderSliderController');
//Route::resource('administrator/related_links','RelatedLinksController');
//Route::resource('administrator/honor_members','HonorMembersController');
//Route::resource('administrator/sponsors','SponsorsController');
//Route::resource('administrator/videos','VideosController');
//Route::resource('administrator/center_members', 'CenterMemberController');
//Route::resource('administrator/gallery', 'GalleryController');
//Route::resource('administrator/content', 'ContentController');
////Route::get('question/destroy_choice/(:any)', array('as' => 'id', 'uses' => 'QuestionnaireController@destroy_choice'));
//Route::get('administrator/question/destroy_choice/{id}', 'QuestionnaireController@destroy_choice');
//Route::resource('administrator/question', 'QuestionnaireController');
//Route::resource('administrator/question_choice', 'QuestionnaireChoiceController');
//Route::resource('administrator/settings', 'SettingsController');
//Route::resource('administrator/place', 'PlaceController');
//Route::resource('administrator/user_proposal','UserProposalController');
//Route::resource('administrator/doc', 'DocController');
//Route::get('administrator/nav_link/screen_record/{id}', 'NavLinkController@screen_record');
//
//Route::get('administrator/nav_link/view_order_navs', 'NavLinkController@view_order_navs');
//Route::get('administrator/nav_link/view_order_links/{id}', 'NavLinkController@view_order_links');
//Route::get('administrator/nav_link/view_order_internal_links/{id}', 'NavLinkController@view_order_internal_links');
//Route::post('administrator/nav_link/set_order_navs', 'NavLinkController@set_order_navs');
//Route::post('administrator/nav_link/set_order_links', 'NavLinkController@set_order_links');
//Route::resource('administrator/nav_link', 'NavLinkController');
//
//
//Route::post('user/validate', 'UserController@validate');
//Route::get('user/login', 'UserController@login');
//Route::get('user/logout', 'UserController@logout');
//Route::get('user/activate/{auth}', 'UserController@activate');
//Route::any('user/send_email_with_view', 'UserController@send_email_with_view');
//Route::any('user/check_email_register', 'UserController@check_email_register');
//
//Route::resource('user','UserController');
//Route::resource('proposal','FrontUserProposalController');
//Route::post('questionnaire/vote','FrontQuestionnaireController@vote');
//Route::post('questionnaire/already_answerd','FrontQuestionnaireController@already_answerd');
//Route::resource('questionnaire','FrontQuestionnaireController');
//
//Route::resource('email','FrontEmailController');
//Route::any('email_template/send_email', 'FrontEmailTemplateController@send_email');
//
//Route::resource('email_template','FrontEmailTemplateController');


