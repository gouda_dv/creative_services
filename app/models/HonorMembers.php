<?php

class HonorMembers extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('seo_meta_keywords', 'seo_meta_description', 'title', 'summery','image','content','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'member_of_honor';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');
}
