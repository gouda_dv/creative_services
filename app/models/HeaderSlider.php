<?php

class HeaderSlider extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('seo_meta_keywords', 'seo_meta_description', 'title', 'summery','image','lang','content','deleted');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'header_slider';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');
}
