<?php

class QuestionnaireChoice extends Eloquent {

    protected $fillable = array( 'questionnaire_id','choice','seo_meta_keywords','seo_meta_description','question'
       ,'last_update_date','last_update_admin_id','deleted','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'questionnaire_choice';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

}
