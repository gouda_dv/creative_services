<?php

class ChoiceIp extends Eloquent {

    protected $fillable = array('ip', 'questionnaire_id','questionnaire_choice_id','seo_meta_keywords'
        ,'seo_meta_description','last_update_date','last_update_admin_id','deleted');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'questionnaire_choice_by_ip';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

	
    
}
