<?php

class Settings extends Eloquent {

    protected $fillable = array('facebook_link', 'twitter_link','youtube_link','google_plus_link'
        ,'instagram_link','linkedin_link','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'setting';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

}
