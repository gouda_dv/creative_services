<?php

class Videos extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('seo_meta_keywords', 'seo_meta_description', 'title', 'summery','video','youtube_link','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'video';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');

	public static function getLastOneByLangAndCategoryCode($lang, $category_code)
	{
		return DB::table('video')
		->select('video.*')
		->join('video_category', 'video.video_category_id', '=', 'video_category.id')
		->where('video.lang', '=', $lang)
		->where('video_category.code', '=', $category_code)
		->where('video.id', DB::raw("(select max(video.id) from video inner join video_category on video.video_category_id = video_category.id where video_category.code='$category_code')"))
		->get();

	}
}
