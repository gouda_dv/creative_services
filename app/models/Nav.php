<?php

class Nav extends Eloquent {

    protected $fillable = array('title', 'code','last_update_date','last_update_admin_id','deleted','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nav';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

}
