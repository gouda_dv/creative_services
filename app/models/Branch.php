<?php

class Branch extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('id','seo_meta_keywords', 'seo_meta_description', 'title', 'summery',
        'content','lang','deleted','map_Latitude','map_longitude');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branch';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');
}
