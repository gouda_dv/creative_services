<?php

class Doc extends Eloquent {

	protected $fillable = array('seo_meta_keywords','seo_meta_description','title'
	,'summery','image','file','content','last_update_date','last_update_admin_id',
            'deleted','lang','file_paid','file_price','file_currency_id','video',
            'youtube_link','video_paid','video_price','video_currency_id');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'doc';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	public $timestamps = false;

	public static function getByLangAndCategoryCode($lang, $category_code)
	{
		return DB::table('doc')
		->select('doc.*')
		->join('doc_category', 'doc.doc_category_id', '=', 'doc_category.id')
		->where('doc_category.lang', '=', $lang)
		->where('doc_category.code', '=', $category_code)
		->get();
	}

}
