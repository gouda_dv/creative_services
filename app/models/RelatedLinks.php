<?php

class RelatedLinks extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('seo_meta_keywords', 'seo_meta_description', 'title', 'link','image','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'related_link';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');
}
