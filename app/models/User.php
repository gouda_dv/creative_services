<?php

class User extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('name', 'email', 'password','mobile','phone','address','active','deleted');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');
}
