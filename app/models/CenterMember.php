<?php

class CenterMember extends Eloquent {

    protected $fillable = array('title', 'emails','mobiles','phones','summery','image','cv','content','last_update_date','last_update_admin_id','deleted','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'center_member';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

}
