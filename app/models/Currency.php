<?php

class Currency extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('code','title', 'last_update_date','last_update_admin_id','deleted','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'currency';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');
}
