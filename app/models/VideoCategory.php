<?php

class VideoCategory extends Eloquent {

    protected $fillable = array('');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'video_category';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

}
