<?php

class Questionnaire extends Eloquent {

    protected $fillable = array( 'seo_meta_keywords','seo_meta_description','question'
       ,'last_update_date','last_update_admin_id','deleted','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'questionnaire';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

	public static function getLastOneByLang($lang)
	{
		return DB::table('questionnaire')
		->where('questionnaire.lang', '=', $lang)
		->where('id', DB::raw("(select max(`id`) from questionnaire)"))
		->get();
	}
}
