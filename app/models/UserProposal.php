<?php

class UserProposal extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('seo_meta_keywords', 'seo_meta_description', 'title','summery','content','lang','name','email');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_proposal';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');
}
