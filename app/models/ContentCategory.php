<?php

class ContentCategory extends Eloquent {

    protected $fillable = array('');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'content_category';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

}
