<?php

class Content extends Eloquent {

    protected $fillable = array('content_category_id', 'seo_meta_keywords', 'seo_meta_description', 'title','date'
        , 'summery', 'image', 'content', 'last_update_date', 'last_update_admin_id', 'deleted', 'lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'content';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

    public static function getByLangAndCategoryCode($lang, $category_code, $limit = 1000) {
        return DB::table('content')
                        ->select('content.*')
                        ->join('content_category', 'content.content_category_id', '=', 'content_category.id')
                        ->where('content_category.lang', '=', $lang)
                        ->where('content_category.code', '=', $category_code)
                        ->orderBy('content.id', 'desc')
                        ->take($limit)
                        ->get();
    }

    public static function getByLangAndLimit($lang, $limit) {
        return DB::table('content')
                        ->select('content.*')
                        ->where('content.lang', '=', $lang)
                        ->orderBy('id', 'desc')
                        ->take($limit)->get();
    }

    public static function getMostRead($lang) {
        return DB::table('content')
                        ->select('content.*')
                        ->where('content.lang', '=', $lang)
                        ->orderBy('read_count', 'desc')
                        ->take(3)->get();
    }

}
