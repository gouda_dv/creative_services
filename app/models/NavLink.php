<?php

class NavLink extends Eloquent {

    protected $fillable = array('link','nav_id','title','summery'
        ,'parent','last_update_date','last_update_admin_id','deleted','lang','image');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nav_link';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

}
