<?php

class Partner extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('id','seo_meta_keywords', 'seo_meta_description', 'title', 'summery','image','content','lang','deleted');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'partner';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');
}
