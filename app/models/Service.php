<?php

class Service extends Eloquent {

    protected $fillable = array('seo_meta_keywords','seo_meta_description','service_category_id','title'
        ,'summery','content','image',
        'last_update_admin_id','deleted','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

}
