<?php

class ContactUs extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('id','name', 'email','mobile','title','content','lang','deleted');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact_us';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');
}
