<?php

class Email extends Eloquent {

    public $timestamps = false;
    protected $fillable = array('name','email');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//	protected $hidden = array('password', 'remember_token');
}
