<?php

class Gallery extends Eloquent {

    protected $fillable = array( 'seo_meta_keywords','seo_meta_description','title'
        ,'summery','image','last_update_date','last_update_admin_id','deleted','lang','active');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gallery';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

}
