<?php

class ServiceCategory extends Eloquent {

    protected $fillable = array('seo_meta_keywords','seo_meta_description','parent','title'
        ,'summery','content','image',
        'last_update_admin_id','deleted','lang');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_category';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;

}
