@extends('website.master')
@section('content')

@if(isset($video))
<div class='title'>
    <label>{{ Lang::get('global.title')}}</label>
    <div> {{$video->title}}</div>
</div>

<div class='cat'>
    <label>{{ Lang::get('global.category_id')}}</label>
    <div>{{$video->video_category_id}}</div>
</div>

<div class='summery'>
    <label>{{ Lang::get('global.summery')}}</label>
    <div>{{$video->summery}}</div>
</div>

<div class='video'>
    <label>{{ Lang::get('global.video')}}</label>
    <video width="400" controls>
        <source src="{{ URL::asset('uploads/video/'.$video->video) }}" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
</div>

<div class="link">
    <label>{{ Lang::get('global.youtube_link')}}</label>
    {{$video->youtube_link}}
</div>

@endif

@stop