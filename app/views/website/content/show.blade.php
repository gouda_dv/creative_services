@extends('website.master')
@section('content')
@if(isset($content))
<div class="container">
  <div class="row">
    <div class="col-md-12 content-box ">
      <h5>{{$content->title}}</h5>
      <div  class="collapsable" style="margin-top:16x;">
     
       <a class="fancybox-media" href="{{ URL::asset('uploads/content/'.$content->image) }}" > 
       
           <img  src="{{ URL::asset('uploads/content/'.$content->image) }}" class="img-thumbnail img-responsive marginl-img" align="right" style="width: 400px">
       </a>
   
        <div id="dv_summery">{{$content->summery}}
        <!-- 
        </div>
        <div id="dv_content">
         -->
        {{$content->content}}</div>    
        
        </div>
    </div>
  </div>
</div>
@endif
@stop
