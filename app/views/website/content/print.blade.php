@extends('website.master_popup')
@section('content')
<div class="t-artical pull-left"><span class="glyphicon glyphicon-chevron-left"></span>{{$content->title}}  

    <div class="resize">
        <img src="{{ URL::asset('website_design/images/print_icon-s.png') }}" height="25" width="25" class="print_btn" onClick="window.print()"/>
    </div>                   
</div>
@if($content->image!='')
<img src="{{ URL::asset('uploads/content/'.$content->image) }}" width="350" class="pull-left img-thumbnail" style="margin-left:30px;">
@endif

{{$content->content}}  
</div>  



@stop
