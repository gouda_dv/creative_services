@extends('website.master')
@section('content')
<div class="eventtittle">
      <div class="container">
    <div class="eventtittle2">
          <div class="row">
        <div class="col-md-12">
              <h4 class="pull-left"><span class="glyphicon glyphicon-book"></span>
				<?php if(isset($doc_category_row[0])) {?>
				{{$doc_category_row[0]->title}}
				<?php }?>
			 </h4>
            </div>
      </div>
        </div>
  </div>
    </div>
<div class="event">
      <div class="container">
    <div class="row"> </div>
    
        
        @foreach($all_doc as $doc)
    	<div class="col-md-6">
         <h3 class="pull-left"><span class="glyphicon glyphicon-minus"></span>{{$doc->title}} </h3>
          <p class="c-artical shadow"> <img src="{{ URL::asset('uploads/doc/'.$doc->image) }}" width="242" height="182" class="pull-left img-thumbnail" style="margin-left:10px;">
          {{$doc->summery}} <br/>
        <br/>
        <a href="{{ URL::asset('uploads/doc/'.$doc->file) }}" target="_blank" class="a-btn" style="clear:both;"> <span class="a-btn-symbol">Z</span> <span class="a-btn-text">{{ Lang::get('global.file')}}</span><span class="a-btn-slide-icon"></span> </a> </p>
          <div> </div>
        </div>
    	@endforeach
    
    
    
    
    
    
    <!-- 
    <div style="clear:both"></div>
        <center>
              <ul class="pagination ">
            <li><a href="#">&laquo;</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&raquo;</a></li>
          </ul>
      </center>
     -->
    

    
    
  </div>
</div>
@stop