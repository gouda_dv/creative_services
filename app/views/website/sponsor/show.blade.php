@extends('website.master')
@section('content')
@if(isset($row))

<div class="eventtittle">
    <div class="container">
        <div class="eventtittle2">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="pull-left"><span class="glyphicon glyphicon-book"></span>{{Lang::get('global.Sponsors')}}</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="event">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="artical">
                    <div class="t-artical pull-left"><span class="glyphicon glyphicon-chevron-left"></span>{{$row->title}}
                        <div class="resize"> <a href="#" class="fontSizePlus">A+</a> | <a href="#" class="fontReset">{{ Lang::get('global.default_font_size')}}</a> | <a href="#" class="fontSizeMinus">A-</a></div>
                    </div>
                    <div class="c-artical intro"> <img src="{{ URL::asset('uploads/sponsors/'.$row->image) }}" width="500" class="pull-left img-thumbnail" style="margin-left:10px;"> 
                        {{$row->content}}   
                    </div>  
                    <div class="col-lg-6 pull-right">
                        <div style="float:left; margin-right:30px; padding-top: 10px;"> <img src="{{ URL::asset('website_design/images/download.png') }}" alt="{{ Lang::get('global.subscribe_with_us')}}" height="25" width="25"></div>
                        <div style="float:left; margin-right:30px; padding-top: 10px;" class="print" ><img src="{{ URL::asset('website_design/images/print_icon-s.png') }}" height="25" width="25"></div>
                        <div style="float:left; margin-right:30px; padding-top: 10px;" class="email"> <img src="{{ URL::asset('website_design/images/envolp.png') }}"  height="25" width="25"></div>
                    </div>

                    <div class="col-lg-6 pull-left" style="vertical-align: middle">
                        <div style="height:35px; float:right; padding-top: 10px;"> <h1>{{ Lang::get('global.subscribe_with_us')}}</h1></div>
                        <div class="social" id="css3" style="padding-top: 10px;">
                            <li class="facebook"> <a target="_blank" href="http://www.facebook.com/sharer.php?u={{ URL::to('sponsor/'.$row->id) }}&t={{urlencode($row->title)}}"></a></li>
                            <li class="linkedin"> <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{ URL::to('sponsor/'.$row->id) }}&title={{urlencode($row->title)}}&summary={{urlencode($row->summery)}}&source={{ Lang::get('global.website_title')}}"></a></li>
                            <li class="google">   <a target="_blank" href="https://plus.google.com/share?url={{ URL::to('sponsor/'.$row->id) }}"></a></li>
                            <li class="twitter">  <a target="_blank" href="http://twitter.com/home?status={{ URL::to('sponsor/'.$row->id) }}"></a></li>
                            <!--  
                            <li class="instgram"><a target="_blank" href="http://www.facebook.com/"></a></li>
                            -->
                        </div>
                    </div>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
</div>


<!-- last add -->
<div class="news">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row eventtittle2">
                    <h4 class="pull-left"><span class="glyphicon glyphicon-folder-open"></span>{{ Lang::get('global.most_read')}}</h4>
                </div>
                <div class="c-thunb">
                    <?php
                    $lang = Session::get('lang');
                    $result = DB::table('sponsor')->where('id', '<>', $row->id)->where('deleted', '=', 0)->Where('lang', $lang)->orderBy('read_count', 'DESC')->take(3)->get();
                    foreach ($result as $value) {
                        echo' <div class="col-md-4"> ';
                             if($value->image != "") {    
                      echo'  <div class="thumbnail"> <img src="' . URL::asset("uploads/sponsors/" . $value->image) . '" width="242" height="182" alt="" class=" img-thumbnail">  ';
                          
                       }else{
                           echo'    <div class="thumbnail"> <img src="'. URL::asset("website_design/images/articalpic.jpg").'" width="242" height="182" alt="" class=" img-thumbnail"> ';
                       }
                      echo '  <div class="caption">
                                <h2>' . $value->title . '</h2> ';
                      echo mb_substr($value->summery, 0, 198,'UTF-8');
                              
                             echo'   <p></p>
                                <center>
                                    <div   class="readmore" > <a href="' . URL::to("sponsor/" . $value->id) . '">&raquo; ' . lang::get('global.read_more') . '</a></div >
                                </center>
                            </div>
                        </div>
                    </div>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row eventtittle2">
                    <h4 class="pull-left"><span class="glyphicon glyphicon-film"></span>اضيف حديثاً </h4>
                </div>
                <div id="navigation-block">
                    <ul id="sliding-navigation">
                        @foreach($latest_content as $contents)
                        <li class="sliding-element"><a href="{{URL::to('content/'.$contents->id)}}"><span class="glyphicon glyphicon-ok"></span>{{$contents->title}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> 
@endif
<script>

    $('.print').on('click', function() {
        // window.open("http://www.w3schools.com");
        window.open("{{ URL::to('sponsor/open_uploads_popup') }}/{{$row->id}}", '1412233895408', 'width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
    });

    $('.email').on('click', function() {
// alert({{$row->id}});
       window.open("{{ URL::to('sponsor/email') }}?pop_up_id={{$row->id}}", '1412233895408', 'width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=320,top=50%,center=0');
    });



</script>

@if(isset($flag) && $flag==1)
{{lang::get('global.send_doc_email')}}
@endif
@stop