@extends('website.master_popup')

@section('content')
<?php 
$doc_id=$_GET['pop_up_id'];
//echo $doc_id;
?>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
{{ Form::open(array('role'=>'form','url'=>'sponsor/send_email','files' => true,'method'=>'post')) }}

 <div class="form-group">
<label class="control-label"> <?php echo Lang::get('global.emails'); ?></label>
<div class="controls">
    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input type="email" required="required" class="form-control" name="email" id="email" placeholder="<?php echo Lang::get('global.emails'); ?>" value="" >
    </div>
</div>
</div>
<!--<a id="send_btn" href="#">  <?php //echo Lang::get('global.send'); ?></a> -->
<button type="submit" data-theme="b" data-icon="check" data-inline="true" >{{ Lang::get('global.send') }}</button>
{{ Form::close() }}
@stop
