
@extends('website.master')
@section('content')
<div class="eventtittle">
      <div class="container">
    <div class="eventtittle2">
          <div class="row">
        <div class="col-md-12">
              <h4 class="pull-left"><span class="glyphicon glyphicon-film"></span>{{ Lang::get('global.videos')}}</h4>
            </div>
      </div>
        </div>
  </div>
    </div>
<div class="event">
      <div class="container">
    <div class="row">
          <div class="col-md-12">
        @foreach($all_video as $video)
        <!-- 
        <article class="video" >
              <figure> <a class="fancybox-media" href="{{$video->youtube_link}}"> <img class="videoThumb" src="{{$video->youtube_link}}/0.jpg" style="height: 180px;" ></a> </figure>
         <br/>
         <h2 class="videoTitle" style="margin-top: 40px!important;">{{$video->title}}</h2>
         </article>
         -->
         
        <article class="video" style="width:29%; height:250px">
        <ul class="youtube-videogallery youtube-videogallery-container">
		<li class="youtube-videogallery-item" style="height: 280px!important;">
		         <a rel="media-gallery" title="{{$video->title}}" data-youtube-id="q78zlyWLqG8" href="{{$video->youtube_link}}" class="youtube-videogallery-link" style="width:320px ;">
		<img style="margin-left: -20px; margin-top: -20px;" class="youtube-videogallery-play" src="" title="play">
		<img class="youtube-videogallery-img" src="{{$video->youtube_link}}/0.jpg" style="width:320px">
		<span class="youtube-videogallery-title" >{{$video->title}}</span>
		</a>
		</li>
		</ul>
		</article>
         @endforeach   
        
        <div style="clear:both"></div>

      </div>
      
        </div>
  </div>
</div>

<div style="height: 50px;"></div>


@stop