@extends('website.master_popup')
@section('content')
<div class="t-artical pull-left"><span class="glyphicon glyphicon-chevron-left"></span>{{$doc->title}}  

    <div class="resize">
        <img src="{{ URL::asset('website_design/images/print_icon-s.png') }}" height="25" width="25" class="print_btn" onClick="window.print()"/>
    </div>                   
</div>
@if($doc->image!='')
<img src="{{ URL::asset('uploads/honor_members/'.$doc->image) }}" width="350" class="pull-left img-thumbnail" style="margin-left:30px;">
@endif

{{$doc->content}}  
</div>  



@stop
