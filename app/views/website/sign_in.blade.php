@extends('website.master')
@section('content')
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif



<?php if(isset($user)) { 
    $name=$user->name;
    $email=$user->email;
  
 } else{
$name='';
    $email='';
 } ?>

{{ Form::open(array('role'=>'form','route'=>'proposal.store','files' => true)) }}

<div class="eventtittle">
        <div class="container">
    <div class="eventtittle2">
            <div class="row">
        <div class="col-md-12">
                <h4 class="pull-left"><span class="

glyphicon glyphicon-envelope"></span> <?php echo Lang::get('global.user_proposal'); ?></h4>

          <div  id="send_res">   </div>  
              </div>
      </div>
          </div>
  </div>
      </div>

<div class="print">
        <div class="container">
    <div class="row">
            <div class="col-md-7">
        <form class="myfirstform" role="form">
                <div class="form-group">
                    
            <label class="control-label"> <?php echo Lang::get('global.full_name'); ?></label>
            <div class="controls">
                    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="text" class="form-control" name="name" placeholder="<?php echo Lang::get('global.full_name'); ?>" value="<?php echo $name;?>" required="required">
              </div>
                  </div>
          </div>
                <div class="form-group">
            <label class="control-label"><?php echo Lang::get('global.emails'); ?></label>
            <div class="controls">
                    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo Lang::get('global.emails'); ?>" value="<?php echo $email;?>" required="required">
              </div>
                  </div>
          </div>
                <div class="form-group ">
            <label class="control-label"><?php echo Lang::get('global.proposal_type'); ?></label>
            <div class="controls">
                    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                        <input type="text" class="form-control" id="proposal_type" name="proposal_type" placeholder="<?php echo Lang::get('global.proposal_type'); ?>" required="required">

              </div>
                  </div>
          </div>
                <div class="form-group ">
            <label class="control-label"><?php echo Lang::get('global.message'); ?></label>
            <div class="controls">
                    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                <textarea name="message" class="form-control " rows="4" cols="78" placeholder="<?php echo Lang::get('global.message'); ?>" required="required"></textarea>
              </div>
                  </div>
          </div>
                <div class="controls" style="margin-right: 40%;">
            <button type="submit" id="mybtn"  class="btn btn-primary" class="send_btn"><?php echo Lang::get('global.send'); ?></button>
          </div>
              </form>
      </div>
            <div class="col-md-5"> <img src="{{ URL::asset('website_design/images/proposal.png') }}" class="img-responsive"> </div>
            
            
          </div>
  </div>
      </div>


@stop

