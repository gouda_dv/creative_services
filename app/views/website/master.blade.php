<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>{{Lang::get('global.creative_service')}}</title>
        <link rel="icon" type="image/x-icon" href="{{URL::asset('website_design/images/favicon.ico') }}" />
        <link rel="stylesheet" type="text/css" href="{{URL::asset('website_design/css/bootstrap-arabic.min.css')}}">
        <link href="{{URL::asset('website_design/css/bootstrap-arabic-theme.min.css')}}" rel="stylesheet" type="text/css">
        <link  href="{{URL::asset('website_design/css/style.css')}}" rel="stylesheet" type="text/css">
        <link rel='stylesheet' href="{{URL::asset('website_design/social.css')}}" type='text/css' media='screen' />

        <!--menu-->
        <script type="text/javascript" src="{{URL::asset('website_design/js/jquery-2.1.1.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('website_design/js/common.js')}}"></script>
        <!--slider-->
        <noscript>
        <link rel="stylesheet" type="text/css" href="{{URL::asset('website_design/css/noscript.css')}}" />
        </noscript>
        <!--hover-->
        <script type="text/javascript" src="{{URL::asset('website_design/js/modernizr.custom.79639.js')}}"></script>
        <!---slider2-->
        <link  href="{{URL::asset('website_design/css/slider.css')}}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{URL::asset('website_design/js/responsiveCarousel.min.js')}}"></script>
        <!--end-slider2-->
        <!--[if gte IE 9]>
          <style type="text/css">
            .gradient {
               filter: none;
            }
          </style>
        <![endif]-->

<!--   <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script> -->
    <script type="text/javascript" src="{{URL::asset('website_design/source/jquery.fancybox.pack.js?v=2.1.5')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::asset('website_design/source/jquery.fancybox.css?v=2.1.5')}}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('website_design/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')}}" />
<script type="text/javascript" src="{{URL::asset('website_design/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')}}"></script>
<script type="text/javascript" src="{{URL::asset('website_design/source/helpers/jquery.fancybox-media.js?v=1.0.6')}}"></script>
<script type="text/javascript">
		$(document).ready(function() {

			$('.fancybox').fancybox();

			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});
		
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

		});
	</script>

<!--collnew-->
<script src="{{URL::asset('website_design/js/html5shiv.js')}}"></script>
<script src="{{URL::asset('website_design/js/rlaccordion.js')}}"></script>
<script src="{{URL::asset('website_design/js/scripts.js')}}"></script>
        <!--hide-paragraph-->
        <script src="{{URL::asset('website_design/js/hide_paragraphs.js')}}"></script>
        <script>
$(function() {
    $(".collapsable").hideParagraphs();
});
        </script>


    </head><body>
        <header>
            <div  class="bg-menu">
                <div class="container">
                    <div class="row">
                        <div class=" col-md-12">
                            <div id="nav">
                                <ul id="main-menu" class="sm sm-blue" >

                                    <?php
                                    $lang = Session ::get('lang');
                                    $nav_id = Nav::where('deleted', '=', 0)->Where('lang', $lang)->Where('code', 'main')->pluck('id');
                                    $links = DB::table('nav_link')->where('deleted', '=', 0)->Where('lang', $lang)->Where('nav_id', $nav_id)->whereNull('parent')->orderBy('sort', 'ASC')->get();
                                    // $links = NavLink::whereRaw("deleted=0 AND lang= '$lang' AND parent=null ")->get();
                                    foreach ($links as $link) {
                                        $sub_links = DB::table('nav_link')->where('deleted', '=', 0)->Where('lang', $lang)->where('parent', $link->id)->orderBy('sort', 'ASC')->get();
// sub_links have main menu == first level
                                        if (count($sub_links) != 0) {
                                            echo'<li>';
                                            echo'<a href="#" >' . $link->title . '</a>';
                                            echo '<ul class="sub-menu">';
                                            foreach ($sub_links as $link) {
                                                $sub_links2 = DB::table('nav_link')->where('deleted', '=', 0)->Where('lang', $lang)->where('parent', $link->id)->orderBy('sort', 'ASC')->get();


                                                if (count($sub_links2) != 0) {
                                                    echo'<li >';
                                                    echo'<a href="' . URL::to($link->link) . '" >' . $link->title . '</a>';
                                                    echo '<ul class="sub-menu">';
                                                    foreach ($sub_links2 as $link2) {

                                                        echo'<li><a href="' . URL::to($link2->link) . '">' . $link2->title . '</a></li>';
                                                    }
                                                    echo '</ul>';
                                                    echo"</li>";
                                                } else {
                                                    echo'<li><a href="' . URL::to($link->link) . '">' . $link->title . '</a></li>';
                                                }
                                            }
                                            echo '</ul>';
                                            echo"</li>";
                                        } else {
                                            echo'<li><a href="' . URL::to($link->link) . '"  >' . $link->title . '</a></li>';
                                        }
                                    }
                                    ?>

   @if(Session::get('lang')=='ar')
      <li id="menu-item-1">   <a  href='{{ URL::to('lang/en') }}' ><img src="{{URL::asset('website_design/images/logo-icon.png')}}" width="47" height="42" class="position"></a>  </li>                       
   @elseif(Session::get('lang')=='en')
      <li id="menu-item-1">    <a  href='{{ URL::to('lang/ar') }}'><img src="{{URL::asset('website_design/images/ar-icon.png')}}" width="47" height="42" class="position"></a> </li>                                  
   @endif                          
<!--   <li id="menu-item-1"><a title="" href="{{Request::url()}}/ar"><img src="{{URL::asset('website_design/images/ar-icon.png')}}" width="47" height="42" class="position"></a> </li>  
                   -->
         </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 "> <img src="{{URL::asset('website_design/images/logo.png')}}" width="1170" height="101" class="img-responsive center-block"> </div>
                </div>
            </div>
        </header>

        @yield('content')





        <!--end-midlle-->

        <div class="bg-topfooter">
            <div class="container">
                <div class="row">
                    <div class="content-box">
                        <div class="col-md-3 bg-subscribe ">
                            <div class="title-subscribe">{{Lang::get('global.newsletter')}}</div>

                            {{ Form::open(array('method'=>'post','class' => 'form-horizontal', 'role'=>'form','route'=>'email.store')) }}

                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label"></label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control space-v-input" id="email_registration_email" name="email">
                                    <div class="clearfix"></div>
                                    <a class="email_registration_btn btn btn-danger btn-xs pull-right btn-v-space space-h-button"style="color: #fff;" >{{lang::get('global.subscribe')}}</a>
                                    <!--                                        <button class=" email_registration_btn btn btn-danger btn-xs pull-right btn-v-space space-h-button " > {{Lang::get('global.subscribe')}}</button>-->
                                </div>
                            </div>
                            {{ Form::close() }}
                            <div id="email_registration_result"  style="height: 40px; display: none;"></div>

                        </div>
                        <div class="col-md-3  c-search ">
                            <h5>{{Lang::get('global.find_us_on_google_map')}}</h5>
                            <?php
                            $lang = Session ::get('lang');
                            $address = DB::table('setting')->get();
                            foreach ($address as $addresses) {
                                ?>
                                <a href="{{URL::to('map')}}" ><img src="{{URL::asset('website_design/images/s-map.png')}}" width="148" height="75" class="btn-v-space img-responsive" align="right"></a> <a href="{{URL::to('map')}}"> {{Lang::get('global.visit_our_location')}}</a> </div>
                        <?php } ?>
                        <div class="col-md-3 c-search">
                            <h5>{{Lang::get('global.search_for_products')}}</h5>
                            <form class="form-inline" role="search">
                                <div class="form-group">
                                    <input type="text" class="form-control" style="margin-right:20px;" placeholder="search .....">
                                    <button type="submit" class="btn btn-default pull-left"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3  c-search">
                            <h5>{{Lang::get('global.follow_us')}}</h5>
                            <div class="social-icons">
                                <?php
                                $lang = Session ::get('lang');
                                $settings = DB::table('setting')->get();
// $links = NavLink::whereRaw("deleted=0 AND lang= '$lang' AND parent=null ")->get();
                                foreach ($settings as $setting) {
                                    ?>
                                    <ul>
                                        <?php
                                        echo' <li class="twitter" style="background-color: #f0f0f0"> <a href=" ' . $setting->twitter_link . '" target="_blank">Twitter</a> </li>
                                    <li class="facebook" style="background-color: #f0f0f0"> <a href=" ' . $setting->facebook_link . '" target="_blank">Facebook</a> </li>
                                    <li class="googleplus" style="background-color: #f0f0f0"> <a href="' . $setting->google_plus_link . '" target="_blank">Google +r</a> </li>
                                   <li class="linkedin" style="background-color: #f0f0f0"> <a href="' . $setting->linkedin_link . '" target="_blank">Linkedin</a> </li>
';
                                    }
                                    ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end-topfooter-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class=" col-md-12">
                        <div class="footertxt"> © {{Lang::get('global.copy_right')}} <br/>
                            <a href="http://www.dv.com.sa/"></a> {{Lang::get('global.designed_developed')}} : <a href="http://www.dv.com.sa/">Digital Vision for Technical Solutions</a><img src="{{URL::asset('website_design/images/ro2a-footer.png')}}" width="33" height="20"> </div>

                    </div>
                </div>
            </div>
        </footer>

        <!-- slider --> 
        <script type="text/javascript" src="{{URL::asset('website_design/js/jquery.eislideshow.js')}}"></script> 
        <script type="text/javascript" src="{{URL::asset('website_design/js/jquery.easing.1.3.js')}}"></script> 
        <script type="text/javascript">
$(function() {
    $('#ei-slider').eislideshow({
        animation: 'center',
        autoplay: true,
        slideshow_interval: 3000,
        titlesFactor: 0
    });
});
        </script> 
        <!-- slider 2--> 
        <script type="text/javascript">
            $(function() {
                $('.crsl-items').carousel({
                    visible: 2,
                    itemMinWidth: 180,
                    itemEqualHeight: 370,
                    itemMargin: 9,
                });

                $("a[href=#]").on('click', function(e) {
                    e.preventDefault();
                });
            });
        </script>

        <script type="text/javascript">
            $("#email_registration_result").hide();
            $('.email_registration_btn').on('click', function() {

                var email = $("#email_registration_email").val();


                if (email !== "") {  // If something was entered

                    if (!isValidEmailAddress(email)) {
                        // alert("not valid");
                        alert("{{lang::get('global.enter_vaild_email')}}");
                        $('#email_registration_email').val('');
                        $("#email_registration_email").focus();   //focus on email field
                        return false;
                    }
                }

                $.ajax({
                    type: "POST",
                    data: "email=" + $('#email_registration_email').val(),
                    url: '{{url("email")}}', success: function(result) {

                        // $('.email_registration_form').hide();
                        $("#email_registration_result").show();
                        if (result == 0) {
                            alert("{{lang::get('global.enter_vaild_email')}}");
                        	$('#email_registration_email').val('');
//                            $("#email_registration_result").html("<p style='color: #599482;font-size: 15px;font-weight: bold;text-align: center;'>{{lang::get('global.compelete_data')}}</p>");
                        } else if (result == 1) {
                            alert("{{lang::get('global.already_subscribed')}}");
                        	$('#email_registration_email').val('');
                        //  $("#email_registration_result").html("<p style='color: #599482;font-size: 15px;font-weight: bold;text-align: center;'>{{lang::get('global.already_found')}}</p>");
                        } else if (result == 2) {
                            alert("{{lang::get('global.success_subscribtion')}}");
                        	$('#email_registration_email').val('');
                        //  $("#email_registration_result").html("<p style='color: #599482;font-size: 15px;font-weight: bold;text-align: center;'>{{lang::get('global.registration_success')}}</p>");
                        }
                    }});





            });

             
     
     

            function isValidEmailAddress(emailAddress) {
                var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
                return pattern.test(emailAddress);
            }

        </script>   

    </body>
</html>
