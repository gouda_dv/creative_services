@extends('website.master')
@section('content')
@if(isset($service))

<div class="container">
  <div class="row">
    <div class="col-md-12 content-box ">
      <h5>{{$service->title}}</h5>
      <div  class="collapsable" style="margin-top:16x;">
     
       <a class="fancybox-media" href="{{ URL::asset('uploads/service/'.$service->image) }}"> 
       
       <img  src="{{ URL::asset('uploads/service/'.$service->image) }}" class="img-thumbnail img-responsive marginl-img" align="right" width="209" height="325">
       </a>
       
        <div>{{$service->content}}</div> 
       
           </div>
    </div>
  </div>
</div>
@endif
@stop
