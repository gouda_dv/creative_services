@extends('website.master')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12 content-box ">
      <h5><?php echo Lang::get('global.contact_us'); ?></h5>
    </div>
    <div class="col-md-8 content-box ">
      <div id="contact" class="section">
        <div class="container-contact">
          <div class="desc">
<!--            <p class="desc">{{Lang::get('global.contact_us_hint')}} </p>-->
          </div>
          <div id="contact-form">
            {{ Form::open(array('role'=>'form','route'=>'contact_us.store','files' => true,'id'=>'contact-us')) }}  
            
              <div class="formblock">
                <label class="screen-reader-text"><?php echo Lang::get('global.full_name'); ?></label>
                <input type="text" name="name" id="name" value="" class="txt requiredField"  />
              </div>
              <div class="formblock">
                <label class="screen-reader-text"><?php echo Lang::get('global.emails'); ?></label>
                <input type="text" name="email" id="email" value="" class="txt requiredField email"  />
              </div>
             <div class="formblock">
                <label class="screen-reader-text"><?php echo Lang::get('global.mobiles'); ?></label>
                <input type="text" name="mobile" id="mobile" value="" class="txt mobile optional"  />
              </div>
             <div class="formblock">
                <label class="screen-reader-text">  <?php echo Lang::get('global.msg_title'); ?></label>
                <input type="text" name="title" id="title" value="" class="txt title requiredField"  />
              </div>
            
          
              <div class="formblock">
                <label class="screen-reader-text"><?php echo Lang::get('global.content'); ?></label>
                <textarea name="content" id="content" class="txtarea content requiredField" ></textarea>
              </div>
              <div class="submit">
                <input type="submit" name="send" id="send" value="<?php echo Lang::get('global.send'); ?>" class="send" />
              </div>
           {{Form::close()}}
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 content-box "> <img  src="{{ URL::asset('website_design/images/Bird-Mail-Email-App.png') }}"  class="center-block img-responsive" style="margin-top:30px;"> </div>
  </div>
</div>


<script type="text/javascript">
                $(document).ready(function() {
                    $('form#contact-us').submit(function() {
                        $('form#contact-us .error').remove();
                        var hasError = false;
                        $('.requiredField').each(function() {
                            if($.trim($(this).val()) == '') {
                                var labelText = $(this).prev('label').text();
                                $(this).parent().append('<span class="error">{{lang::get('global.please_enter')}}'+labelText+'.</span>');
                                $(this).addClass('inputError');
                                hasError = true;
                            } else if($(this).hasClass('email')) {
                                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                                if(!emailReg.test($.trim($(this).val()))) {
                                    var labelText = $(this).prev('label').text();
                                    $(this).parent().append('<span class="error">{{lang::get('global.validate_email')}}'+labelText+'.</span>');
                                    $(this).addClass('inputError');
                                    hasError = true;
                                }
                            }
                      
                               else if($(this).hasClass('title')) {
                                var titleReg = /^[A-Za-z\s]+$/;
                                if(!titleReg.test($.trim($(this).val()))) {
                                    var labelText = $(this).prev('label').text();
                                    $(this).parent().append('<span class="error">{{lang::get('global.validate_title')}}'+labelText+'.</span>');
                                    $(this).addClass('inputError');
                                    hasError = true;
                                }
                            }
                            else if($(this).hasClass('content')) {
                                var contentReg = /^[A-Za-z\s]+$/;
                                if(!contentReg.test($.trim($(this).val()))) {
                                    var labelText = $(this).prev('label').text();
                                    $(this).parent().append('<span class="error">{{lang::get('global.validate_content')}}'+labelText+'.</span>');
                                    $(this).addClass('inputError');
                                    hasError = true;
                                }
                            }
                        })
                        $('.optional').each(function() {
                        
                         if($(this).hasClass('mobile')) {
                             if($.trim($(this).val()) != ''){
                            
                                var mobReg = /^(009665|9665|\+9665|05)(5|0|3|6|4|9|1)([0-9]{7})$/i;
                                if(!mobReg.test($.trim($(this).val()))) {
                                    var labelText = $(this).prev('label').text();
                                    $(this).parent().append('<span class="error">{{lang::get('global.validate_mobile')}}'+labelText+'.</span>');
                                    $(this).addClass('inputError');
                                    hasError = true;
                                }
                            }
                            }else{
                            hasError = false;
                            }
                            
                            });
                        
                        
                      
                        if(!hasError) {
                            var formInput = $(this).serialize();
                            $.post($(this).attr('action'),formInput, function(data){
                                $('form#contact-us').slideUp("fast", function() {				   
                                    $(this).before('<p class="tick"> {{lang::get('global.contact_us_message')}}</p>');
                                });
                            });
                        }
                        
                        return false;	
                    });
                });
            </script>


<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>   -->
<!--<script> 
    $('.send').on('click',function{
      
      $('#mobile').focusout(function(){
                 var phone = $('#mobile').val();               
                var length = phone.length;
if(length>0){
    var regex = /^(009665|9665|\+9665|05)(5|0|3|6|4|9|1)([0-9]{7})$/i;
                if (regex.test(phone)) {
                    return true;
                } else {
                    alert("{{lang::get('global.format_phone')}}");
                    return false;
                } 
}else{
    alert("{{lang::get('global.format_phone')}}");
            return false;  
}}

$('#name').focusout(function(){
                 var name = $('#name').val();               
                var name_length = name.length;
if(name_length>0){
    var regex = /^([a-zA-Z ]+)$/;
                if (regex.test(name)) {
                    return true;
                } else {
                    alert("{{lang::get('global.validate_name')}}");
                    return false;
                } 
}else{
   alert("{{lang::get('global.validate_name')}}");
            return false;  
}}

$('#title').focusout(function(){
                 var title = $('#title').val();               
                var title_length = title.length;
if(title_length>0){
    var regex = /^([a-zA-Z ]+)$/;
                if (regex.test(title)) {
                    return true;
                } else {
                    alert("{{lang::get('global.validate_title')}}");
                    return false;
                } 
}else{
   alert("{{lang::get('global.validate_title')}}");
            return false;  
}}

$('#content').focusout(function(){
                 var content = $('#content').val();               
                var content_length = content.length;
if(content_length>0){
    var regex = /^([a-zA-Z ]+)$/;
                if (regex.test(content)) {
                    return true;
                } else {
                    alert("{{lang::get('global.validate_content')}}");
                    return false;
                } 
}else{
   alert("{{lang::get('global.validate_content')}}");
            return false;  
}}

$('#email').focusout(function(){
                 var email = $('#email').val();               
                var email_length = phone.length;
if(email_length>0){
    var regex = /^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i;
                if (regex.test(email)) {
                    return true;
                } else {
                    alert("{{lang::get('global.validate_email')}}");
                    return false;
                } 
}else{
   alert("{{lang::get('global.validate_email')}}");
            return false;  
}}
      
        
    });
  
 
</script>-->

@stop

