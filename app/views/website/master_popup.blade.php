
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>{{Lang::get('global.website_title')}}</title>
        <link href="{{ URL::asset('website_design/css/bootstrap-arabic.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('website_design/css/bootstrap-arabic-theme.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('website_design/css/css.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('website_design/css/font-awesome.css') }}" rel="stylesheet" type="text/css">

        <!--  slider-->
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="{{ URL::asset('website_design/js/skdslider.js') }}"></script>
        <link href="{{ URL::asset('website_design/css/skdslider.css') }}" rel="stylesheet">
        
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
        
       
        <style type="text/css">
            .demo-code {
                background-color: #ffffff;
                border: 1px solid #1476b4;
                display: block;
                padding: 10px;
            }
        </style>
        <script src="{{ URL::asset('website_design/js/jquery.simpleWeather.js') }}" charset="utf-8"></script>


        <!--end-slider-->



        <!-- Add fancyBox main JS and CSS files -->
        <script type="text/javascript" src="{{ URL::asset('website_design/source/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('website_design/source/jquery.fancybox.css?v=2.1.5') }}" media="screen" />

        <!-- Add Button helper (this is optional) -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('website_design/source/helpers/jquery.fancybox-buttons.css?v=1.0.5') }}" />
        <script type="text/javascript" src="{{ URL::asset('website_design/source/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>

        <!-- Add Media helper (this is optional) -->
        <script type="text/javascript" src="{{ URL::asset('website_design/source/helpers/jquery.fancybox-media.js?v=1.0.6') }}"></script>


   
        
       <!--flashtext-->
      <script type="text/javascript" src="js/core.js"></script>
      <!--end-->
      <!--video-->
      <link href="{{ URL::asset('website_design/css/youtube-video-gallery.css') }}" rel="stylesheet" type="text/css">
      <script type="text/javascript" src="{{ URL::asset('website_design/js/jquery.youtubevideogallery.js') }}"></script>
      
      <link rel="stylesheet" href="http://fancyapps.com/fancybox/source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
      <script type="text/javascript" src="http://fancyapps.com/fancybox/source/jquery.fancybox.pack.js?v=2.1.4"></script>
      <script type="text/javascript" src="http://fancyapps.com/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
      <!--end  -->


            
    </head>
    <body>
        
        
          @yield('content')
        
         </body>
</html>
