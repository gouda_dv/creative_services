
@extends('website.master')
@section('content')
<div class="eventtittle">
      <div class="container">
    <div class="eventtittle2">
          <div class="row">
        <div class="col-md-12">
              <h4 class="pull-left"><span class="glyphicon glyphicon-film"></span>{{ Lang::get('global.Gallery')}}</h4>
            </div>
      </div>
        </div>
  </div>
    </div>
<div class="event">
      <div class="container">
    <div class="row">
          <div class="col-md-12">
        @foreach($galleries as $gallery)
        <article class="video" >
              <figure> <a class="fancybox-media" href="{{ URL::asset('gallery/'.$gallery->id) }}"> <img class="videoThumb" src="{{ URL::asset('uploads/gallery/'.$gallery->image) }}" style="height: 150px;" ></a> </figure>
         <br/>
         <h2 class="videoTitle">{{$gallery->title}}</h2>
         </article>
         @endforeach   
        
        <div style="clear:both"></div>

      </div>
      
        </div>
  </div>
</div>

<div style="height: 50px;"></div>


@stop