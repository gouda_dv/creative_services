
@extends('website.master')
@section('content')
<div class="eventtittle">
      <div class="container">
    <div class="eventtittle2">
          <div class="row">
        <div class="col-md-12">
              <h4 class="pull-left"><span class="glyphicon glyphicon-film"></span>{{$gallery->title}}</h4>
            </div>
      </div>
        </div>
  </div>
    </div>
<div class="event">
      <div class="container">
    <div class="row">
          <div class="col-md-12">
        @foreach($images as $image)
        <article class="video" >
              <figure> <a class="fancybox-media" href="{{ URL::asset('uploads/gallery/'.$gallery->id.'/'.$image) }}"> <img class="videoThumb" src="{{ URL::asset('uploads/gallery/'.$gallery->id.'/'.$image) }}" style="height: 150px;" ></a> </figure>
            </article>
         @endforeach   
        
        <div style="clear:both"></div>

      </div>
      
        </div>
  </div>
</div>

<div style="height: 50px;"></div>


@stop