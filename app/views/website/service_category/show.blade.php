@extends('website.master')
@section('content')
@if(isset($service_category))

<div class="container">
  <div class="row">
    <div class="col-md-12 content-box ">
      <h5>{{$service_category->title}} </h5>
      <div  style="margin-top:16x;"> <img src="{{ URL::asset('uploads/service_category/'.$service_category->image) }}" class=" marginl-img margint-img img-responsive img-thumbnail" align="right"  >
        <p> {{$service_category->content}} </p>
      </div>
    </div>
    <div class="col-md-12 content-box">
      <main class="main-wrapper">
        <div id="second">
            @if(isset($service))
            <?php foreach($service as $services){?>
          <h3> <img src="{{ URL::asset('website_design/images/logo-icon.png') }}" width="35" height="15">{{$services->title}}</h3>
          <div>
            <p  class="main_pragraph"> 
          <img src="{{ URL::asset('uploads/service_category/'.$services->image) }}" width="325" height="209" align="right" class="marginl-img img-rounded img-responsive"> {{$services->summery}}
        
            
           <span class="readmoreblue">
            <a href="{{URL::to('service_category/'.$services->id)}}">{{Lang::get('global.read_more')}}</a>
            </span>
            </p>
                     
          </div>
            <?php }?>
          @endif

        @if(isset($internal_service))
            <?php foreach($internal_service as $internal_services){?>
          <h3> <img src="{{ URL::asset('website_design/images/logo-icon.png') }}" width="35" height="15">{{$internal_services->title}}</h3>
          <div>
            <p class="main_pragraph"> 
                <img src="{{ URL::asset('uploads/service/'.$internal_services->image) }}" width="325" height="209" align="right" class="marginl-img img-rounded img-responsive"> {{$internal_services->summery}}               
           <span class="readmoreblue">
            <a href="{{URL::to('service/'.$internal_services->id)}}">{{Lang::get('global.read_more')}}</a>
            </span>
             </p>   
          </div>
            <?php }?>
          @endif

        </div>
      </main>
    </div>
  </div>
</div>
@endif
@stop
