
@extends('website.master')
@section('content')

@if(isset($place))

<div class="eventtittle">
      <div class="container">
    <div class="eventtittle2">
          <div class="row">
        <div class="col-md-12">
              <h4 class="pull-left"><span class="glyphicon glyphicon-flag"></span>{{ Lang::get('global.madinah_places')}}</h4>
            </div>
      </div>
        </div>
  </div>
    </div>
<div class="event">
      <div class="container">
    <div class="row">
          <div class="col-md-12">
        <div class="artical">
              <div class="t-artical pull-left"><span class="glyphicon glyphicon-chevron-left"></span>{{$place->title}}
            <div class="resize"> <a href="#" class="fontSizePlus">A+</a> | <a href="#" class="fontReset">{{ Lang::get('global.default_font_size')}}</a> | <a href="#" class="fontSizeMinus">A-</a></div>
          </div>
              <p class="c-artical intro">{{$place->summery}} <br/>
          </p>
              <div class="clearfix"></div>
              <div class="col-md-6">
            <div class="google-maps">
                  <iframe height="350"  frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=%D8%A7%D9%84%D9%85%D8%AF%D9%8A%D9%86%D8%A9+%D8%A7%D9%84%D9%85%D9%86%D9%88%D8%B1%D8%A9&amp;aq=&amp;sll={{$place->map_longitude}},-122.01843&amp;sspn=0.007321,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=Medina,+Al+Madinah+Province+Saudi+Arabia&amp;t=m&amp;z=16&amp;ll={{$place->map_Latitude}},39.6&amp;output=embed"></iframe>
                </div>
          </div>
              <div class="col-md-6">
            @if($place->youtube_link!='')
                        <div class="google-maps">
                            <iframe class="embed-responsive-item" src="{{$place->youtube_link}}"> </iframe>
                        </div>
                        @elseif($place->video!='') 
                        <div class="google-maps">
                            <video width="100%" height="350px" controls>
                                <source src="{{ URL::asset('uploads/place/'.$place->video) }}" type="video/mp4">
                                Your browser does not support HTML5 video.
                            </video>
                        </div>
                        @endif
          </div>
          <div class="col-lg-6 pull-right" style="margin-top:25px;">
            <div style="float:left; margin-right:30px; padding-top: 10px;"> <img src="{{ URL::asset('website_design/images/download.png') }}" alt="{{ Lang::get('global.subscribe_with_us')}}" height="25" width="25"></div>
            <div style="float:left; margin-right:30px; padding-top: 10px;" class="print" ><img src="{{ URL::asset('website_design/images/print_icon-s.png') }}" height="25" width="25"></div>
            <div style="float:left; margin-right:30px; padding-top: 10px;" class="email"> <img src="{{ URL::asset('website_design/images/envolp.png') }}"  height="25" width="25"></div>
                   
          </div>
              <div class="col-lg-6 pull-left" style="margin-top:25px;">
            <div style="height:29px; float:right;">
                  <h1>{{ Lang::get('global.subscribe_with_us')}}</h1>
                </div>
            <div class="social" id="css3" style="">
                  <li class="facebook"> <a target="_blank" href="http://www.facebook.com/sharer.php?u={{ URL::to('madinah_place/'.$place->id) }}&t={{urlencode($place->title)}}"></a></li>
                            <li class="linkedin"> <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{ URL::to('madinah_place/'.$place->id) }}&title={{urlencode($place->title)}}&summary={{urlencode($place->summery)}}&source={{ Lang::get('global.website_title')}}"></a></li>
                            <li class="google">   <a target="_blank" href="https://plus.google.com/share?url={{ URL::to('madinah_place/'.$place->id) }}"></a></li>
                            <li class="twitter">  <a target="_blank" href="http://twitter.com/home?status={{ URL::to('madinah_place/'.$place->id) }}"></a></li>
                            <!--  
                            <li class="instgram"><a target="_blank" href="http://www.facebook.com/"></a></li>
                            -->
                </div>
          </div>
            </div>
      </div>

	<div class="col-md-12 pull-left demo-1" style="margin-top:20px; margin-bottom:25px;">
          
        <div class="img-responsive img albuem"> {{ Lang::get('global.photo_album')}} </div>
        <div class="clearfix"></div>
        
          
          
        <ul id="carousel" class="elastislide-list ">
        
        		@foreach($place_galleries as $place_gallery)
                   <li>
		            <figure><a class="fancybox-media" href="{{ URL::asset('uploads/gallery/'.$place->gallery_id.'/'.$place_gallery) }}"> <img class="videoThumb" src="{{ URL::asset('uploads/gallery/'.$place->gallery_id.'/'.$place_gallery) }}"></a></figure>
		          </li>
                 @endforeach

            </ul>
      </div>
        </div>
  </div>
    </div>


@endif
<script>

    $('.print').on('click', function() {
        // window.open("http://www.w3schools.com");
        window.open("{{ URL::to('madinah_place/open_uploads_popup') }}/{{$place->id}}", '1412233895408', 'width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
    });

    $('.email').on('click', function() {
        window.open("{{ URL::to('madinah_place/email') }}?pop_up_id={{$place->id}}", '1412233895408', 'width=900,height=700,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=320,top=50%,center=0');
    });



</script>

@if(isset($flag) && $flag==1)
{{lang::get('global.send_doc_email')}}
@endif
@stop