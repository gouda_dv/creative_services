@extends('website.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12 content-box ">
      <h5>{{ Lang::get('global.branches')}}</h5>
     
       <div id="map_canvas"  class="collapsable"  style="width:100%; height: 500px; margin-top:16x;"></div>
           
          <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
		  <script src="{{ URL::asset('js/locationpicker/locationpicker.jquery.js') }}"></script>
          
          <script>
          initialize();
          function initialize() 
          {
              var latlng = new google.maps.LatLng(24.4211245720623, 39.6171875);

              var myOptions = {
                  zoom: 11,
                  center: latlng,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              };

              var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

              var locations = [
				@foreach($all_branches as $all_branch)             
                  ['{{$all_branch->title}}', {{$all_branch->map_Latitude}}, {{$all_branch->map_longitude}}, '{{ preg_replace("/[\r\n]*/", "", $all_branch->summery)}}', '{{URL::to("branch/".$all_branch->id)}}'],
                @endforeach
              ];

              
    		  
              for (var i = 0; i < locations.length; i++) {


            	  var contentString = '<div class="col-md-3 center-block space-h" style="width:290px; padding:10px;">'+
        		  '<h5>'+locations[i][0]+'</h5> <br/>'+
        		  '<div >'+locations[i][3]+' <br/>'+
        		  '</div>'+
        		  '</div>';
        		  
                  var marker = new google.maps.Marker({
                      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                      map: map,
                      buborek: contentString,
                  });

						  		  
                  var infowindow = new google.maps.InfoWindow();
                  
                  google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
                    return function() {
                    	infowindow.setContent(this.buborek); 
                        infowindow.open(map,this); 
                    }
                  })(marker, i));
              }
          }
		</script>
     
     	</div>
           
  </div>
</div>
@stop