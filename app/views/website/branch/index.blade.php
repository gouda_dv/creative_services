
@extends('website.master')
@section('content')

<div class="container">
  <div class="row">
      
      @if(isset($all_branches))
    <?php  foreach($all_branches as $branches){?>
      
      <div class="col-md-6 content-box border-b">
      <h5>{{$branches->title}}</h5>
      <div class="col-md-6 content-box"> {{$branches->summery}}</div>
      <div class="col-md-6 content-box">
        <div class="google-maps">
          <iframe height="100%"   width="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
            src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=%D8%A7%D9%84%D9%85%D8%AF%D9%8A%D9%86%D8%A9+%D8%A7%D9%84%D9%85%D9%86%D9%88%D8%B1%D8%A9&amp;aq=&amp;sll={{$branches->map_longitude}},-122.01843&amp;sspn=0.007321,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=Medina,+Al+Madinah+Province+Saudi+Arabia&amp;t=m&amp;z=11&amp;ll={{$branches->map_Latitude}},39.6&amp;output=embed"></iframe>
        </div>
      </div>
    </div>
      <?php }?>
      @endif
   
    
  </div>
</div>

@stop