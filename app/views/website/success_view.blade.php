@extends('website.master')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12 content-box ">
      <h5>{{ $params['title']}}</h5>
    </div>
    <div class="col-md-8 content-box ">
      <div id="contact" class="section">
        <div class="container-contact">
            <div class="col-md-8 " style="color: #026bb6;margin-left: 70px;margin-top: 75px">{{  $params['message']}}</div>
          
        </div>
      </div>
    </div>
    <div class="col-md-4 content-box "> <img  src="{{ URL::asset('website_design/images/Bird-Mail-Email-App.png') }}"  class="center-block img-responsive" style="margin-top:30px;"> </div>
  </div>
</div>

@stop

