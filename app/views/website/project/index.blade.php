
@extends('website.master')
@section('content')
@if(isset($all_projects))
<div class="container">
  <div class="row">
    <div class="col-md-12 content-box ">
      <h5>{{Lang::get('global.our_projects')}}</h5>
     
    </div>
    <div class="col-md-12 content-box">
        
        <?php foreach($all_projects as $projects){?>
         <div class="c-new">
       <div class="bg-h3"> <!-- <img src="{{ URL::asset('website_design/images/new-icon.png')}} " width="17" height="20"> -->{{$projects->title;}} </div>
        <div class="collapsable">
            <p> <span class="date-news"> <img src="{{ URL::asset('website_design/images/calendar-copy-icon-clock.png')}}" width="20" height="25">{{$projects->date}}</span>  </p>
              <img src="{{ URL::asset('uploads/project/'.$projects->image)}}" width="325" height="209" align="right" class="marginl-img img-rounded img-responsive"> </p>
          <div id="dv_summery">{{$projects->summery}}</div>
          <div id="dv_content">{{$projects->content}}</div>
        </div>
      </div>
        <?php }?>
     
    
    </div>
  </div>
</div>
@endif
@stop