@extends('website.master')
@section('content')
@if(isset($project))

<div class="container">
  <div class="row">
    <div class="col-md-12 content-box ">
      <h5>{{$project->title}}</h5>
      <div  class="collapsable" style="margin-top:16x;">
     
       <a class="fancybox-media" href="{{ URL::asset('uploads/project/'.$project->image) }}"> 
       
       <img  src="{{ URL::asset('uploads/project/'.$project->image) }}" class="img-thumbnail img-responsive marginl-img" align="right" width="209" height="325">
       </a>
       
        <div>{{$project->content}}</div> 
       
           </div>
    </div>
  </div>
</div>
@endif
@stop
