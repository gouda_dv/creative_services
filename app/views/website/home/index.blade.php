@extends('website.master')
@section('content')

<div class="bg-slider">
  <div class="container">
    <div class=" col-md-12">
      <div id="ei-slider" class="ei-slider">
          @if(isset($sliders))
         
               
        <ul class="ei-slider-large">
            <?php foreach($sliders as $slider) {?>
          <li> <img src="{{URL::asset('uploads/header_slider/'.$slider->image)}}" alt="image01" />
            <div class="ei-title">
              <h2>{{$slider->title}}</h2>
              <h3>{{$slider->summery}}</h3>
            </div>
          </li>
           <?php }?> 
        </ul>
        <ul class="ei-slider-thumbs">
                      <li class="ei-slider-element">Current</li>
           <?php foreach($sliders as $slider) {?>

          <li ><a href="#">Slide 1</a><img src="{{URL::asset('uploads/header_slider/'.$slider->image)}}" alt="thumb01" /></li>
        <?php }?> 
        </ul>
           @endif  
       
      </div>
      <img src="{{URL::asset('website_design/images/dropshadow.png')}}" width="1035" height="26" class="img-responsive center-block"> </div>
  </div>
</div>
<!--end-slider-->
<div class="space">
  <div class="container">
    <div class="row">
      <div class="col-md-1"> <img src="{{URL::asset('website_design/images/services.png')}}" width="53" height="255" class="img-responsive  center-block"></div>
     @if(isset($services_category))
      <?php foreach($services_category as $category){?>
      <div class="col-md-3 center-block space-h">
        <div  class="bg-serv">
          <ul class="ch-grid">
            <li>
              <div class="ch-item ch-img-1">
                <div class="ch-info-wrap">
                  <div class="ch-info">
                    <div class="ch-info-front ch-img-1"></div>
                    <div class="ch-info-back">
                      <p><a href="{{URL::to('service_category/'.$category->id)}}">{{Lang::get('global.read_more')}}</a></p>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
          </ul>
        </div>
        <div class="t-serv">{{$category->title}}</div>
        <p> {{$category->summery}} </p>
        <div class="serv-link"> <a href="{{URL::to('service_category/'.$category->id)}}"> <?php echo Lang::get('global.read_more'); ?> <span class="glyphicon glyphicon-expand"></span></a> </div>
      </div>
      <?php }?>
     @endif
     
  
    </div>
  </div>
</div>

<!--end-services-->

<div class="space">
  <div class="container">
    <div class="row">
        
        
        
        
      <div class="col-md-4  pull-right ">
     @if(isset($welcome))
   <?php foreach($welcome as $welcome_text) {?>
     <div class="c-color"> <span class="h6">{{Lang::get('global.welcome')}} &nbsp;</span> <span class="h5">{{$welcome_text->title}}</span> </div>
        
        <div class="pull-left" style="margin-top:16px;"> <img src="{{URL::asset('uploads/nav_link/'.$welcome_text->image)}}" width="325" height="209" class="img-responsive img-thumbnail center-block"  >
          <p> {{$welcome_text->summery}}</p>
          <div class="readmoreblue" > <a href="{{URL::to($welcome_text->link)}}">{{Lang::get('global.read_more')}}</a></div>
        </div>
        <?php }?>
@endif
      </div>
          @if(isset($project))
      <div class="col-md-7 c-lastproject">
        <h5>Last Projects</h5>
        <div class="clearfix"></div>
        <div id="w">
          <div class="content-box">
            <div class="crsl-items" data-navigation="navbtns">
              <div class="crsl-wrap">
                @if(isset($project))
                
                 <?php foreach($project as $projects){?>
                <div class="crsl-item">
                  <div class="thumbnail"> <img src="{{URL::asset('uploads/project/'.$projects->image)}}"> <span class="postdate">{{$projects->date}}</span> </div>
                  <h3><a href="#">{{$projects->title}}</a></h3>
                  <p>{{$projects->summery}}</p>
                  <p class="readmore"><a href="{{URL::to('project/'.$projects->id)}}"> {{Lang::get('global.read_more')}}&raquo;</a></p>
                </div>
                  <?php }?>
                
                
               @endif
              </div>
            </div>
            <nav class="slidernav center-block">
              <div id="navbtns" class="clearfix"> <a href="#" class="previous">{{Lang::get('global.prev')}}</a> <a href="#" class="next">{{Lang::get('global.next')}}</a> </div>
            </nav>
          </div>
        </div>
      </div>
      </div>
          @endif
      <div class="col-md-12 ">
        <div class="partners">{{Lang::get('global.our_partners')}}</div>
           <div class="bg-partners ">
        <div style="text-align:center">
    <div class="c">     
    <div class=" itms">
        @if(isset($partner))
        <?php foreach($partner as $partners){?>
        <img src="{{URL::asset('uploads/partner/'.$partners->image)}}" width="131" height="119"> 
       
        <?php }?>
        @endif
    </div>
      </div>  
        
        </div>

      
     </div> 
      
      
        
        
        
     </div>
    </div>
  </div>
</div>
@stop