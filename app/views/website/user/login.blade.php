
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1 class="text-center">تسجيل دخول</h1>
            </div>
            <div class="modal-body">
                {{ Form::open(array('role'=>'form','url'=>'user/validate' ,'method'=>'post')) }}

                <div class="form-group ">
                    {{ Form::label('name',Lang::get('global.name')) }}
                    {{ Form::text('name',"",array('class'=>'form-control','placeholder'=>'Type Name','autocomplete'=>'off')) }} 
                </div>

                <div class="form-group ">
                    {{ Form::label('password', Lang::get('global.password')) }}
                    {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Type Your Password','autocomplete'=>'off')) }} 
                </div>

                {{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
