
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

           

 <div class="modal-body">
{{ Form::open(array('role'=>'form','route'=>'user.store','files' => true)) }}

<div class="form-group ">
    <?php echo Lang::get('global.name'); ?>
    {{ Form::text('name',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.emails'); ?>
    {{ Form::text('email',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.password'); ?>
    {{ Form::password('password',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
  <?php echo Lang::get('global.mobiles'); ?>
    {{ Form::text('mobile',"",array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
  <?php echo Lang::get('global.phones'); ?>
    {{ Form::text('phone',"",array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
  <?php echo Lang::get('global.address'); ?>
    {{ Form::text('address',"",array('class'=>'form-control')) }} 
</div>

{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}

 </div>

