@extends('website.master')
@section('content')

@if(isset($user))
<div class="modal-content">
    <div class="modal-header">


        <div  id="send_res">   </div>  
    </div>
    <div class="modal-body">

        {{ Form::open(array('method'=>'put','class' => 'form-bootstrap', 'role'=>'form','url'=>'user/'. $user->id)) }}

        <div class="eventtittle">
            <div class="container">
                <div class="eventtittle2">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="pull-left"><span class="

                                                        glyphicon glyphicon-envelope"></span> <?php echo Lang::get('global.profile'); ?></h4>

                            <div  id="send_res">   </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="print">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <form class="myfirstform" role="form" id="profile_form">
                            <div class="form-group">

                                <label class="control-label"> <?php echo Lang::get('global.name'); ?></label>
                                <div class="controls">
                                    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input type="text" class="form-control" name="name" placeholder="<?php echo Lang::get('global.name'); ?>" value="<?php echo $user->name; ?>" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo Lang::get('global.emails'); ?></label>

                                <div class="controls">
                                    <div class="input-group"  > <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <label class="form-control" style="background-color: #e0e0e0"> <?php echo $user->email; ?></label>
                                       <!-- <input type="email" class="form-control" id="email" name="email" placeholder="<?php //echo Lang::get('global.emails');   ?>" 
                                               value="<?php //echo $user->email;  ?>" style="background-color: #e0e0e0" disabled="disabled">  -->
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label"><?php echo Lang::get('global.password'); ?></label>
                                <div class="controls">
                                    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="<?php echo Lang::get('global.password'); ?>" value="<?php echo $user->password; ?>" required="required">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label"><?php echo Lang::get('global.phones'); ?></label>
                                <div class="controls">
                                    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="<?php echo Lang::get('global.phones'); ?>" value="<?php echo $user->phone; ?>" required="required">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label"><?php echo Lang::get('global.mobile'); ?></label>
                                <div class="controls">
                                    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                        <input type="text" class="form-control" id="mobile" name="mobile" placeholder="<?php echo Lang::get('global.mobile'); ?>" value="<?php echo $user->mobile; ?>" required="required">

                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label"><?php echo Lang::get('global.address'); ?></label>
                                <div class="controls">
                                    <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="<?php echo Lang::get('global.address'); ?>" value="<?php echo $user->address; ?>" required="required">

                                    </div>
                                </div>
                            </div>

                            <div class="controls" style="margin-right: 40%;">
                                {{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success",'onclick'=>"return validate();")) }}
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        @endif
        <script>
            function validate()
            {
                var phone = $('#mobile').val();
               // var length = phone.length;

                // alert(length);
                var regex = /^(009665|9665|\+9665|05)(5|0|3|6|4|9|1)([0-9]{7})$/i;
                if (regex.test(phone)) {
                   return true;
                } else {
                    alert("{{lang::get('global.format_phone')}}");
                    return false;
                }


            }
            
            
          
        </script>

        @stop

