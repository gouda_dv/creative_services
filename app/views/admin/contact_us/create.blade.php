@extends('master')
@section('content')
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif
@if(isset($contact_us))
{{ Form::open(array('method'=>'put','class' => 'form-bootstrap', 'role'=>'form','url'=>'administrator/contact_us/'. $contact_us->id)) }}
<div class="form-group ">

     {{ Form::hidden('id',$contact_us->id) }} 
</div>
<div class="form-group ">
   <?php echo Lang::get('global.full_name'); ?>
    {{ Form::text('name',$contact_us->name,array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
     <?php echo Lang::get('global.emails'); ?>
    {{ Form::text('email',$contact_us->email,array('class'=>'form-control',"data-validation"=>"email")) }} 
</div>
<div class="form-group ">
     <?php echo Lang::get('global.mobiles'); ?>
    {{ Form::text('mobile',$contact_us->mobile,array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
     <?php echo Lang::get('global.msg_title'); ?>
    {{ Form::text('title',$contact_us->title,array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
     <?php echo Lang::get('global.message'); ?>
   {{ Form::textarea('content',$contact_us->content,array('class'=>'ckeditor',"data-validation"=>"required")) }} 
   
</div>

{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}

@else

{{ Form::open(array('role'=>'form','route'=>'administrator.contact_us.store')) }}


<div class="form-group ">
   <?php echo Lang::get('global.full_name'); ?>
    {{ Form::text('name',"",array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
  <?php echo Lang::get('global.emails'); ?>
    {{ Form::text('email',"",array('class'=>'form-control',"data-validation"=>"required email")) }} 
</div>
<div class="form-group ">
  <?php echo Lang::get('global.mobiles'); ?>
    {{ Form::text('mobile',"",array('class'=>'form-control','id'=>'mobile',"data-validation"=>"custom" ,"data-validation-regex"=>"/^(009665|9665|\+9665|05)(5|0|3|6|4|9|1)([0-9]{7})$/i")) }} 
</div>
<div class="form-group ">
  <?php echo Lang::get('global.msg_title'); ?>
    {{ Form::text('title',"",array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
     <?php echo Lang::get('global.message'); ?>
   {{ Form::textarea('content',"",array('class'=>'ckeditor',"data-validation"=>"required")) }} 
</div>

{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif
<script>  CKEDITOR.replace('content',
            {
                filebrowserBrowseUrl: "{{ URL::asset('ckfinder/ckfinder.html') }}",
                filebrowserImageBrowseUrl:"{{ URL::asset('ckfinder/ckfinder.html') }}?type=Images",
                filebrowserUploadUrl:
                        "{{ URL::asset('ckfinder') }}/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/",
                filebrowserImageUploadUrl:
                       "{{ URL::asset('ckfinder')}}/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/"
            });
</script>
<script> 
    $.validate({
  modules : 'file'
}); 

</script>
 
@stop

