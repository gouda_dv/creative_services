@extends('master')
@section('content')
<a class="btn btn-info " href="{{ URL::to('administrator/contact_us/create') }}"><?php echo Lang::get('global.add'); ?> </a>
<table class="table" style="margin-top:100px">
    <thead>
        <tr>
            <th style="text-align:center"><?php echo Lang::get('global.full_name'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.emails'); ?></th>
             <th style="text-align:center"><?php echo Lang::get('global.mobiles'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.msg_title'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.message'); ?></th>
            <th style="text-align:center" colspan="2"><?php echo Lang::get('global.actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        @foreach($contact_us as $contacts_us)
        <tr>
             <td style="text-align:center">{{$contacts_us->name}}</td>
              <td style="text-align:center">{{$contacts_us->email}}</td>
               <td style="text-align:center">{{$contacts_us->mobile}}</td>
               <td style="text-align:center">{{$contacts_us->title}}</td>
                <td style="text-align:center">{{$contacts_us->content}}</td>
              

            <td style="text-align:center"><a href="contact_us/{{$contacts_us->id}}/edit" class="btn btn-warning"> {{ lang::get('global.edit') }} </a></td>
     
            <td style="text-align:center">
                {{ Form::open(array('url'=>'administrator/contact_us/'.$contacts_us->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
          
        </tr>
        @endforeach
    </tbody>
</table>

@stop