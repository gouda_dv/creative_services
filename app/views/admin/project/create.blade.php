@extends('master')
@section('content')
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif


@if(isset($project))

{{ Form::open(array('method'=>'put','class' => 'form-bootstrap', 'role'=>'form','url'=>'administrator/project/'. $project->id,'files' => true)) }}

<div class="form-group ">
   
    {{ Form::hidden('id',$project->id) }} 
</div>

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_keywords'); ?>
    {{ Form::text('seo_meta_keywords',$project->seo_meta_keywords,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
     <?php echo Lang::get('global.seo_meta_description'); ?>
    {{ Form::text('seo_meta_description',$project->seo_meta_description,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    <?php echo Lang::get('global.title'); ?>
    {{ Form::text('title',$project->title,array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.summery'); ?>
    {{ Form::textarea('summery',$project->summery,array('class'=>'ckeditor')) }} 
</div>
<div class="form-group ">
    <?php echo Lang::get('global.content'); ?>
    {{ Form::textarea('content',$project->content,array('class'=>'ckeditor')) }} 
</div>

<div class="form-group">
    <img src="{{ URL::asset('uploads/project/'.$project->image) }}" width="100" height="100" style="margin-bottom: 15px;"/>
    {{ Form::file('image',array("data-validation"=>"mime size", "data-validation-allowing"=>"jpg, png, gif" ,"data-validation-max-size"=>"8M")) }} 

</div>
<div class="form-group ">
   <?php echo Lang::get('global.date'); ?>
    {{ Form::text('date',date("m/d/20y", strtotime($project->date)),array('id'=>'datepicker','class'=>'datepicker')) }} 
</div>
{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}

@else








{{ Form::open(array('role'=>'form','route'=>'administrator.project.store','files' => true,'class'=>'validation')) }}

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_keywords'); ?>
    {{ Form::text('seo_meta_keywords',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_description'); ?>
    {{ Form::text('seo_meta_description',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.title'); ?>
    {{ Form::text('title',"",array('class'=>'form-control',"data-validation"=>"required" )) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.summery'); ?>
    {{ Form::textarea('summery',"",array('class'=>'ckeditor')) }} 
</div>
<div class="form-group ">
   <?php echo Lang::get('global.content'); ?>
   {{ Form::textarea('content',"",array('class'=>'ckeditor')) }} 

</div>

<div class="form-group">
    {{ Form::file('image',array("data-validation"=>"required mime size", "data-validation-allowing"=>"jpg, png, gif" ,"data-validation-max-size"=>"8M")) }} 

</div>
<div class="form-group ">
   <?php echo Lang::get('global.date'); ?>
    {{ Form::text('date',"",array('id'=>'datepicker','class'=>'datepicker')) }} 
</div>
{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif
<script>  CKEDITOR.replace('content',
            {
                filebrowserBrowseUrl: "{{ URL::asset('ckfinder/ckfinder.html') }}",
                filebrowserImageBrowseUrl:"{{ URL::asset('ckfinder/ckfinder.html') }}?type=Images",
                filebrowserUploadUrl:
                        "{{ URL::asset('ckfinder') }}/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/",
                filebrowserImageUploadUrl:
                       "{{ URL::asset('ckfinder')}}/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/"
            });
</script>


<script> $.validate({
  modules : 'file'
}); 
$('.datepicker').datepicker();

</script>
 
@stop