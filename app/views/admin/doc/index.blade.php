@extends('master')
@section('content')
<a href="doc/create" style="margin-top: 36px;" class="btn btn-info">{{ Lang::get('global.add')}}</a>
<table class="table" style="margin-top:50px">
    <thead>
        <tr>
            <th style="text-align:center">#</th>
            <th style="text-align:center">{{ Lang::get('global.title')}}</th>
            <th style="text-align:center">{{ Lang::get('global.category_id')}}</th>
            <th style="text-align:center">{{Lang::get('global.summery')}}</th>
            <th style="text-align:center">{{ Lang::get('global.image')}}</th>
            <th style="text-align:center">{{ Lang::get('global.file')}}</th>
            <th style="text-align:center" colspan="2"><?php echo Lang::get('global.options'); ?></th>
        </tr>
    </thead>
    <tbody>
        @foreach($all_doc as $doc)
        <tr>
            <td style="text-align:center">{{$doc->id}}</td>
            <td style="text-align:center">{{$doc->title}}</td>
            <td style="text-align:center">{{$doc->doc_category_id}}</td>
            <td style="text-align:center">{{$doc->summery}}</td>

            <td style="text-align:center"><img src="{{ URL::asset('uploads/doc/'.$doc->image) }}" width="100" height="100" /></td>
            <td style="text-align:center"><a href="{{ URL::asset('uploads/doc/'.$doc->file) }}" class="btn btn-primary" download>{{ Lang::get('global.download_file')}}</a></td>
            <td style="text-align:center"><a href="doc/{{$doc->id}}/edit" class="btn btn-warning">{{ Lang::get('global.edit')}}</a></td>

            <td style="text-align:center">
                {{ Form::open(array('url'=>'administrator/doc/'.$doc->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(Lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close()}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop