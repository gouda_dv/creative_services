@extends('master')
@section('content')

@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

@if(isset($doc))
{{ Form::open(array('role'=>'form','url'=>'administrator/doc/'. $doc->id,'files' => true,'method'=>'put')) }}
<div class="form-group ">
    {{ Form::hidden('id',$doc->id) }} 
</div>


<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title')) }}
    {{ Form::text('title',$doc->title,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    {{ Form::label('doc_category_id',Lang::get('global.category_id')) }}
    {{ Form::select('doc_category_id',$cats,$doc->doc_category_id,array('class'=>'form-control'))}}

</div>

<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords',$doc->seo_meta_keywords,array('class'=>'form-control')) }}
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description',$doc->seo_meta_description,array('class'=>'form-control')) }}
</div>

<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::text('summery',$doc->summery,array('class'=>'form-control')) }} 
</div>

<div class="form-group">
    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image') }} 
    <img src="{{ URL::asset('uploads/doc/'.$doc->image) }}" width="100" height="100"  />
</div>

<div class="form-group">
    {{ Form::label('file',Lang::get('global.file')) }}
    {{ Form::file('file',array('display'=>'inline')) }} 
    @if($doc->file)
    <a  href="{{ URL::asset('uploads/doc/'.$doc->file) }}" class="btn btn-primary">{{Lang::get('global.download_file')}}</a>
    @endif
</div>

<?php if($doc->file_paid == 1) { ?>
<div class="form-group">
    {{ Form::label('file_paid',Lang::get('global.file_paid')) }}
    {{ Form::checkbox('file_paid','1',true) }} 
</div>
<div class="form-group">
     <?php echo Lang::get('global.price'); ?>
    {{ Form::text('file_price',$doc->file_price,array('class'=>'form-control price')) }} 
     <?php echo Lang::get('global.coin'); ?>
    {{ Form::select('file_currency_id',$coin,$doc->file_currency_id,array('class'=>'form-control coin')) }} 
</div>
<?php }else {?>
<div class="form-group">
    {{ Form::label('file_paid',Lang::get('global.file_paid')) }}
    {{ Form::checkbox('file_paid','0') }} 
</div>
<div class="form-group">
     <?php echo Lang::get('global.price'); ?>
    {{ Form::text('file_price','',array('class'=>'form-control price','disabled'=>'disabled')) }} 
     <?php echo Lang::get('global.coin'); ?>
    {{ Form::select('file_currency_id',$coin,array('class'=>'form-control coin'),array('disabled')) }} 
</div>
<?php }?>

<div class="form-group ">
    {{ Form::label('content',Lang::get('global.content')) }}
    {{ Form::textarea('content',$doc->content,array('class'=>'ckeditor')) }} 


</div>
<div class="form-group ">
     <?php echo Lang::get('global.youtube_link'); ?>
    {{ Form::text('youtube_link',$doc->youtube_link,array('class'=>'form-control')) }} 
</div>
<?php if($doc->video_paid == 1) { ?>
<div class="form-group">
    {{ Form::label('file_paid',Lang::get('global.file_paid')) }}
    {{ Form::checkbox('video_paid','1',true) }} 
</div>
 <div class="form-group">
     <?php echo Lang::get('global.price'); ?>
    {{ Form::text('video_price',$doc->video_price,array('class'=>'form-control price')) }} 
     <?php echo Lang::get('global.coin'); ?>
    {{ Form::select('video_currency_id',$coin,$doc->video_currency_id,array('class'=>'form-control coin')) }} 
</div>
<?php }else {?>
<div class="form-group">
    {{ Form::label('file_paid',Lang::get('global.file_paid')) }}
    {{ Form::checkbox('video_paid','0') }} 
</div>
 <div class="form-group">
     <?php echo Lang::get('global.price'); ?>
    {{ Form::text('video_price','',array('class'=>'form-control price','disabled'=>'disabled')) }} 
     <?php echo Lang::get('global.coin'); ?>
    {{ Form::select('video_currency_id',$coin,array('class'=>'form-control coin'),array('disabled')) }} 
</div>
<?php }?>




{{ Form::submit(Lang::get('global.save'),array('class'=>"btn btn-success")) }}

{{ Form::close() }}

@else

{{ Form::open(array('role'=>'form','route'=>'administrator.doc.store','files' => true)) }}
<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title') ) }}
    {{ Form::text('title','',array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    {{ Form::label('doc_category_id',Lang::get('global.category_id')) }}
    {{ Form::select('doc_category_id',$cats,'',array('class'=>'form-control'))}}

</div>

<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords','',array('class'=>'form-control')) }}
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description','',array('class'=>'form-control')) }}
</div>

<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::text('summery','',array('class'=>'form-control')) }} 
</div>



<div class="form-group">
    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image') }} 
</div>

<div class="form-group">
    {{ Form::label('file',Lang::get('global.file')) }}
    {{ Form::file('file') }} 
</div>
<div class="form-group">
    {{ Form::label('file_paid',Lang::get('global.file_paid')) }}
    {{ Form::checkbox('file_paid','1',true) }} 
</div>
<div class="form-group">
     <?php echo Lang::get('global.price'); ?>
    {{ Form::text('file_price','',array('class'=>'form-control price')) }} 
     <?php echo Lang::get('global.coin'); ?>
    {{ Form::select('file_currency_id',$coin,array('class'=>'form-control coin')) }} 
</div>
<div class="form-group ">
    {{ Form::label('content',Lang::get('global.content')) }}
    {{ Form::textarea('content','',array('class'=>'ckeditor')) }} 

</div>


<div class="form-group ">
   <?php echo Lang::get('global.youtube_link'); ?>
    {{ Form::text('youtube_link',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group">
     <?php echo Lang::get('global.video'); ?>
    {{ Form::file('video') }} 

</div>

<div class="form-group">
    {{ Form::label('file_paid',Lang::get('global.file_paid')) }}
    {{ Form::checkbox('video_paid','1',true,array('id'=>'video_paid','class'=>'video_paid')) }}
    </div>
    <div class="form-group">
     <?php echo Lang::get('global.price'); ?>
    {{ Form::text('video_price','',array('class'=>'form-control price')) }} 
     <?php echo Lang::get('global.coin'); ?>
    {{ Form::select('video_currency_id',$coin,array('class'=>'form-control coin')) }} 
</div>



{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif
<script>  CKEDITOR.replace('content',
            {
                filebrowserBrowseUrl: "{{ URL::asset('ckfinder/ckfinder.html') }}",
                filebrowserImageBrowseUrl:"{{ URL::asset('ckfinder/ckfinder.html') }}?type=Images",
                filebrowserUploadUrl:
                        "{{ URL::asset('ckfinder') }}/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/",
                filebrowserImageUploadUrl:
                       "{{ URL::asset('ckfinder')}}/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/"
            });
</script>
@stop