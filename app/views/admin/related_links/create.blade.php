@extends('master')
@section('content')
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif
@if(isset($links))

{{ Form::open(array('method'=>'put','class' => 'form-bootstrap', 'role'=>'form','url'=>'administrator/related_links/'. $links->id,'files' => true)) }}
<div class="form-group ">

     {{ Form::hidden('id',$links->id) }} 
</div>
<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_keywords'); ?>
    {{ Form::text('seo_meta_keywords',$links->seo_meta_keywords,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
     <?php echo Lang::get('global.seo_meta_description'); ?>
    {{ Form::text('seo_meta_description',$links->seo_meta_description,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.title'); ?>
    {{ Form::text('title',$links->title,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
     <?php echo Lang::get('global.link'); ?>
    {{ Form::text('link',$links->link,array('class'=>'form-control')) }} 
</div>

<?php if($links->image !=""){
    ?>
<div class="form-group">
    <img src="{{ URL::asset('uploads/related_links/'.$links->image) }}" width="100" height="100" style="margin-bottom: 15px;"/>
    {{ Form::file('image') }} 

</div>
<?php } else {?>
 <img  width="100" height="100" style="margin-bottom: 15px;"/>
  {{ Form::file('image') }} 
<?php }?>
{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}


@else



{{ Form::open(array('role'=>'form','route'=>'administrator.related_links.store','files' => true)) }}

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_keywords'); ?>
    {{ Form::text('seo_meta_keywords',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.seo_meta_description'); ?>
    {{ Form::text('seo_meta_description',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.title'); ?>
    {{ Form::text('title',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
  <?php echo Lang::get('global.link'); ?>
    {{ Form::text('link',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group">
    {{ Form::file('image') }} 

</div>
{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif

@stop

