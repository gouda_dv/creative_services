@extends('master')
@section('content')
<a class="btn btn-info " href="{{ URL::to('administrator/related_links/create') }}"><?php echo Lang::get('global.add'); ?> </a>
<table class="table" style="margin-top:100px">
    <thead>
        <tr>
            <th style="text-align:center"><?php echo Lang::get('global.title'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.link'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.photo'); ?></th>
            <th style="text-align:center" colspan="2"><?php echo Lang::get('global.actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        @foreach($links as $link)
        <tr>
             <td style="text-align:center">{{$link->title}}</td>
              <td style="text-align:center">{{$link->link}}</td>
<?php if ($link->image !="") { ?>
            <td style="text-align:center"><img src="{{ URL::asset('uploads/related_links/'.$link->image) }}" width="100" height="100" /></td>
<?php } else {?>
              <td style="text-align:center"></td>
<?php }?>
            <td style="text-align:center"><a href="related_links/{{$link->id}}/edit" class="btn btn-warning"> {{ lang::get('global.edit') }} </a></td>
     
            <td style="text-align:center">
                {{ Form::open(array('url'=>'administrator/related_links/'.$link->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
          
        </tr>
        @endforeach
    </tbody>
</table>

@stop