@extends('master')
@section('content')

<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script src="{{ URL::asset('js/locationpicker/locationpicker.jquery.js') }}"></script>

@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

@if(isset($branch))
{{ Form::open(array('role'=>'form','url'=>'administrator/branch/'. $branch->id,'files' => true,'method'=>'put','id'=>'place_form','novalidate'=>'novalidate')) }}
<div class="form-group ">
    {{ Form::hidden('id',$branch->id) }} 
</div>

<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title')) }}
    {{ Form::text('title',$branch->title,array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords',$branch->seo_meta_keywords,array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description',$branch->seo_meta_description, ['class' => 'form-control']) }}
</div>
<div class="map-group " style="float: right;margin-bottom: 10px;margin-left: 130px;">
    {{ Form::label('map_longitude',Lang::get('global.longitude')) }}
    {{ Form::text('map_longitude', $branch->map_longitude, array('class' => '',"data-validation"=>"required")) }}
</div>

<div class="map-group "style="margin-bottom: 10px;">
    {{ Form::label('map_Latitude',Lang::get('global.latitude')) }}
    {{ Form::text('map_Latitude', $branch->map_Latitude,array('class' => '',"data-validation"=>"required")) }}
</div>

<div id="somecomponent" style="width: 500px; height: 400px;"></div>
<script>
$('#somecomponent').locationpicker({
    location: {latitude: <?php echo $branch->map_Latitude; ?>, longitude:<?php echo $branch->map_longitude; ?>},
    radius: 100,
    zoom: 11,
    inputBinding: {
        latitudeInput: $('#map_Latitude'),
        longitudeInput: $('#map_longitude'),
    },
    enableAutocomplete: true,
});
</script>				


<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::textarea('summery',$branch->summery,array('class'=>'ckeditor')) }} 
</div>

<!--<div class="form-group ">
    {{ Form::label('content',Lang::get('global.content')) }}
    {{ Form::textarea('content',$branch->content,array('class'=>'ckeditor')) }} 

</div>-->



{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}


@else

{{ Form::open(array('role'=>'form','route'=>'administrator.branch.store','files' => true,'id'=>'place_form')) }}

<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title')) }}
    {{ Form::text('title',"",array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords','',array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description','', ['class' => 'form-control']) }}
</div>
<div class="map-group " style="float: right;margin-bottom: 10px;margin-left: 130px;">
    {{ Form::label('map_longitude',Lang::get('global.longitude')) }}
    {{ Form::text('map_longitude', '', array('class' => '',"data-validation"=>"required")) }}
</div>

<div class="map-group " style="margin-bottom: 10px;">
    {{ Form::label('map_Latitude',Lang::get('global.latitude')) }}
    {{ Form::text('map_Latitude','', array('class' => '',"data-validation"=>"required")) }}
</div>
<div id="somecomponent" style="width: 500px; height: 400px;"></div>
<script>
$('#somecomponent').locationpicker({
    location: {latitude:24.4211245720623, longitude:39.6171875},
    radius: 100,
    zoom: 11,
    inputBinding: {
        latitudeInput: $('#map_Latitude'),
        longitudeInput: $('#map_longitude'),
    },
    enableAutocomplete: true,
});
</script>				
				
<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::textarea('summery','',array('class'=>'ckeditor')) }} 
</div>

<!--<div class="form-group ">
    {{ Form::label('content',Lang::get('global.content')) }}
    {{ Form::textarea('content','',array('class'=>'ckeditor')) }} 

</div>-->
{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif

<script>  CKEDITOR.replace('content',
            {
                filebrowserBrowseUrl: "{{ URL::asset('ckfinder/ckfinder.html') }}",
                filebrowserImageBrowseUrl:"{{ URL::asset('ckfinder/ckfinder.html') }}?type=Images",
                filebrowserUploadUrl:
                        "{{ URL::asset('ckfinder') }}/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/",
                filebrowserImageUploadUrl:
                       "{{ URL::asset('ckfinder')}}/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/"
            });
</script>
<script> 
    $.validate({
  modules : 'file'
}); </script>
@stop