@extends('master')
@section('content')
<a href="branch/create" style="margin-top: 36px;" class="btn btn-info">{{ Lang::get('global.add')}}</a>
<table class="table" style="margin-top:50px">
    <thead>
        <tr>
            <th style="text-align:center">#</th>
            <th style="text-align:center">{{ Lang::get('global.title')}}</th>
            <th style="text-align:center">{{ Lang::get('global.longitude')}}</th>
            <th style="text-align:center">{{ Lang::get('global.latitude')}}</th>
            <th style="text-align:center">{{ Lang::get('global.summery')}}</th>
<!--            <th style="text-align:center">{{ Lang::get('global.content')}}</th>-->
        
            <th style="text-align:center" colspan="2">{{ Lang::get('global.options')}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($all_branches as $branch)
        <tr>
            <td style="text-align:center">{{$branch->id}}</td>
            <td style="text-align:center">{{$branch->title}}</td>
            <td style="text-align:center">{{$branch->map_longitude}}</td>
            <td style="text-align:center">{{$branch->map_Latitude}}</td>
            <td style="text-align:center">{{$branch->summery}}</td>
<!--             <th style="text-align:center">{{$branch->content}}</th>-->
           
            
           
            <td style="text-align:center"><a href="branch/{{$branch->id}}/edit" class="btn btn-warning">{{ Lang::get('global.edit')}}</a></td>

            <td style="text-align:center">
                {{ Form::open(array('url' =>'administrator/branch/'.$branch->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(Lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop