@extends('master')
@section('content')
<a class="btn btn-info " href="{{ URL::to('administrator/header_slider/create') }}"><?php echo Lang::get('global.add'); ?> </a>

<table class="table" style="margin-top:100px">
    <thead>
        <tr>
            <th style="text-align:center"><?php echo Lang::get('global.title'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.summery'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.photo'); ?></th>
<!--             <th style="text-align:center"><?php //echo Lang::get('global.content'); ?></th>-->
            <th style="text-align:center" colspan="2"><?php echo Lang::get('global.actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        @foreach($slider_images as $images)
        <tr>
             <td style="text-align:center">{{$images->title}}</td>
              <td style="text-align:center">{{$images->summery}}</td>
                

            <td style="text-align:center"><img src="{{ URL::asset('uploads/header_slider/'.$images->image) }}" width="100" height="100" /></td>
<!--           <td style="text-align:center">{{$images->content}}</td>-->
            <td style="text-align:center"><a href="header_slider/{{$images->id}}/edit" class="btn btn-warning">  {{ lang::get('global.edit') }}</a></td>
  <?php if($images->active ==1 ) { ?>
    
        <td style="text-align:center">  <img src="{{ URL::asset('uploads/img_delete.png') }}"  id="{{$images->id}}" class="active_disable"  />  </td>
        <?php }else { ?>
        <td style="text-align:center" >  <img src="{{ URL::asset('uploads/img_ok.png') }}"  id="{{$images->id}}" class="active_disable"  /> </td>
        <?php } ?>
            <td style="text-align:center">
                {{ Form::open(array('url' =>'administrator/header_slider/'.$images->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
          
        </tr>
        @endforeach
    </tbody>
</table>

<script type="text/javascript">
$('.active_disable').on('click', function(){
	var slider_id = $(this).attr('id');

	$.ajax({url:"header_slider/ActiveAjax/"+slider_id,success:function(result){
  	}});

	var base_url="{{URL::asset('')}}";
    var src = ($(this).attr('src') === base_url+'uploads/img_ok.png')
       ? base_url+'uploads/img_delete.png'
       : base_url+'uploads/img_ok.png';
    $(this).attr('src', src);
});
</script>

@stop