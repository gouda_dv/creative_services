@extends('master')
@section('content')
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif
@if(isset($slider_images))
{{ Form::open(array('method'=>'put','class' => 'form-bootstrap', 'role'=>'form','url'=>'administrator/header_slider/'. $slider_images->id,'files' => true ,'id'=> "slider")) }}
<div class="form-group ">

     {{ Form::hidden('id',$slider_images->id) }} 
</div>
<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_keywords'); ?>
    {{ Form::text('seo_meta_keywords',$slider_images->seo_meta_keywords,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
     <?php echo Lang::get('global.seo_meta_description'); ?>
    {{ Form::text('seo_meta_description',$slider_images->seo_meta_description,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    <?php echo Lang::get('global.title'); ?>
    {{ Form::text('title',$slider_images->title,array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.summery'); ?>
    {{ Form::text('summery',$slider_images->summery,array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
    <?php echo Lang::get('global.content'); ?>
    {{ Form::textarea('content',$slider_images->content,array('class'=>'ckeditor')) }} 

</div>

<div class="form-group">
    <img src="{{ URL::asset('uploads/header_slider/'.$slider_images->image) }}" width="100" height="100" style="margin-bottom: 15px;"/>
    {{ Form::file('image',array("data-validation"=>"mime size", "data-validation-allowing"=>"jpg, png, gif" ,"data-validation-max-size"=>"8M")) }} 

</div>
{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}

@else


{{ Form::open(array('role'=>'form','route'=>'administrator.header_slider.store','files' => true,'id '=> "slider")) }}

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_keywords'); ?>
    {{ Form::text('seo_meta_keywords',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_description'); ?>
    {{ Form::text('seo_meta_description',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.title'); ?>
    {{ Form::text('title',"",array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.summery'); ?>
    {{ Form::text('summery',"",array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.content'); ?>
   {{ Form::textarea('content',"",array('class'=>'ckeditor')) }} 
</div>

<div class="form-group">
    {{ Form::file('image',array("data-validation"=>"required mime size", "data-validation-allowing"=>"jpg, png, gif" ,"data-validation-max-size"=>"8M")) }} 

</div>
{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif
<script>  CKEDITOR.replace('content',
            {
                filebrowserBrowseUrl: "{{ URL::asset('ckfinder/ckfinder.html') }}",
                filebrowserImageBrowseUrl:"{{ URL::asset('ckfinder/ckfinder.html') }}?type=Images",
                filebrowserUploadUrl:
                        "{{ URL::asset('ckfinder') }}/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/",
                filebrowserImageUploadUrl:
                       "{{ URL::asset('ckfinder')}}/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/"
            });
</script>
<script> 
    $.validate({
  modules : 'file'
}); </script>
@stop