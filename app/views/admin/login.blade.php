<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Creative Services | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{ URL::asset('css/AdminLTE-ltr.css') }}" rel="stylesheet" type="text/css" />
    </head>
    <body class="bg-black">
{{ Form::open(array('role'=>'form','url'=>'administrator/admin/validate','method'=>'post')) }}
        <div class="form-box" id="login-box">
            <div class="header">{{ Lang::get('global.log_in')}}</div>

            <div class="body bg-gray">
                @if($errors)
                <div class="box box-solid box-danger ">
                    <div class=" box-header" >   
                        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
                        <?php echo $message; ?>
                        @endforeach
                    </div>
                </div>
                @endif
                

                <div class="form-group ">
                    {{ Form::label('admin_name',Lang::get('global.admin_name')) }}
                    {{ Form::text('admin_name',"",array('class'=>'form-control','placeholder'=>'Type Admin Name','autocomplete'=>'off')) }} 
                </div>

                <div class="form-group ">
                    {{ Form::label('password', Lang::get('global.password')) }}
                    {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Type Your Password','autocomplete'=>'off')) }} 
                </div>
            </div>
            <div class="footer">  
                {{ Form::submit(Lang::get('global.log_in') ,array('class'=>"btn btn-success")) }}

                
            </div>
            
        </div>
{{ Form::close() }}
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>