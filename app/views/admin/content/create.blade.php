@extends('master')
@section('content')

@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

@if(isset($content))
{{ Form::open(array('role'=>'form','url'=>'administrator/content/'. $content->id,'files' => true,'method'=>'put')) }}
<div class="form-group ">
    {{ Form::hidden('id',$content->id) }} 
</div>


<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title')) }}
    {{ Form::text('title',$content->title,array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
    {{ Form::label('content_category_id',Lang::get('global.category_id')) }}
    {{ Form::select('content_category_id',$cats,$content->content_category_id,array('class'=>'form-control',"data-validation"=>"required"))}}

</div>
<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords',$content->seo_meta_keywords,array('class'=>'form-control')) }}
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description',$content->seo_meta_description,array('class'=>'form-control')) }}
</div>

<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
     {{ Form::textarea('summery',$content->summery,array('class'=>'ckeditor')) }} 
   
</div>

<div class="form-group ">
   <?php echo Lang::get('global.date'); ?>
    {{ Form::text('date',date("m/d/20y", strtotime($content->date)),array('id'=>'datepicker','class'=>'datepicker')) }} 
</div>

<div class="form-group">
    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image',array("data-validation"=>"mime size", "data-validation-allowing"=>"jpg, png, gif" ,"data-validation-max-size"=>"8M")) }} 
    <img src="{{ URL::asset('uploads/content/'.$content->image) }}" width="100" height="100"  />
</div>

<div class="form-group ">
    {{ Form::label('content',Lang::get('global.content')) }}
    {{ Form::textarea('content',$content->content,array('class'=>'ckeditor')) }} 

</div>
{{ Form::submit(Lang::get('global.save'),array('class'=>"btn btn-success")) }}

{{ Form::close() }}

@else

{{ Form::open(array('role'=>'form','route'=>'administrator.content.store','files' => true)) }}
<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title') ) }}
    {{ Form::text('title','',array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>

<div class="form-group ">
    {{ Form::label('content_category_id', Lang::get('global.category_id')) }}
    {{ Form::select('content_category_id',$cats,null,array('class'=>'form-control',"data-validation"=>"required"))}}
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords','',array('class'=>'form-control')) }}
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description','',array('class'=>'form-control')) }}
</div>

<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::textarea('summery','',array('class'=>'ckeditor')) }} 
   
</div>
<div class="form-group ">
   <?php echo Lang::get('global.date'); ?>
    {{ Form::text('date',"",array('id'=>'datepicker','class'=>'datepicker')) }} 
</div>


<div class="form-group">
    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image',array("data-validation"=>"required mime size", "data-validation-allowing"=>"jpg, png, gif" ,"data-validation-max-size"=>"8M")) }} 
</div>

<div class="form-group ">
    {{ Form::label('content',Lang::get('global.content')) }}
    {{ Form::textarea('content','',array('class'=>'ckeditor')) }} 


</div>
{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif
<script>  CKEDITOR.replace('content',
            {
                filebrowserBrowseUrl: "{{ URL::asset('ckfinder/ckfinder.html') }}",
                filebrowserImageBrowseUrl:"{{ URL::asset('ckfinder/ckfinder.html') }}?type=Images",
                filebrowserUploadUrl:
                        "{{ URL::asset('ckfinder') }}/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/",
                filebrowserImageUploadUrl:
                       "{{ URL::asset('ckfinder')}}/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/"
            });
</script>
<script> 
    $.validate({
  modules : 'file'
}); 
$('.datepicker').datepicker();
</script>
@stop