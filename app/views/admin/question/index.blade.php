@extends('master')
@section('content')
<a href="question/create" style="margin-top: 36px;" class="btn btn-info">{{ Lang::get('global.add')}}</a>
<table class="table" style="margin-top:50px">
    <thead>
        <tr>
            <th style="text-align:center">#</th>
            <th style="text-align:center">{{ Lang::get('global.question')}}</th>

            <th style="text-align:center" colspan="2"><?php echo Lang::get('global.options'); ?></th>
        </tr>
    </thead>
    <tbody>
        @foreach($all_questions as $question)
        <tr>
            <td style="text-align:center">{{$question->id}}</td>
            <td style="text-align:center">{{$question->question}}</td>
            <td style="text-align:center"><a href="question/{{$question->id}}/edit" class="btn btn-warning">{{ Lang::get('global.edit')}}</a></td>

            <td style="text-align:center">
                {{ Form::open(array('url'=>'administrator/question/'.$question->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(Lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close()}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop