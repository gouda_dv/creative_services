@extends('master')
@section('content')
<script>

    // When the browser is ready...
    $(document).ready(function() {

    // Setup form validation on the #register-form element
    $("#center_member_form").validate({
    // Specify the validation rules
    rules: {
    title: "required",
    },
            // Specify the validation error messages
            messages: {
            title: " {{Lang::get('global.required_msg')}} {{Lang::get('global.question')}} ",
            },
            submitHandler: function(form) {
            form.submit();
            }
    });
    });</script>
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

@if(isset($question))
{{ Form::open(array('role'=>'form','url'=>'administrator/question/'. $question->id,'files' => true,'method'=>'put')) }}
<div class="form-group ">
    {{ Form::hidden('id',$question->id) }} 
</div>


<div class="form-group ">
    {{ Form::label('question',Lang::get('global.question')) }}
    {{ Form::text('question',$question->question,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    <div id="dynamicInput">
        <?php $i = 1 ?>
        @foreach($choice_details as $choice)
        <div style="margin-left:0px 0px 10px 10px">
            {{ Form::label('choice',Lang::get('global.question_choice').$i ) }}
            {{ Form::text('oldchoice['.$choice->id.']',$choice->choice,array('class'=>'form-control ')) }} 
        </div>
        <a  href='{{ URL::to('administrator/') }}/question/destroy_choice/{{$choice->id}}' class="btn btn-warning" style="margin-bottom: 10px" onsubmit="return confirm('{{Lang::get('global.delete_msg')}}');" >Remove</a>
        <?php $i++ ?>
        @endforeach
    </div>
    <br />
    <input type="button" value="{{Lang::get('global.add_another_choice')}} " class="add_field_button btn btn-info" >
    <br />
</div>

<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords',$question->seo_meta_keywords,array('class'=>'form-control')) }}
</div>

<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description',$question->seo_meta_description,array('class'=>'form-control')) }}
</div>

{{ Form::submit(Lang::get('global.save'),array('class'=>"btn btn-success")) }}

{{ Form::close() }}

@else

{{ Form::open(array('role'=>'form','route'=>'administrator.question.store','files' => true)) }}
<div class="form-group ">
    {{ Form::label('question',Lang::get('global.question') ) }}
    {{ Form::text('question','',array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    <div id="dynamicInput">
        {{ Form::label('choice',Lang::get('global.question_choice')."1") }}
        {{ Form::text('newchoice[]',null,array('class'=>'form-control')) }} 
    </div>
</div>

<div style="margin: 0px 0px 10px 0px;">
    <input type="button" value="{{Lang::get('global.add_another_choice')}}" class="btn btn-info add_field_button" onClick="addInput('dynamicInput');">
</div>

<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords','',array('class'=>'form-control')) }}
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description','',array('class'=>'form-control')) }}
</div>

{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif
<script type="text/javascript">
//    var counter = 1;
//    var limit = 10;
//    function addInput(divName) {
//        if (counter == limit) {
//            alert("{{Lang::get('global.limit_msg')}}  " + counter + "  {{Lang::get('global.choices')}} ");
//        }
//        else {
//            var newdiv = document.createElement('div');
//            newdiv.innerHTML = "<label>{{Lang::get('global.new_choice')}} </label> " + " <br><input type='text' name='newchoice[]' class='form-control'>";
//            document.getElementById(divName).appendChild(newdiv);
//            counter++;
//        }
//    }


                    var max_fields = 10; //maximum input boxes allowed
                    var wrapper = $("#dynamicInput"); //Fields wrapper
                    var add_button = $(".add_field_button"); //Add button ID

                    var x = 1; //initlal text box count
                    $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
                    if (x < max_fields){ //max input box allowed
            x++; //text box increment
                    $(wrapper).append('<div> <label style="display:block;">{{Lang::get("global.new_choice")}} </label> <input style="margin-left:10px" type="text" name="newchoice[]" class="form-control"/><a href="#" class="remove_field btn btn-warning">Remove</a></div>'); //add input box
            }
            });
                    $(wrapper).on("click", ".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
            })

</script>


@stop