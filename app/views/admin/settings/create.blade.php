@extends('master')
@section('content')
<script>

    // When the browser is ready...
    $(document).ready(function() {

        // Setup form validation on the #register-form element
        $("#center_member_form").validate({
            // Specify the validation rules
            rules: {
                title: "required",
                content_category_id: "required",
                image: {
                    accept: "image/*"
                }
            },
            // Specify the validation error messages
            messages: {
                title: " {{Lang::get('global.required_msg')}} {{Lang::get('global.title')}} ",
                content_category_id: " {{Lang::get('global.required_msg')}} {{Lang::get('global.category_id')}} ",
                image: {
                    accept: " {{Lang::get('global.valid_msg')}} {{Lang::get('global.image')}}"
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif
@if(isset($success))
<div class="box box-solid box-success ">
    <div class=" box-header" > 
        <li style="list-style: none;margin: 10px">{{Lang::get('global.success_msg')}}</li>
        <?php $success=null; ?>
    </div>
</div>
@endif

@if(isset($settings))
{{ Form::open(array('role'=>'form','url'=>'administrator/settings/'. $settings->id,'method'=>'put')) }}
<div class="form-group ">
    {{ Form::hidden('id',$settings->id) }} 
</div>


<div class="form-group ">
    {{ Form::label('facebook_link',Lang::get('global.facebook_link')) }}
    {{ Form::text('facebook_link',$settings->facebook_link,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    {{ Form::label('twitter_link',Lang::get('global.twitter_link')) }}
    {{ Form::text('twitter_link',$settings->twitter_link,array('class'=>'form-control')) }}
</div>
<div class="form-group ">
    {{ Form::label('youtube_link',Lang::get('global.youtube_link')) }}
    {{ Form::text('youtube_link',$settings->youtube_link,array('class'=>'form-control')) }}
</div>

<div class="form-group ">
    {{ Form::label('google_plus_link',Lang::get('global.google_plus_link')) }}
    {{ Form::text('google_plus_link',$settings->google_plus_link,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    {{ Form::label('instagram_link',Lang::get('global.instagram_link')) }}
    {{ Form::text('instagram_link',$settings->instagram_link,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    {{ Form::label('linkedin_link',Lang::get('global.linkedin_link')) }}
    {{ Form::text('linkedin_link',$settings->linkedin_link,array('class'=>'form-control')) }} 
</div>
<!--<div class="form-group ">
    {{ Form::label('address',Lang::get('global.address')) }}
    {{ Form::text('address',$settings->address,array('class'=>'form-control')) }} 
</div>-->


{{ Form::submit(Lang::get('global.save'),array('class'=>"btn btn-success")) }}

{{ Form::close() }}


@endif

@stop