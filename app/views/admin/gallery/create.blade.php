@extends('master')
@section('content')
<script>

    // When the browser is ready...
    $(document).ready(function() {

        // Setup form validation on the #register-form element
        $("#center_member_form").validate({
            // Specify the validation rules
            rules: {
                title: "required",
                image: {
                    required: true,
                    accept: "image/*"
                }
            },
            // Specify the validation error messages
            messages: {
                title: " {{Lang::get('global.required_msg')}} {{Lang::get('global.title')}} ",
                image: {
                    accept: " {{Lang::get('global.valid_msg')}} {{Lang::get('global.image')}}"
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>

@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

@if(isset($gallery))
{{ Form::open(array('role'=>'form','url'=>'administrator/gallery/'. $gallery->id,'files' => true,'method'=>'put')) }}
<div class="form-group ">
    {{ Form::hidden('id',$gallery->id) }} 
</div>


<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title')) }}
    {{ Form::text('title',$gallery->title,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords',$gallery->seo_meta_keywords,array('class'=>'form-control')) }}
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description',$gallery->seo_meta_description,array('class'=>'form-control')) }}
</div>

<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::text('summery',$gallery->summery,array('class'=>'form-control')) }} 
</div>



<div class="form-group">
    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image') }} 
    <img src="{{ URL::asset('uploads/gallery/'.$gallery->image) }}" width="100" height="100"  />
</div>
<?php if($gallery->active == 1) { ?>
<div class="form-group">
    {{ Form::label('active',Lang::get('global.active')) }}
    {{ Form::checkbox('active','1',true) }} 
</div>
<?php }else {?>
<div class="form-group">
    {{ Form::label('active',Lang::get('global.active')) }}
    {{ Form::checkbox('active','0') }} 
</div>
<?php }?>

{{ Form::submit(Lang::get('global.save'),array('class'=>"btn btn-success")) }}

{{ Form::close() }}

@else

{{ Form::open(array('role'=>'form','route'=>'administrator.gallery.store','files' => true)) }}
<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title') ) }}
    {{ Form::text('title','',array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords','',array('class'=>'form-control')) }}
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description','',array('class'=>'form-control')) }}
</div>

<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::text('summery','',array('class'=>'form-control')) }} 
</div>



<div class="form-group">
    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image') }} 
</div>
<div class="form-group">
    {{ Form::label('active',Lang::get('global.active')) }}
    {{ Form::checkbox('active','1',true) }} 
</div>

{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif

@stop