@extends('master')
@section('content')
<a href="gallery/create"style="margin-top: 36px;display: block;width: 60px;" class="btn btn-info">{{ Lang::get('global.add')}}</a>
@foreach($all_galleries as $gallery)
<div style="float:right;text-align:center ;width:100px;height: 100px; margin: 40px;" >
  
    <img class="gallery_photo" id="{{$gallery->id}}" src="{{ URL::asset('uploads/gallery/'.$gallery->image) }}" width="100" height="100" style="cursor: pointer;" /><br/>{{$gallery->title}} <br/>
 

    <td style="text-align:center; width:10px;margin: 5px;"><a href="gallery/{{$gallery->id}}/edit" > <img src="{{ URL::asset('uploads/edit.png') }}"  />  </a></td>
     <td style="text-align:center; width:10px;margin: 5px;">
         
                {{ Form::open(array('url'=>'administrator/gallery/'.$gallery->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                <button type="submit" style="border: 0; background: transparent">
    <img src="{{ URL::asset('uploads/del.png') }}" width="30" height="30" alt="submit" />
</button>

                {{ Form::close() }}
 </td>
</div>


 @endforeach

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
  <!--              <img id="mimg" src="" height="400" width="600">-->

                <div class="container">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Add files...</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="files[]" multiple>
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress" style="width: 48%;">
                        <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <!-- The container for the uploaded files -->  
                </div>


            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $('.gallery_photo').on('click', function()
    {
        //var sr = $(this).attr('src');
		//$('#mimg').attr('src',sr);
        //$('#myModal').modal('show');

        var gallery_id = $(this).attr('id');
        window.open("{{ URL::to('administrator/gallery/open_uploads_popup') }}"+"/"+gallery_id,'1412233895408','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
        
    });

</script>
@stop