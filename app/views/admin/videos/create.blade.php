@extends('master')
@section('content')
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

@if(isset($videos))


{{ Form::open(array('method'=>'put','class' => 'form-bootstrap', 'role'=>'form','url'=>'administrator/videos/'. $videos->id,'files' => true)) }}

<div class="form-group ">
     {{ Form::hidden('id',$videos->id) }} 
</div>

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_keywords'); ?>
    {{ Form::text('seo_meta_keywords',$videos->seo_meta_keywords,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
     <?php echo Lang::get('global.seo_meta_description'); ?>
    {{ Form::text('seo_meta_description',$videos->seo_meta_description,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.title'); ?>
    {{ Form::text('title',$videos->title,array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
     <?php echo Lang::get('global.summery'); ?>
   {{ Form::textarea('summery',$videos->summery,array('class'=>'ckeditor')) }} 
   
</div>

<div class="form-group ">
    {{ Form::label('video_category_id',Lang::get('global.category_id')) }}
    {{ Form::select('video_category_id',$cats,$videos->video_category_id,array('class'=>'form-control'))}}

</div>

<div class="form-group ">
     <?php echo Lang::get('global.youtube_link'); ?>
    {{ Form::text('youtube_link',$videos->youtube_link,array('class'=>'form-control')) }} 
</div>


<div class="form-group">
    <?php if($videos->video != "") {?>
    <td style="text-align:center"><a href="{{ URL::asset('uploads/videos/'.$videos->video) }}" class="btn btn-info" >video</a></td>
    {{ Form::file('video') }} 
    <?php }else { ?>
    <td style="text-align:center"> </td>
    {{ Form::file('video') }} 
    <?php }?>
</div>

<?php if($videos->video_paid == 1) { ?>
<div class="form-group">
    {{ Form::label('file_paid',Lang::get('global.file_paid')) }}
    {{ Form::checkbox('video_paid','1',true) }} 
</div>
 <div class="form-group">
     <?php echo Lang::get('global.price'); ?>
    {{ Form::text('price',$videos->video_price,array('class'=>'form-control price')) }} 
     <?php echo Lang::get('global.coin'); ?>
    {{ Form::select('coin',$coin,$videos->video_currency_id,array('class'=>'form-control coin')) }} 
</div>
<?php }else {?>
<div class="form-group">
    {{ Form::label('file_paid',Lang::get('global.file_paid')) }}
    {{ Form::checkbox('video_paid','0') }} 
</div>
 <div class="form-group">
     <?php echo Lang::get('global.price'); ?>
    {{ Form::text('price','',array('class'=>'form-control price','disabled'=>'disabled')) }} 
     <?php echo Lang::get('global.coin'); ?>
    {{ Form::select('coin',$coin,array('class'=>'form-control coin'),array('disabled')) }} 
</div>
<?php }?>
{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}

@else

{{ Form::open(array('role'=>'form','route'=>'administrator.videos.store','files' => true,'class'=>'test')) }}

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_keywords'); ?>
    {{ Form::text('seo_meta_keywords',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_description'); ?>
    {{ Form::text('seo_meta_description',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.title'); ?>
    {{ Form::text('title',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
     <?php echo Lang::get('global.summery'); ?>
   {{ Form::textarea('summery',"",array('class'=>'ckeditor')) }} 
   
</div>

<div class="form-group ">
    {{ Form::label('video_category_id',Lang::get('global.category_id')) }}
    {{ Form::select('video_category_id',$cats,'',array('class'=>'form-control'))}}

</div>

<div class="form-group ">
   <?php echo Lang::get('global.youtube_link'); ?>
    {{ Form::text('youtube_link',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group">
    {{ Form::file('video') }} 

</div>

<div class="form-group">
    {{ Form::label('file_paid',Lang::get('global.file_paid')) }}
    {{ Form::checkbox('video_paid','1',true,array('id'=>'video_paid','class'=>'video_paid')) }}
 </div>
   <div class="form-group">
     <?php echo Lang::get('global.price'); ?>
    {{ Form::text('price','',array('class'=>'form-control price')) }} 
     <?php echo Lang::get('global.coin'); ?>
    {{ Form::select('coin',$coin,array('class'=>'form-control coin')) }} 
</div>
{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif
<script>  CKEDITOR.replace('summery',
            {
                filebrowserBrowseUrl: "{{ URL::asset('ckfinder/ckfinder.html') }}",
                filebrowserImageBrowseUrl:"{{ URL::asset('ckfinder/ckfinder.html') }}?type=Images",
                filebrowserUploadUrl:
                        "{{ URL::asset('ckfinder') }}/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/",
                filebrowserImageUploadUrl:
                       "{{ URL::asset('ckfinder')}}/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/"
            });
</script>
<script>
    
  $("#video_paid").click(function (){
      alert('ok');
      
  });
</script>
@stop