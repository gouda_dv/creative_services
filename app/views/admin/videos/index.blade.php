@extends('master')
@section('content')
<a class="btn btn-info " href="{{ URL::to('administrator/videos/create') }}"><?php echo Lang::get('global.add'); ?> </a>
<table class="table" style="margin-top:100px">
    <thead>
        <tr>
            <th style="text-align:center"><?php echo Lang::get('global.title'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.summery'); ?></th>
            
             <th style="text-align:center"><?php echo Lang::get('global.link'); ?></th>
             <th style="text-align:center"><?php echo Lang::get('global.video'); ?></th>
           
            <th style="text-align:center" colspan="2"><?php echo Lang::get('global.actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        @foreach($videos as $video)
        <tr>
             <td style="text-align:center">{{$video->title}}</td>
              <td style="text-align:center">{{$video->summery}}</td>
               <td style="text-align:center">{{$video->youtube_link}}</td>
<?php if($video->video !="") {?>
               <td style="text-align:center"><a href="{{ URL::asset('uploads/videos/'.$video->video) }}" class="btn btn-info" download="video">Download Video</a></td>
<?php } else { ?>
     <td style="text-align:center"><?php echo Lang::get('global.not_found'); ?></td>
   
<?php }?>
               <td style="text-align:center"><a href="videos/{{$video->id}}/edit" class="btn btn-warning">{{ lang::get('global.edit') }}</a></td>
   
            <td style="text-align:center">
                {{ Form::open(array('url'=>'administrator/videos/'.$video->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');"))}}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
          
        </tr>
        @endforeach
    </tbody>
</table>

@stop