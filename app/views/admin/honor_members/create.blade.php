@extends('master')
@section('content')
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

@if(isset($members))

{{ Form::open(array('method'=>'put','class' => 'form-bootstrap', 'role'=>'form','url'=>'administrator/honor_members/'. $members->id,'files' => true)) }}
<div class="form-group ">
    {{ Form::hidden('id',$members->id) }} 
</div>
<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_keywords'); ?>
    {{ Form::text('seo_meta_keywords',$members->seo_meta_keywords,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
     <?php echo Lang::get('global.seo_meta_description'); ?>
    {{ Form::text('seo_meta_description',$members->seo_meta_description,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    <?php echo Lang::get('global.title'); ?>
    {{ Form::text('title',$members->title,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.summery'); ?>
    {{ Form::text('summery',$members->summery,array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    <?php echo Lang::get('global.content'); ?>
    {{ Form::textarea('content',$members->content,array('class'=>'ckeditor')) }} 
</div>

<div class="form-group">
    <img src="{{ URL::asset('uploads/honor_members/'.$members->image) }}" width="100" height="100" style="margin-bottom: 15px;"/>
    {{ Form::file('image') }} 

</div>
{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}


@else



{{ Form::open(array('role'=>'form','route'=>'administrator.honor_members.store','files' => true)) }}

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_keywords'); ?>
    {{ Form::text('seo_meta_keywords',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    <?php echo Lang::get('global.seo_meta_description'); ?>
    {{ Form::text('seo_meta_description',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.title'); ?>
    {{ Form::text('title',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
   <?php echo Lang::get('global.summery'); ?>
    {{ Form::text('summery',"",array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
   <?php echo Lang::get('global.content'); ?>
   {{ Form::textarea('content',"",array('class'=>'ckeditor')) }} 
</div>

<div class="form-group">
    {{ Form::file('image') }} 

</div>
{{ Form::submit(lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}

@endif
<script>  CKEDITOR.replace('content',
            {
                filebrowserBrowseUrl: "{{ URL::asset('ckfinder/ckfinder.html') }}",
                filebrowserImageBrowseUrl:"{{ URL::asset('ckfinder/ckfinder.html') }}?type=Images",
                filebrowserUploadUrl:
                        "{{ URL::asset('ckfinder') }}/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/",
                filebrowserImageUploadUrl:
                       "{{ URL::asset('ckfinder')}}/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/"
            });
</script>
@stop