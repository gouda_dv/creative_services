@extends('master')
@section('content')
<a class="btn btn-info " href="{{ URL::to('administrator/honor_members/create') }}"><?php echo Lang::get('global.add'); ?> </a>
<table class="table" style="margin-top:100px">
    <thead>
        <tr>
            <th style="text-align:center"><?php echo Lang::get('global.title'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.summery'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.content'); ?></th>
            <th style="text-align:center"><?php echo Lang::get('global.photo'); ?></th>
            <th style="text-align:center" colspan="2"><?php echo Lang::get('global.actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        @foreach($members as $member)
        <tr>
             <td style="text-align:center">{{$member->title}}</td>
              <td style="text-align:center">{{$member->summery}}</td>
               <td style="text-align:center">{{$member->content}}</td>

            <td style="text-align:center"><img src="{{ URL::asset('uploads/honor_members/'.$member->image) }}" width="100" height="100" /></td>
            <td style="text-align:center"><a href="honor_members/{{$member->id}}/edit" class="btn btn-warning">{{ lang::get('global.edit') }}</a></td>
   
            <td style="text-align:center">
                {{ Form::open(array('url'=>'administrator/honor_members/'.$member->id , 'class' => 'pull-right' ,'onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
          
        </tr>
        @endforeach
    </tbody>
</table>

@stop