@extends('master')
@section('content')
<a href="service_category/create" style="margin-top: 36px;" class="btn btn-info">{{ Lang::get('global.add')}}</a>
<table class="table" style="margin-top:50px">
    <thead>
        <tr>
           
            <th style="text-align:center">{{ Lang::get('global.title')}}</th>
            <th style="text-align:center">{{ Lang::get('global.parent')}}</th>
            <th style="text-align:center">{{Lang::get('global.summery')}}</th>
            <th style="text-align:center">{{ Lang::get('global.image')}}</th>

            <th style="text-align:center" colspan="2"><?php echo Lang::get('global.options'); ?></th>
        </tr>
    </thead>
    <tbody>
        @foreach($all_content as $content)
        <tr>
           
            <td style="text-align:center">{{$content->title}}</td>
            <td style="text-align:center">{{$content->parent}}</td>
            <td style="text-align:center">{{$content->summery}}</td>

            <td style="text-align:center"><img src="{{ URL::asset('uploads/service_category/'.$content->image) }}" width="100" height="100" /></td>
            <td style="text-align:center"><a href="service_category/{{$content->id}}/edit" class="btn btn-warning">{{ Lang::get('global.edit')}}</a></td>

            <td style="text-align:center">
                {{ Form::open(array('url'=>'administrator/service_category/'.$content->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(Lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close()}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop