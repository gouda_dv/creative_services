@extends('master')
@section('content')
<a href="center_members/create" style="margin-top: 36px;" class="btn btn-info ">{{ Lang::get('global.add')}}</a>
<table class="table" style="margin-top:50px">
    <thead>
        <tr>
            <th style="text-align:center">#</th>
            <th style="text-align:center">{{ Lang::get('global.title')}}</th>
            <th style="text-align:center">{{ Lang::get('global.emails')}}</th>
            <th style="text-align:center">{{ Lang::get('global.mobiles')}}</th>
            <th style="text-align:center">{{ Lang::get('global.phones')}}</th>
            <th style="text-align:center">{{ Lang::get('global.summery')}}</th>
            <th style="text-align:center">{{ Lang::get('global.image')}}</th>
            <th style="text-align:center">{{ Lang::get('global.cv')}}</th>
            <th style="text-align:center" colspan="2">{{ Lang::get('global.options')}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($all_members as $member)
        <tr>
            <td style="text-align:center">{{$member->id}}</td>
            <td style="text-align:center">{{$member->title}}</td>
            <td style="text-align:center">{{$member->emails}}</td>
            <td style="text-align:center">{{$member->mobiles}}</td>
            <td style="text-align:center">{{$member->phones}}</td>
            <td style="text-align:center">{{$member->summery}}</td>

            <td style="text-align:center"><img src="{{ URL::asset('uploads/center_member/'.$member->image) }}" width="100" height="100" /></td>
            <td style="text-align:center"><a href="{{ URL::asset('uploads/center_member/'.$member->cv) }}" download="CV" class="btn btn-primary">{{ Lang::get('global.download_cv')}}</a></td>
            <td style="text-align:center"><a href="center_members/{{$member->id}}/edit" class="btn btn-warning">{{ Lang::get('global.edit')}}</a></td>

            <td style="text-align:center">
                {{ Form::open(array('url'=>'administrator/center_members/'.$member->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(Lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop