@extends('master')
@section('content')
<script>
jQuery.noConflict();
    // When the browser is ready...

   (function(jQuery) {

        // Setup form validation on the #register-form element
        jQuery("#center_member_form").validate({
            // Specify the validation rules
            rules: {
                title: "required",
                image: {
                    required: true,
                    accept: "image/*"
                },
                cv: {
                    extension: "docx|doc|pdf"
                },
                mobiles: {
                    number: true
                },
                phones: {
                    number: true
                },
                emails: {
                    email: true
                },
            },
            // Specify the validation error messages
            messages: {
                title: " {{Lang::get('global.required_msg')}} {{Lang::get('global.title')}} ",
                image: {
                    required: "{{Lang::get('global.required_msg')}} {{Lang::get('global.image')}}",
                    accept: " {{Lang::get('global.valid_msg')}} {{Lang::get('global.image')}}"
                },
                cv: " {{Lang::get('global.valid_msg')}} {{Lang::get('global.cv')}} ",
                email: "{{Lang::get('global.valid_msg')}} {{Lang::get('global.emails')}}",
                phones: "{{Lang::get('global.valid_msg')}} {{Lang::get('global.phones')}}",
                mobiles: "{{Lang::get('global.valid_msg')}} {{Lang::get('global.mobiles')}}"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

@if(isset($member))
{{ Form::open(array('role'=>'form','url'=>'administrator/center_members/'. $member->id,'files' => true,'method'=>'put','id'=>'center_member_form','novalidate'=>'novalidate')) }}
<div class="form-group ">
    {{ Form::hidden('id',$member->id) }} 
</div>

<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title')) }}
    {{ Form::text('title',$member->title,array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    {{ Form::label('emails',Lang::get('global.emails')) }}
    {{ Form::email('emails',$member->emails,array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    {{ Form::label('mobiles',Lang::get('global.mobiles')) }}
    {{ Form::input('tel', 'mobiles',$member->mobiles, ['class' => 'form-control']) }}
</div>
<div class="form-group ">
    {{ Form::label('phones',Lang::get('global.phones')) }}
    {{ Form::input('tel', 'phones', $member->phones, ['class' => 'form-control']) }}
</div>

<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::text('summery',$member->summery,array('class'=>'form-control')) }} 
</div>



<div class="form-group">

    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image') }} 
    <img src="{{ URL::asset('uploads/center_member/'.$member->image) }}" width="100" height="100"  />
</div>

<div class="form-group">
    {{ Form::label('cv',Lang::get('global.cv')) }}
    {{ Form::file('cv',array('display'=>'inline')) }} 
    @if($member->cv)
    <a  href="{{ URL::asset('uploads/center_member/'.$member->cv) }}" class="btn btn-primary">{{Lang::get('global.download_cv')}}</a>
    @endif
</div>

<div class="form-group ">
    {{ Form::label('content',Lang::get('global.content')) }}
    {{ Form::textarea('content',$member->content,array('class'=>'ckeditor')) }} 
</div>
{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}


@else

{{ Form::open(array('role'=>'form','route'=>'administrator.center_members.store','files' => true,'id'=>'center_member_form')) }}

<div class="form-group ">
   {{ Form::label('title',Lang::get('global.title')) }}
    {{ Form::text('title',"",array('class'=>'form-control')) }} 
</div>

<div class="form-group ">
    {{ Form::label('emails',Lang::get('global.emails')) }}
    {{ Form::email('emails',"",array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    {{ Form::label('mobiles',Lang::get('global.mobiles')) }}
    {{ Form::input('tel', 'mobiles', null, ['class' => 'form-control']) }}
</div>
<div class="form-group ">
    {{ Form::label('phones',Lang::get('global.phones')) }}
    {{ Form::input('tel', 'phones', null, ['class' => 'form-control']) }}
</div>

<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::text('summery',"",array('class'=>'form-control')) }} 
</div>



<div class="form-group">
    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image') }} 
</div>

<div class="form-group">
    {{ Form::label('cv',Lang::get('global.cv')) }}
    {{ Form::file('cv') }} 
</div>

<div class="form-group ">
    {{ Form::label('content',Lang::get('global.content')) }}
    {{ Form::textarea('content',"",array('class'=>'ckeditor')) }} 
</div>
{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif
<script>  CKEDITOR.replace('content',
            {
                filebrowserBrowseUrl: "{{ URL::asset('ckfinder/ckfinder.html') }}",
                filebrowserImageBrowseUrl:"{{ URL::asset('ckfinder/ckfinder.html') }}?type=Images",
                filebrowserUploadUrl:
                        "{{ URL::asset('ckfinder') }}/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/",
                filebrowserImageUploadUrl:
                       "{{ URL::asset('ckfinder')}}/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/"
            });
</script>
@stop