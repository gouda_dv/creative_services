@extends('master')
@section('content')

@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

@if(isset($nav_link))
{{ Form::open(array('role'=>'form','url'=>'administrator/nav_link/'. $nav_link->id,'files' => true,'method'=>'put')) }}
<div class="form-group ">
    {{ Form::hidden('id',$nav_link->id) }} 
    {{ Form::hidden('record_id',$nav_link->record,array('id'=>'record_id')) }} 
</div>


<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title')) }}
    {{ Form::text('title',$nav_link->title,array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::text('summery',$nav_link->summery,array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image',array("data-validation"=>"mime size", "data-validation-allowing"=>"jpg, png, gif" ,"data-validation-max-size"=>"8M")) }} 
    <img src="{{ URL::asset('uploads/nav_link/'.$nav_link->image) }}" width="100" height="100"  />
</div>
<div class="form-group ">
    {{ Form::label('nav_id', Lang::get('global.nav')) }}
    {{ Form::select('nav_id',$selectedNavs,$nav_link->nav_id,array('class'=>'form-control'))}}
</div>

<div class="form-group ">
    {{ Form::label('parent', Lang::get('global.parent')) }}
    {{ Form::select('parent',$selectedLinks,$nav_link->parent,array('class'=>'form-control'))}}
</div>

<div class="form-group ">
    {{ Form::label('screen', Lang::get('global.screen')) }}
    {{ Form::select('screen',$selectedScreens,$nav_link->screen,array('class'=>'form-control'))}}
</div>

<div class="form-group record_group" >

</div>


{{ Form::submit(Lang::get('global.save'),array('class'=>"btn btn-success")) }}

{{ Form::close() }}

@else

{{ Form::open(array('role'=>'form','route'=>'administrator.nav_link.store','files' => true)) }}
<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title') ) }}
    {{ Form::text('title','',array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery') ) }}
    {{ Form::text('summery','',array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    {{ Form::label('image',Lang::get('global.image') ) }}
   {{ Form::file('image')}}
</div>

<div class="form-group ">
    {{ Form::label('nav_id', Lang::get('global.nav')) }}
    {{ Form::select('nav_id',$selectedNavs,null,array('class'=>'form-control'))}}
</div>

<div class="form-group ">
    {{ Form::label('parent', Lang::get('global.parent')) }}
    {{ Form::select('parent',$selectedLinks,null,array('class'=>'form-control'))}}
</div>

<div class="form-group ">
    {{ Form::label('screen', Lang::get('global.screen')) }}
    {{ Form::select('screen',$selectedScreens,null,array('class'=>'form-control'))}}
</div>

<div class="form-group record_group" >

</div>

{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif

<script>

    var screen_val = $("#screen").val();
            if (screen_val == 0) {

    select = '<label>{{Lang::get("global.record")}}</label><select name="record" class="form-control" id="record" >';
            select += '</select>';
            $(".record_group").html(select);
    }
    else{
    $.getJSON("{{ URL::to('administrator/nav_link/screen_record') }}" + "/" + screen_val, function(jsonData) {
    select = '<label>{{Lang::get("global.record")}}</label><select name="record" class="form-control  " id="record" >';
            select += '<option value="0">{{Lang::get('global.choose')}}</option>';
            $.each(jsonData, function(i, data)
            {
//            if (!data){
//            select = '<label>{{Lang::get("global.record")}}</label><select name="record" class="form-control" id="record" >';
//                    select += '</select>';
//                    $(".record_group").html(select);
//            }
            if (data.id == $("#record_id").val())
            {
            select += '<option selected="selected" value="' + data.id + '">' + data.title + '</option>';
            }
            else{
            select += '<option value="' + data.id + '">' + data.title + '</option>';
            }
            });
            select += '</select>';
            $(".record_group").html(select);
    });
    }

    if ($("#nav_id").val() != 0)
    {
    $("#parent").attr("disabled", true);
    }
    else
    {
    $("#parent").removeAttr("disabled");
    }


    if ($("#parent").val() != 0)
    {
    $("#nav_id").attr("disabled", true);
    }
    else
    {
    $("#nav_id").attr("disabled", false); }





    /* Load positions into postion <selec> */
    $("#screen").change(function()
    {
    if ($(this).val() == 0) {
    select = '<label>{{Lang::get("global.record")}}</label><select name="record" class="form-control  " id="record" >';
            select += '</select>';
            $(".record_group").html(select);
    }
    else{
    $.getJSON("{{ URL::to('administrator/nav_link/screen_record') }}" + "/" + $(this).val(), function(jsonData) {    
//        if(jsonData.length === 0)    
//        {
//            select = '<label>{{Lang::get("global.record")}}</label><select name="record" class="form-control" id="record" >';
//                    select += '</select>';
//                    $(".record_group").html(select);
//            }
    select = '<label>{{Lang::get("global.record")}}</label><select name="record" class="form-control  " id="record" >';
            select += '<option value="0">{{Lang::get('global.choose')}}</option>';
            
        
    $.each(jsonData, function(i, data)
            {
            

            select += '<option value="' + data.id + '">' + data.title + '</option>';
            });
            select += '</select>';
            $(".record_group").html(select);
    });
    }
    });
            $("#nav_id").change(function()
    {
    if ($(this).val() != 0)
    {
    $("#parent").attr("disabled", true);
    }
    else
    {
    $("#parent").removeAttr("disabled");
    }
    });
            $("#parent").change(function()
    {
    if ($(this).val() != 0)
    {
    $("#nav_id").attr("disabled", true);
    }
    else
    {
    $("#nav_id").attr("disabled", false);
    }
    });
</script>

@stop