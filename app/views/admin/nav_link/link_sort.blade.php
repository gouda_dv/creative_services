@extends('master')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('js/sort/style.css') }}" />
<!--<script type="text/javascript" src="{{ URL::asset('js/sort/script.js') }}"></script>-->

<div style="margin-bottom: 50px;margin-right: 40px;"> <a href="{{ URL::previous() }}" style="float: right;" class="btn btn-info">{{ Lang::get('global.back')}}</a></div>

<ul id="sortable">
    @foreach($all_links as $link)
    <li id='{{$link->id}}'> 
        <span class="ui-sortable-handle"></span>
        <div>
 <a href="{{ URL::to('administrator/nav_link/view_order_internal_links/'.$link->id) }}" style="float: right;" class="btn btn-info">{{ Lang::get('global.internal_links')}}</a>
            <h2>{{$link->title}}</h2>
        </div>
                   
</li>
@endforeach
</ul>
<script>

    $('#sortable').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function(event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
            // change order in the database using Ajax
            $.ajax({
                url: "{{ URL::to('administrator/nav_link/set_order_links') }}",
                type: 'POST',
                data: {list_order: list_sortable},
                success: function(data) {
                    //finished
                }
            });
        }
    }); // fin sortable

</script>
@stop