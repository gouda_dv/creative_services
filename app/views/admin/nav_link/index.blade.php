@extends('master')
@section('content')
<a href="nav_link/create" style="margin-top: 36px;" class="btn btn-info">{{ Lang::get('global.add')}}</a>

<a href="{{ URL::to('administrator/nav_link/view_order_navs') }}" style="margin-right: 450px;margin-top: 35px;" class="btn btn-info">{{ Lang::get('global.navs')}}</a>

<table class="table" style="margin-top:50px">
    <thead>
        <tr>
            <th style="text-align:center">#</th>
            <th style="text-align:center">{{ Lang::get('global.title')}}</th>
             <th style="text-align:center">{{ Lang::get('global.summery')}}</th>
              <th style="text-align:center">{{ Lang::get('global.image')}}</th>
            <th style="text-align:center">{{ Lang::get('global.link')}}</th>
            <th style="text-align:center">{{Lang::get('global.nav')}}</th>
            <th style="text-align:center">{{ Lang::get('global.parent')}}</th>

            <th style="text-align:center" colspan="2"><?php echo Lang::get('global.options'); ?></th>
        </tr>
    </thead>
    <tbody>
        @foreach($all_links as $link)
        <tr>
            <td style="text-align:center">{{$link->id}}</td>
            <td style="text-align:center">{{$link->title}}</td>
             <td style="text-align:center">{{$link->summery}}</td>
           <?php  if($link->image != ''){ ?>
             <td style="text-align:center"><img src="{{ URL::asset('uploads/nav_link/'.$link->image) }}" width="100" height="100" /></td>           
           <?php }else {?> 
             <td style="text-align:center"></td>
           <?php }?>
            <td style="text-align:center">{{$link->link}}</td>
            <td style="text-align:center">{{$link->nav_id}}</td>
            <td style="text-align:center">{{$link->parent}}</td>
            <td style="text-align:center"><a href="nav_link/{{$link->id}}/edit" class="btn btn-warning">{{ Lang::get('global.edit')}}</a></td>

            <td style="text-align:center">
                {{ Form::open(array('url'=>'administrator/nav_link/'.$link->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(Lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close()}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@stop