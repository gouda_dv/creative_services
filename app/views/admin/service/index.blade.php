@extends('master')
@section('content')
<a href="service/create" style="margin-top: 36px;" class="btn btn-info">{{ Lang::get('global.add')}}</a>
<table class="table" style="margin-top:50px">
    <thead>
        <tr>
           
            <th style="text-align:center">{{ Lang::get('global.title')}}</th>
            <th style="text-align:center">{{ Lang::get('global.category_id')}}</th>
            <th style="text-align:center">{{ Lang::get('global.summery')}}</th>
<!--             <th style="text-align:center">{{ Lang::get('global.content')}}</th>-->
            
            <th style="text-align:center">{{ Lang::get('global.image')}}</th>
            
            <th style="text-align:center" colspan="2">{{ Lang::get('global.options')}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($service as $services)
        <tr>
            
            <td style="text-align:center">{{$services->title}}</td>
            <td style="text-align:center">{{$services->service_category_id}}</td>
            <td style="text-align:center">{{$services->summery}}</td>
<!--             <td style="text-align:center">{{$services->content}}</td>-->
            @if(isset($services->image))
            <td style="text-align:center"><img src="{{ URL::asset('uploads/service/'.$services->image) }}" width="100" height="100" /></td>
            @else
            <td style="text-align:center"></td>
            @endif
            
            <td style="text-align:center"><a href="service/{{$services->id}}/edit" class="btn btn-warning">{{ Lang::get('global.edit')}}</a></td>

            <td style="text-align:center">
                {{ Form::open(array('url' =>'administrator/service/'.$services->id , 'class' => 'pull-right','onsubmit'=>"return confirm('".Lang::get('global.delete_msg')."');")) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit(Lang::get('global.delete'), array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop