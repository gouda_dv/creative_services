@extends('master')
@section('content')

<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script src="{{ URL::asset('js/locationpicker/locationpicker.jquery.js') }}"></script>

@if($errors)
<div class="box box-solid box-danger ">
    <div class=" box-header" >   
        @foreach ($errors->all('<li style="list-style: none;margin: 10px">:message</li>') as $message)
        <?php echo $message; ?>
        @endforeach
    </div>
</div>
@endif

@if(isset($service))
{{ Form::open(array('role'=>'form','url'=>'administrator/service/'. $service->id,'files' => true,'method'=>'put','id'=>'service_form','novalidate'=>'novalidate')) }}
<div class="form-group ">
    {{ Form::hidden('id',$service->id) }} 
</div>



<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords',$service->seo_meta_keywords,array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description',$service->seo_meta_description, ['class' => 'form-control']) }}
</div>
<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title')) }}
    {{ Form::text('title',$service->title,array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>
<div class="form-group ">
    {{ Form::label('service_category_id',Lang::get('global.category_id')) }}
    {{ Form::select('service_category_id',$cats,$service->service_category_id,array('class'=>'form-control',"data-validation"=>"required"))}}

</div>
<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
     {{ Form::textarea('summery',$service->summery,array('class'=>'ckeditor')) }} 
    
</div>
<div class="form-group ">
    {{ Form::label('content',Lang::get('global.content')) }}
    {{ Form::textarea('content',$service->content,array('class'=>'ckeditor')) }} 
</div>

<div class="form-group">

    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image',array("data-validation"=>"mime size", "data-validation-allowing"=>"jpg, png, gif" ,"data-validation-max-size"=>"8M")) }} 
    @if($service->image)
    <img src="{{ URL::asset('uploads/service/'.$service->image) }}" width="100" height="100"  />
    @endif
</div>


{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}


@else

{{ Form::open(array('role'=>'form','route'=>'administrator.service.store','files' => true,'id'=>'service_form')) }}


<div class="form-group ">
    {{ Form::label('seo_meta_keywords',Lang::get('global.seo_meta_keywords')) }}
    {{ Form::text('seo_meta_keywords','',array('class'=>'form-control')) }} 
</div>
<div class="form-group ">
    {{ Form::label('seo_meta_description',Lang::get('global.seo_meta_description')) }}
    {{ Form::text('seo_meta_description','', ['class' => 'form-control']) }}
</div>
<div class="form-group ">
    {{ Form::label('title',Lang::get('global.title')) }}
    {{ Form::text('title',"",array('class'=>'form-control',"data-validation"=>"required")) }} 
</div>
<div class="form-group ">
    {{ Form::label('service_category_id', Lang::get('global.category_id')) }}
    {{ Form::select('service_category_id',$cats,null,array('class'=>'form-control',"data-validation"=>"required"))}}
</div>

<div class="form-group ">
    {{ Form::label('summery',Lang::get('global.summery')) }}
    {{ Form::textarea('summery','',array('class'=>'ckeditor')) }} 
</div>
<div class="form-group ">
    {{ Form::label('content',Lang::get('global.content')) }}
    {{ Form::textarea('content','',array('class'=>'ckeditor')) }} 
</div>

<div class="form-group">
    {{ Form::label('image',Lang::get('global.image')) }}
    {{ Form::file('image',array("data-validation"=>"required mime size", "data-validation-allowing"=>"jpg, png, gif" ,"data-validation-max-size"=>"8M")) }} 
</div>



 




{{ Form::submit(Lang::get('global.save') ,array('class'=>"btn btn-success")) }}

{{ Form::close() }}
@endif

<script>  CKEDITOR.replace('content',
            {
                filebrowserBrowseUrl: "{{ URL::asset('ckfinder/ckfinder.html') }}",
                filebrowserImageBrowseUrl:"{{ URL::asset('ckfinder/ckfinder.html') }}?type=Images",
                filebrowserUploadUrl:
                        "{{ URL::asset('ckfinder') }}/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/",
                filebrowserImageUploadUrl:
                       "{{ URL::asset('ckfinder')}}/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/"
            });
</script>
<script> 
    $.validate({
  modules : 'file'
}); </script>
@stop