<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Creative Services | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="http://code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="{{ URL::asset('css/morris/morris.css') }}" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="{{ URL::asset('css/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="{{ URL::asset('css/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="{{ URL::asset('css/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="{{ URL::asset('css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">-->

        <!-- Theme style -->
        <!-- add new calendar event modal -->
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>

        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

        <!-- JS -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="{{ URL::asset('js/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="{{ URL::asset('js/plugins/sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="{{ URL::asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{ URL::asset('js/plugins/jqueryKnob/jquery.knob.js') }}" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="{{ URL::asset('js/plugins/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
        <!-- datepicker -->
        <script src="{{ URL::asset('js/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ URL::asset('js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="{{ URL::asset('js/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('js/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ URL::asset('js/AdminLTE/app.js') }}" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ URL::asset('js/AdminLTE/dashboard.js') }}" type="text/javascript"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
<!--  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>-->
        <!-- AdminLTE for demo purposes -->

<!--        <script src="{{ URL::asset('js/AdminLTE/demo.js') }}" type="text/javascript"></script>-->

        <!-- validation jquery  -->

        <!--        
            @if(Session::get('lang')=='ar')
            <a class="btn btn-info" href='{{Request::url()}}/en' >English</a>
            <link href="{{ URL::asset('css/AdminLTE-rtl.css') }}" rel="stylesheet" type="text/css" />
            <style>
                #ar{display:none}
                #en{display:block}
            </style>
            @elseif(Session::get('lang')=='en')
            <link href="{{ URL::asset('css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
            <a class="btn btn-info" href='{{Request::url()}}/ar'>Arabic</a>
            <style>
                #ar{display:block}
                #en{display:none}
            </style>
            @endif-->

<!--        <script src="{{ URL::asset('js/AdminLTE/demo.js') }}" type="text/javascript"></script>-->


    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Creative Services
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <!--                    <li class="dropdown messages-menu">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-envelope"></i>
                                                    <span class="label label-success">4</span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="header">You have 4 messages</li>
                                                    <li>
                                                         inner menu: contains the actual data 
                                                        <ul class="menu">
                                                            <li> start message 
                                                                <a href="#">
                                                                    <div class="pull-left">
                                                                        <img src="{{ URL::asset('uploads/admin/'.Session::get('admin_image')) }}" class="img-circle" alt="User Image"/>
                                                                    </div>
                                                                    <h4>
                                                                        Support Team
                                                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                                    </h4>
                                                                    <p>Why not buy a new awesome theme?</p>
                                                                </a>
                                                            </li> end message 
                                                        </ul>
                                                    </li>
                                                    <li class="footer"><a href="#">See All Messages</a></li>
                                                </ul>
                                            </li>
                                             Notifications: style can be found in dropdown.less 
                                            <li class="dropdown notifications-menu">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-warning"></i>
                                                    <span class="label label-warning">10</span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="header">You have 10 notifications</li>
                                                    <li>
                                                         inner menu: contains the actual data 
                                                        <ul class="menu">
                                                            <li>
                                                                <a href="#">
                                                                    <i class="ion ion-ios7-people info"></i> 5 new members joined today
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="footer"><a href="#">View all</a></li>
                                                </ul>
                                            </li>
                                             Tasks: style can be found in dropdown.less 
                                            <li class="dropdown tasks-menu">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-tasks"></i>
                                                    <span class="label label-danger">9</span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="header">You have 9 tasks</li>
                                                    <li>
                                                         inner menu: contains the actual data 
                                                        <ul class="menu">
                                                            <li> Task item 
                                                                <a href="#">
                                                                    <h3>
                                                                        Design some buttons
                                                                        <small class="pull-right">20%</small>
                                                                    </h3>
                                                                    <div class="progress xs">
                                                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="sr-only">20% Complete</span>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </li> end task item 
                                                        </ul>
                                                    </li>
                                                    <li class="footer">
                                                        <a href="#">View all tasks</a>
                                                    </li>
                                                </ul>
                                            </li>-->
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{Session::get('admin_name')}} <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="{{ URL::asset('uploads/admin/'.Session::get('admin_image')) }}" class="img-circle" alt="User Image" />
                                    <p>
                                        {{ Session::get('admin_name')}} - Web Developer
     <!--                                    <small>Member since Nov. 2012</small>-->
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <!--                            <li class="user-body">
                                                                <div class="col-xs-4 text-center">
                                                                    <a href="#">Followers</a>
                                                                </div>
                                                                <div class="col-xs-4 text-center">
                                                                    <a href="#">Sales</a>
                                                                </div>
                                                                <div class="col-xs-4 text-center">
                                                                    <a href="#">Friends</a>
                                                                </div>
                                                            </li>-->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ URL::to('administrator/admin/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown user user-menu ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                <span><?php echo Lang::get('global.lang'); ?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu " style="width: 160px">
                                <!-- User image -->


                                <!-- Menu Footer-->
                                <li class="user-footer" style="padding: 0px;">
                                    <div class="pull-left ">
                                        <a href='{{ URL::to('administrator/lang/ar') }}' style="width: 160px;" class="btn btn-default btn-flat"> <img src="{{ URL::asset('website_design/images/sa.gif') }}"/> العربيه</a>
                                    </div>
                                </li>


                                <li class="user-footer"  style="padding: 0px;">
                                    <div class="pull-left " >
                                        <a href='{{ URL::to('administrator/lang/en') }}'  style="width: 160px" class="btn btn-default btn-flat" > <img src="{{ URL::asset('website_design/images/gb.gif') }}"/>  english</a>
                                    </div>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{ URL::asset('uploads/admin/'.Session::get('admin_image')) }}" class="img-circle" alt="User Image"  width="100" height="100"/>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, {{ Session::get('admin_name')}}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>





                        @if(Session::get('lang')=='ar')
                        <!--   <a class="btn btn-info" href='{{ URL::to('administrator/lang/en') }}' >English</a>  -->
                        <link href="{{ URL::asset('css/AdminLTE-rtl.css') }}" rel="stylesheet" type="text/css" />

                        @elseif(Session::get('lang')=='en')
                        <link href="{{ URL::asset('css/AdminLTE-ltr.css') }}" rel="stylesheet" type="text/css" />  
                        <!--  <a class="btn btn-info" href='{{ URL::to('administrator/lang/ar') }}'>عربي</a>  -->

                        @else
                        <!--   <a class="btn btn-info" href='{{ URL::to('administrator/lang/en') }}' >English</a>   -->
                        <link href="{{ URL::asset('css/AdminLTE-ltr.css') }}" rel="stylesheet" type="text/css" /> 

                        @endif



                    </div>
                    <!-- search form -->
                    <!--                <form action="#" method="get" class="sidebar-form">
                                        <div class="input-group">
                                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                                            <span class="input-group-btn">
                                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </form>-->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <!--                        <li class="active">
                                                    <a href="index.html">
                                                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="pages/widgets.html">
                                                        <i class="fa fa-th"></i> <span>Widgets</span> <small class="badge pull-right bg-green">new</small>
                                                    </a>
                                                </li>-->

                        <li class="treeview"style="padding:5px 30px 5px 5px">
                            <i class="fa fa-laptop"></i> 
                            <span><a href="{{ URL::to('administrator/settings') }}"><?php echo Lang::get('global.settings'); ?>  </a></span>
                        </li>

<!--                        <li class="treeview">
                            <a href="{{ URL::to('place') }}">

                                <i class="fa fa-laptop"></i>
                                <span><?php //echo Lang::get('global.places'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/place') }}"><i class="fa "></i><?php //echo Lang::get('global.Show_All_Places'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/place/create') }}"><i class="fa"></i><?php //echo Lang::get('global.Add_New_Place'); ?></a></li>


                            </ul>
                        </li>-->
  <li class="treeview">
                            <a href="{{ URL::to('service') }}">

                                <i class="fa fa-laptop"></i>
                                <span><?php echo Lang::get('global.service'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/service') }}"><i class="fa "></i><?php echo Lang::get('global.Show_All_services'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/service/create') }}"><i class="fa"></i><?php echo Lang::get('global.Add_New_service'); ?></a></li>


                            </ul>
                        </li>
                        
                         <li class="treeview">
                            <a href="{{ URL::to('administrator/service_category') }}">

                                <i class="fa fa-laptop"></i>
                                <span><?php echo Lang::get('global.service_category'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/service_category') }}"><i class="fa "></i><?php echo Lang::get('global.show_all_service_categories'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/service_category/create') }}"><i class="fa"></i><?php echo Lang::get('global.add_new_service_categories'); ?></a></li>


                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="{{ URL::to('administrator/project') }}">

                                <i class="fa fa-laptop"></i>
                                <span><?php echo Lang::get('global.project'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/project') }}"><i class="fa "></i><?php echo Lang::get('global.show_all_projects'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/project/create') }}"><i class="fa"></i><?php echo Lang::get('global.add_new_project'); ?></a></li>


                            </ul>
                        </li>
                           <li class="treeview">
                            <a href="{{ URL::to('administrator/partner') }}">
                                <i class="fa fa-laptop"></i>
                                <span><?php echo Lang::get('global.partners'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="{{ URL::to('administrator/partner') }}"><i class="fa "></i> <?php echo Lang::get('global.show_all_partners'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/partner/create') }}"><i class="fa "></i><?php echo Lang::get('global.add_new_partner'); ?> </a></li>



                            </ul>  
                        </li>
                         <li class="treeview">
                            <a href="{{ URL::to('administrator/nav_link') }}">

                                <i class="fa fa-laptop"></i>
                                <span><?php echo Lang::get('global.nav_links'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/nav_link') }}"><i class="fa "></i><?php echo Lang::get('global.Show_All_NavLinks'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/nav_link/create') }}"><i class="fa"></i><?php echo Lang::get('global.Add_New_NavLink'); ?></a></li>


                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="{{ URL::to('administrator/branch') }}">

                                <i class="fa fa-laptop"></i>
                                <span><?php echo Lang::get('global.branches'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/branch') }}"><i class="fa "></i><?php echo Lang::get('global.show_all_branches'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/branch/create') }}"><i class="fa"></i><?php echo Lang::get('global.add_new_branch'); ?></a></li>


                            </ul>
                        </li>
                        
                        

<!--                        <li class="treeview">
                            <a href="{{ URL::to('administrator/question') }}">

                                <i class="fa fa-laptop"></i>
                                <span><?php //echo Lang::get('global.Questionnaire'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/question') }}"><i class="fa"></i><?php //echo Lang::get('global.All_Questionnaires'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/question/create') }}"><i class="fa "></i><?php //echo Lang::get('global.New_Questionnaire'); ?></a></li>


                            </ul>
                        </li>-->

                        <li class="treeview">
                            <a href="{{ URL::to('administrator/content') }}">

                                <i class="fa fa-laptop"></i>
                                <span><?php echo Lang::get('global.Content'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/content') }}"><i class="fa "></i><?php echo Lang::get('global.All_Content'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/content/create') }}"><i class="fa"></i><?php echo Lang::get('global.Add_Content'); ?></a></li>


                            </ul>
                        </li>

<!--                        <li class="treeview">
                            <a href="{{ URL::to('administrator/gallery') }}">

                                <i class="fa fa-laptop"></i>
                                <span><?php //echo Lang::get('global.Gallery'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="{{ URL::to('administrator/gallery') }}"><i class="fa "></i><?php //echo Lang::get('global.All_Galleries'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/gallery/create') }}"><i class="fa "></i><?php //echo Lang::get('global.Add_Gallery'); ?></a></li>

                            </ul>
                        </li>-->

<!--                        <li class="treeview">
                            <a href="{{ URL::to('administrator/center_members') }}">
                                <i class="fa fa-laptop"></i>
                                <span><?php //echo Lang::get('global.Center_Members'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="{{ URL::to('administrator/center_members') }}"><i class="fa "></i> <?php //echo Lang::get('global.All_Members'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/center_members/create') }}"><i class="fa "></i><?php //echo Lang::get('global.Add_Member'); ?> </a></li>


                            </ul>
                        </li>-->

<!--                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span><?php //echo Lang::get('global.Honor_Members'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="{{ URL::to('administrator/honor_members') }}"><i class="fa "></i><?php //echo Lang::get('global.Show_All_Members'); ?> </a></li>
                                <li><a href="{{ URL::to('administrator/honor_members/create') }}"><i class="fa "></i><?php //echo Lang::get('global.Add_New_Member'); ?> </a></li>


                            </ul>
                        </li>-->
<!--                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span><?php //echo Lang::get('global.videos'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="{{ URL::to('administrator/videos') }}"><i class="fa "></i><?php //echo Lang::get('global.Show_All_Videos'); ?> </a></li>
                                <li><a href="{{ URL::to('administrator/videos/create') }}"><i class="fa "></i><?php //echo Lang::get('global.Add_New_Video'); ?> </a></li>



                            </ul>
                        </li>-->
                     

                        <li class="treeview">
                            <a href="#">

                                <i class="fa fa-laptop"></i>
                                <span><?php echo Lang::get('global.Header_Slider_Images'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/header_slider') }}"><i class="fa "></i> <?php echo Lang::get('global.Show_All_Images'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/header_slider/create') }}"><i class="fa"></i> <?php echo Lang::get('global.Add_New_Image'); ?></a></li>


                            </ul>

                        </li>
                         <li class="treeview">
                            <a href="{{ URL::to('administrator/contact_us') }}">

                                <i class="fa fa-laptop"></i>
                                <span><?php echo Lang::get('global.contact_us'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/contact_us') }}"><i class="fa "></i><?php echo Lang::get('global.show_all_msgs'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/contact_us/create') }}"><i class="fa"></i><?php echo Lang::get('global.add_new_msg'); ?></a></li>


                            </ul>
                        </li>

<!--                        <li class="treeview">
                            <a href="#">

                                <i class="fa fa-laptop"></i> <span> <?php //echo Lang::get('global.Related_Links'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/related_links') }}"><i class="fa "></i><?php //echo Lang::get('global.Show_All_Links'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/related_links/create') }}"><i class="fa "></i><?php //echo Lang::get('global.Add_New_Link'); ?></a></li>

                            </ul> 

                        </li>-->


<!--                        <li class="treeview">
                            <a href="#">

                                <i class="fa fa-laptop"></i> <span> <?php //echo Lang::get('global.user_proposal'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('administrator/user_proposal') }}"><i class="fa "></i><?php //echo Lang::get('global.show_all_proposals'); ?></a></li>
                                <li><a href="{{ URL::to('administrator/user_proposal/create') }}"><i class="fa "></i><?php //echo Lang::get('global.add_new_proposal'); ?></a></li>

                            </ul> 

                        </li>-->


                        <!--                    <li>
                                                <a href="pages/calendar.html">
                                                    <i class="fa fa-calendar"></i> <span>Calendar</span>
                                                    <small class="badge pull-right bg-red">3</small>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="pages/mailbox.html">
                                                    <i class="fa fa-envelope"></i> <span>Mailbox</span>
                                                    <small class="badge pull-right bg-yellow">12</small>
                                                </a>
                                            </li>
                                            <li class="treeview">
                                                <a href="#">
                                                    <i class="fa fa-folder"></i> <span>Examples</span>
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </a>
                                                <ul class="treeview-menu">
                                                    <li><a href="pages/examples/invoice.html"><i class="fa "></i> Invoice</a></li>
                                                    <li><a href="pages/examples/login.html"><i class="fa "></i> Login</a></li>
                                                    <li><a href="pages/examples/register.html"><i class="fa "></i> Register</a></li>
                                                    <li><a href="pages/examples/lockscreen.html"><i class="fa "></i> Lockscreen</a></li>
                                                    <li><a href="pages/examples/404.html"><i class="fa "></i> 404 Error</a></li>
                                                    <li><a href="pages/examples/500.html"><i class="fa "></i> 500 Error</a></li>
                                                    <li><a href="pages/examples/blank.html"><i class="fa "></i> Blank Page</a></li>
                                                </ul>
                                            </li>-->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
    <!--            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>-->

                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section>
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->




    </body>
</html>
